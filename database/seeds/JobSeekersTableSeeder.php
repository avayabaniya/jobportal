<?php

use App\JobSeeker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class JobSeekersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $job_seeker = new JobSeeker();

        $job_seeker->name = 'JobSeeker1 JobSeeker1';
        $job_seeker->email = 'jobseeker1@gmail.com';
        $job_seeker->password = Hash::make('password');

        $job_seeker->save();
    }
}
