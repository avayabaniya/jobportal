<?php

use App\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = new Company();

        $company->name = 'Company1 Company1';
        $company->email = 'company1@gmail.com';
        $company->password = Hash::make('password');
        $company->profile_image = 'img1.jpg';
        $company->save();
    }
}
