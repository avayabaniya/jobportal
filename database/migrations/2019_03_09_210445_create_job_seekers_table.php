<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobSeekersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_seekers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->string('verification_token')->nullable();
            $table->integer('verification_status')->default(0);
            $table->string('password');
            $table->string('profile_image')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('address')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('cv')->nullable();
            $table->string('tagline')->nullable();
            $table->integer('gender')->nullable();
            $table->string('qualification')->nullable();
            $table->string('reference_email')->nullable();
            $table->integer('preferred_job_category_id')->nullable();
            $table->string('personal_site')->nullable();
            $table->string('linkedin')->nullable();
            $table->longText('description')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_seekers');
    }
}
