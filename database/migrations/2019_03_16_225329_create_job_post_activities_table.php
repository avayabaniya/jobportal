<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPostActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_post_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('job_seeker_id')->unsigned();
            $table->integer('job_post_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->date('applied_date');
            $table->string('cv')->nullable();
            $table->integer('application_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_post_activities');
    }
}
