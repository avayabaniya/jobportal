<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('company_id')->unsigned();
            $table->string('job_title');
            $table->string('job_type');
            $table->string('job_category');
            $table->string('job_location');
            $table->date('job_publish_date');
            $table->date('job_expiry_date');
            $table->longText('job_description')->nullable();
            $table->integer('job_vacancy_number')->default(1);
            $table->double('min_job_salary')->nullable();
            $table->double('max_job_salary')->nullable();
            $table->integer('show_salary')->default(1);
            $table->integer('job_active')->default(1);
            $table->integer('job_status')->default(0);
            $table->integer('candidates')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_posts');
    }
}
