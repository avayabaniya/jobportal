<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->string('verification_token')->nullable();
            $table->integer('verification_status')->default(0);
            $table->integer('status')->default(0);
            $table->string('password');
            $table->string('profile_image')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('pin_number')->nullable();
            $table->string('established_date')->nullable();
            $table->string('category')->nullable();
            $table->integer('number_of_employees')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('address')->nullable();
            $table->string('company_site')->nullable();
            $table->string('linkedin')->nullable();
            $table->double('lat',20,10)->default(0.0);
            $table->double('lng',20,10)->default(0.0);
            $table->longText('description')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
