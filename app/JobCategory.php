<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCategory extends Model
{
    public function jobPosts ()
    {
        return $this->hasMany('App\JobPost', 'job_category');
    }

    public function company ()
    {
        return $this->hasOne('App\Company', 'category');
    }
}
