<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPost extends Model
{
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function category()
    {
        return $this->belongsTo('App\JobCategory', 'job_category');
    }

    public function jobType()
    {
        return $this->belongsTo('App\JobType', 'job_type');
    }

    public function jobPostActivity()
    {
        return $this->hasMany('App\JobPostActivity', 'job_post_id');
    }

    public function jobBookmark()
    {
        return $this->hasMany('App\JobBookmark', 'job_post_id');
    }
}
