<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class NotificationController extends Controller
{
    public function get(){
        return Notification::all();
    }

    public function applyJobNotification(Request $request)
    {
            if ($request->notification_count != Auth::guard('company')->user()->unreadNotifications->count())
            {
                $notification_count = Auth::guard('company')->user()->unreadNotifications->count();
                //return Auth::guard('company')->user()->unreadNotifications;

                $APP_URL = json_encode(url('/'));

                $div = '<div class="simplebar-track vertical fade-out-notification" style="visibility: visible;"><div class="simplebar-scrollbar" style="visibility: visible;"></div></div>
                        <div class="simplebar-track horizontal" style="visibility: visible;"><div class="simplebar-scrollbar" style="visibility: visible; left: 0px; width: 25px;"></div></div>
                        <div class="simplebar-scroll-content" style="padding-right: 17px; margin-bottom: -34px;"><div class="simplebar-content" style="padding-bottom: 17px; margin-right: -17px;">
                        <ul>
                            <span id="updated_count" style="display: none;">'. $notification_count .'</span>';

                            foreach (Auth::guard('company')->user()->unreadNotifications as $notification)
                            {
                                    if (empty($notification->data['jobSeeker']['profile_image']))
                                    {
                                        $notification_image = '<span class="notification-icon"><i class="icon-material-outline-group"></i></span>';
                                    }else{
                                        $notification_image = '<span class="notification-icon"><img src="/coursework/public/job_seeker/images/profile/'.$notification->data['jobSeeker']['profile_image'].'" ></span>';
                                    }

                                    if ($notification->type == "App\Notifications\JobApplyNotification")
                                    {
                                        $link = '<a href="http://localhost/coursework/company/manage-candidates/'. $notification->data['jobPost']['id'].'">';
                                        $notification_text = '<strong>'.$notification->data['jobSeeker']['name'].'</strong> applied for a job <span class="color">'.$notification->data['jobPost']['job_title'].'</span>';
                                    }
                                    else{
                                        if ($notification->data['jobPost']['job_status'] == 0){
                                            $jobStatus = "Pending";
                                        }elseif($notification->data['jobPost']['job_status'] == 1){
                                            $jobStatus = "Accepted";
                                        }else{
                                            $jobStatus = "Declined";
                                        }
                                        $link = '<a href="http://localhost/coursework/company/edit/job-post/'. $notification->data['jobPost']['id'].'">';
                                        $notification_text = 'Your job post status for <strong>'.$notification->data['jobPost']['job_title'].'</strong> has been changed to <span class="color">'.$jobStatus.'</span>';
                                    }

                                    $div = $div . '<li class="notifications-not-read">'.
                                    $link
                                    .$notification_image.
                                    '<span class="notification-text">'.
                                        $notification_text
                                    .'</span>
                                    </a>
                                    </li>';
                            }

                            $notification_div = $div. '</ul>
                                                        </div>
                                                        </div>';

                //return view('frontend.notification.apply_job_notification');
                return [$notification_count, $notification_div];
            }
        //}
        //elseif (Auth::guard('job_seeker')->check())
        //{
            //return 'aaaa';

        //}
        return -1;//no change required
    }



    public function applicationChangeNotification(Request $request)
    {
        if ($request->notification_count != Auth::guard('job_seeker')->user()->unreadNotifications->count()){

            $notification_count = Auth::guard('job_seeker')->user()->unreadNotifications->count();
            //return Auth::guard('company')->user()->unreadNotifications;

            $APP_URL = json_encode(url('/'));

            $div = '<div class="simplebar-track vertical fade-out-notification" style="visibility: visible;"><div class="simplebar-scrollbar" style="visibility: visible;"></div></div>
                        <div class="simplebar-track horizontal" style="visibility: visible;"><div class="simplebar-scrollbar" style="visibility: visible; left: 0px; width: 25px;"></div></div>
                        <div class="simplebar-scroll-content" style="padding-right: 17px; margin-bottom: -34px;"><div class="simplebar-content" style="padding-bottom: 17px; margin-right: -17px;">
                        <ul>
                            <span id="updated_count" style="display: none;">'. $notification_count .'</span>';


            foreach (Auth::guard('job_seeker')->user()->unreadNotifications as $notification)
            {
                if (empty($notification->data['company']['profile_image']))
                {
                    $notification_image = '<span class="notification-icon"><i class="icon-material-outline-group"></i></span>';
                }else{
                    $notification_image = '<span class="notification-icon"><img src="public/company/images/profile/'.$notification->data['company']['profile_image'].'" ></span>';
                }

                if ($notification->type == "App\Notifications\ApplicationStatusChangeNotification")
                {

                    if ($notification->data['jobPostActivity']['application_status'] == 0){
                        $jobStatus = "Pending";
                    }elseif($notification->data['jobPostActivity']['application_status'] == 1){
                        $jobStatus = "Accepted";
                    }else{
                        $jobStatus = "Declined";
                    }
                    $link = '<a href="http://localhost/coursework/job-seeker/view-applied-jobs">';
                    $notification_text = 'Your application status for <strong>'.$notification->data['jobPost']['job_title'].'</strong> has been changed to <span class="color">'.$jobStatus.'</span>';
                }

                $div = $div . '<li class="notifications-not-read">'.
                    $link
                    .$notification_image.
                    '<span class="notification-text">'.
                    $notification_text
                    .'</span>
                                    </a>
                                    </li>';
            }

            $notification_div = $div. '</ul>
                                                        </div>
                                                        </div>';

            //return view('frontend.notification.apply_job_notification');
            return [$notification_count, $notification_div];
        }

        return -1;

    }

    public function markAllAsRead()
    {
        if (Auth::guard('company')->check()){
            if (Auth::guard('company')->user()->unreadNotifications->count())
            {
                foreach (Auth::guard('company')->user()->unreadNotifications as $notification)
                {
                    $notification->markAsRead();
                }

                return redirect()->back()->with('success', 'All Notification successfully marked as read');
            }
        }elseif (Auth::guard('job_seeker')->check()){
            if (Auth::guard('job_seeker')->user()->unreadNotifications->count())
            {
                foreach (Auth::guard('job_seeker')->user()->unreadNotifications as $notification)
                {
                    $notification->markAsRead();
                }

                return redirect()->back()->with('success', 'All Notification successfully marked as read');
            }
        }


        return redirect()->back()->with('error', 'No unread notification found');
    }
}
