<?php

namespace App\Http\Controllers;

use App\Achievement;
use App\JobCategory;
use App\JobPostActivity;
use App\JobSeeker;
use App\ReferenceEmail;
use App\Skill;
use App\Timeline;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Image;

class JobSeekerController extends Controller
{
    public function logout(){
        //Session::flush();
        Auth::guard('job_seeker')->logout();
        return redirect()->back()->with('flash_message_success', 'Logout Successful');
    }

    public function sendVerificationEmailDone($email, $verificationToken)
    {
        $jobseeker = JobSeeker::where('email', $email)->where('verification_token', $verificationToken)->first();
        if ($jobseeker)
        {
            $jobseeker->verification_status = 1;
            $jobseeker->verification_token = null;
            $jobseeker->save();

            //log the user in
            //Auth::guard('job_seeker')->attempt(['email' => $jobseeker->email]);

            Auth::guard('job_seeker')->loginUsingId($jobseeker->id);

            return redirect()->route('index')->with('success', 'Email successfully verified');

        }else{
            return redirect()->back()->with('error', 'User does not exist');
        }
    }

    public function dashboard()
    {
        $jobseeker_id = Auth::guard('job_seeker')->user()->id;
        $jobseeker = JobSeeker::where('id', $jobseeker_id)->firstOrFail();

        return view('frontend.dashboard.jobseeker_dashboard')->with(compact('jobseeker'));
    }

    public function setting(Request $request)
    {
        $jobseeker = JobSeeker::with('skill')->where('id', Auth::guard('job_seeker')->user()->id)->first();
        $jobCategories = JobCategory::where('status', 1)->get();
        if ($request->isMethod('post'))
        {
            //dd($request->all());

            $this->validate($request,[
                'name' => 'required',
                'contact_number' => 'required',
                'personal_site' => 'nullable|url',
                'linkedin' => 'nullable|url',
                'facebook' => 'nullable|url',
                'twitter' => 'nullable|url',
                'github' => 'nullable|url',
                'profile_image' => 'nullable|image',
                'email' =>'required|email|unique:companies',
            ]);


            //dd($request->all());
            ini_set('memory_limit','256M');
            if($request->hasFile('profile_image')){
                $image_tmp = Input::file('profile_image');
                if($image_tmp->isValid()){
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(7777,999999).'.'.$extension;

                    $image_path = 'public/job_seeker/images/profile/'.$filename;


                    Image::make($image_tmp)->save($image_path);
                    //remove old image

                    if ($jobseeker->profile_image != "default_avatar.jpg")
                    {
                        if (!empty($jobseeker->profile_image)) {
                            if (file_exists('public/job_seeker/images/profile/' . $jobseeker->profile_image)) {
                                unlink('public/job_seeker/images/profile/' . $jobseeker->profile_image);
                            }
                        }
                    }

                }
            }else{
                if (empty($jobseeker->profile_image))
                {
                    $filename = "default_avatar.jpg";
                }else{
                    $filename = $jobseeker->profile_image;
                }
            }

            if($request->hasFile('cv')){
                $cv_tmp = Input::file('cv');
                if($cv_tmp->isValid()){
                    $extension = $cv_tmp->getClientOriginalExtension();
                    $cvname = 'cv_'.$jobseeker->name.'_'.rand(777,999).'.'.$extension;

                    $cv_path = public_path('job_seeker\files');
                    $cv_tmp->move($cv_path, $cvname);
                    //remove old cv
                    if (!empty($jobseeker->cv))
                    {
                        if (file_exists('public/job_seeker/files/'. $jobseeker->cv)) {
                            unlink('public/job_seeker/files/'. $jobseeker->cv);
                        }
                    }

                }
            }else{
                $cvname = $jobseeker->cv;
            }

            $jobseeker->profile_image = $filename;
            $jobseeker->cv = $cvname;
            $jobseeker->name = $request->name;
            $jobseeker->email = $request->email;
            $jobseeker->contact_number = $request->contact_number;
            $jobseeker->gender = $request->gender;
            $jobseeker->address = $request->address;
            $jobseeker->date_of_birth = $request->date_of_birth;
            $jobseeker->preferred_job_category_id = $request->job_category;
            $jobseeker->qualification = $request->qualification;
            $jobseeker->reference_email = $request->reference_email;
            $jobseeker->tagline = $request->tagline;
            $jobseeker->personal_site = $request->personal_site;
            $jobseeker->linkedin = $request->linkedin;
            $jobseeker->facebook = $request->facebook;
            $jobseeker->twitter = $request->twitter;
            $jobseeker->github = $request->github;
            $jobseeker->description = $request->description;
            if (!empty($request->current_password) && !empty($request->new_password) && !empty($request->confirm_password))
            {
                if ($request->new_password = $request->confirm_password)
                {
                    if (Hash::check($request->current_password, $jobseeker->password))
                    {
                        $jobseeker->password = bcrypt($request->new_password);
                    }
                }
            }

            if (!empty($request->skill))
            {
                Skill::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->delete();

                foreach ($request->skill as $s)
                {
                    if (empty($s))
                    {
                        continue;
                    }

                    $skill = new Skill();
                    $skill->job_seeker_id = Auth::guard('job_seeker')->user()->id;
                    $skill->skill = $s;
                    $skill->save();
                }
            }

            $jobseeker->save();

            return redirect()->back()->with('success', 'Profile successfully updated');
        }
        return view('frontend.dashboard.setting.jobseeker_setting')->with(compact('jobseeker', 'jobCategories'));

    }

    public function chkUserPassword(Request $request)
    {
        $data = $request->all();

        $current_password = $data['current_password'];
        $user_id = Auth::guard('job_seeker')->user()->id;
        $check_password = JobSeeker::where('id', $user_id)->first();
        if (Hash::check($current_password, $check_password->password)){
            echo "true"; die;
        }else{
            echo "false"; die;
        }
    }

    public function addTimeline(Request $request)
    {
        if ($request->isMethod('post'))
        {
            if (empty($request->timeline_type))
            {
                $request->timeline_type = 0;
            }

            //dd($request->all());
            $timeline = new Timeline();
            $timeline->job_seeker_id = Auth::guard('job_seeker')->user()->id;
            $timeline->title = $request->title;
            $timeline->sub_title = $request->sub_title;
            $timeline->start_date = $request->start_date;
            $timeline->end_date = $request->end_date;
            $timeline->description = $request->description;
            $timeline->timeline_type = $request->timeline_type;
            $timeline->save();

            if (!empty($request->email))
            {
                foreach ($request->email as $s)
                {
                    if (empty($s))
                    {
                        continue;
                    }

                    $email = new ReferenceEmail();
                    $email->timeline_id = $timeline->id;
                    $email->email = $s;
                    $email->save();
                }
            }

            if ($request->timeline_type == 0)
            {
                return redirect()->route('job.seeker.timeline')->with('success', 'Event successfully added to timeline');
            }elseif($request->timeline_type == 1){
                return redirect()->route('job.seeker.academic.timeline')->with('success', 'Event successfully added to timeline');
            }

        }
        return view('frontend.dashboard.timeline.add_timeline');
    }

    public function viewCareerTimeline(Request $request)
    {

        $timelineEvents = Timeline::with('referenceEmail')->where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->where('timeline_type', 0)->orderBy('start_date', 'DESC')->get();
        return view('frontend.dashboard.timeline.view_timeline')->with(compact('timelineEvents'));

    }

    public function viewAcademicTimeline(Request $request)
    {

        $timelineEvents = Timeline::with('referenceEmail')->where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->where('timeline_type', 1)->orderBy('start_date', 'DESC')->get();
        return view('frontend.dashboard.timeline.view_academic_timeline')->with(compact('timelineEvents'));

    }

    public  function editTimeline(Request $request, $id)
    {
        $timeline = Timeline::where('id', $id)->where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->first();

        if (count($timeline) == 0)
        {
            return redirect()->back();
        }

        if ($request->isMethod('post'))
        {
            if (empty($request->timeline_type))
            {
                $request->timeline_type = 0;
            }


            $timeline->title = $request->title;
            $timeline->sub_title = $request->sub_title;
            $timeline->start_date = $request->start_date;
            $timeline->end_date = $request->end_date;
            $timeline->description = trim($request->description);
            $timeline->timeline_type = $request->timeline_type;

            if (!empty($request->email))
            {
                ReferenceEmail::where('timeline_id', $timeline->id)->delete();
                foreach ($request->email as $s)
                {
                    if (empty($s))
                    {
                        continue;
                    }

                    $email = new ReferenceEmail();
                    $email->timeline_id = $timeline->id;
                    $email->email = $s;
                    $email->save();
                }
            }

            $timeline->save();

            if ($request->timeline_type == 0)
            {
                return redirect()->route('job.seeker.timeline')->with('success', 'Timeline successfully updated');
            }elseif($request->timeline_type == 1){
                return redirect()->route('job.seeker.academic.timeline')->with('success', 'Timeline successfully updated');
            }

        }

        return view('frontend.dashboard.timeline.edit_timeline')->with(compact('timeline'));

    }

    public function deleteTimeline($id)
    {
        $timeline = Timeline::where('id', $id)->where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->first();

        if (count($timeline) == 0)
        {
            return redirect()->back()->with('error', 'Could not delete timeline');
        }

        $timeline->delete();
        return redirect()->back()->with('success', 'Timeline event successfully deleted');


    }

    public function achievements(Request $request)
    {
        $achievements = Achievement::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
        if ($request->isMethod('post'))
        {
            //dd($request->all());

            if (!empty($request->achievement))
            {
                    $achievements = new Achievement();
                    $achievements->job_seeker_id = Auth::guard('job_seeker')->user()->id;
                    $achievements->achievement = $request->achievement;
                    $achievements->date = $request->date;
                    $achievements->save();

                    return redirect()->route('job.seeker.achievements')->with('success', 'Achievement successfully added');
            }

        }
        return view('frontend.dashboard.achievements.achievements')->with(compact('achievements'));
    }

    public function editAchievement(Request $request, $id)
    {
        $achievement = Achievement::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)
            ->where('id', $id)
            ->FirstOrFail();

        if ($request->isMethod('post'))
        {
            //dd($request->all());
            $achievement->job_seeker_id = Auth::guard('job_seeker')->user()->id;
            $achievement->achievement = $request->achievement;
            $achievement->date = $request->date;
            $achievement->save();

            return redirect()->route('job.seeker.achievements')->with('success', 'Achievement successfully Updated');

        }

        return view('frontend.dashboard.achievements.edit_achievement')->with(compact('achievement'));


    }

    public function deleteAchievement($id)
    {
        $achievement = Achievement::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->where('id', $id)->delete();
        return redirect()->route('job.seeker.achievements')->with('success', 'Achievement successfully deleted');
    }

    public function viewAppliedJobs()
    {
        $appliedJobs = JobPostActivity::with('company', 'jobSeeker', 'jobPost')
            ->where('job_seeker_id', Auth::guard('job_seeker')->user()->id)
            ->latest()
            ->get();


        return view('frontend.dashboard.job_post.view_applied_job_post')->with(compact('appliedJobs'));
    }

}
