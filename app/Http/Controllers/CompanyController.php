<?php

namespace App\Http\Controllers;

use App\Company;
use App\JobCategory;
use App\JobPost;
use App\JobPostActivity;
use App\JobSeeker;
use App\Mail\ApplicationStatusChange;
use App\Mail\CompanyVerifyEmail;
use App\Mail\JobRecommendation;
use App\Mail\JobSeekerVerifyEmail;
use App\Mail\SendMessageToCandidate;
use App\Notifications\ApplicationStatusChangeNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Image;

class CompanyController extends Controller
{
    public function login(Request $request){

        if($request->isMethod('post')){
            $data = $request->input();
            if(Auth::guard('company')->attempt(['email' => $data['email'], 'password' => $data['password']])){
                //dd(Auth::guard()->check());
                return redirect()->back();
            } elseif (Auth::guard('job_seeker')->attempt(['email' => $data['email'], 'password' => $data['password']]))
            {
                return redirect()->back();
            }
            else {
                return redirect()->back()->with('error', 'Invalid Username or Password');
            }
        }

       return redirect()->back();


    }

    public function logout(){
        //Session::flush();
        Auth::guard('company')->logout();
        return redirect()->route('index')->with('flash_message_success', 'Logout Successful');
    }

    public function register(Request $request)
    {
        $data = $request->all();
        $sameEmailCount1 = JobSeeker::where('email', $request->email)->count();
        $sameEmailCount2 = Company::where('email', $request->email)->count();
        if ($sameEmailCount1 > 0 || $sameEmailCount2 > 0)
        {
            return redirect()->back()->with('error', 'User with that email already exists');
        }

        if ($request->account_type == 0)
        {
            $jobSeeker = new JobSeeker();
            $jobSeeker->profile_image = "default_avatar.jpg";
            $jobSeeker->name = $request->name;
            $jobSeeker->email = $request->email;
            $jobSeeker->contact_number = $request->contact_number;
            $jobSeeker->password = Hash::make($request->password);
            $jobSeeker->verification_token= Str::random(40);
            $jobSeeker->save();

            Mail::to($request->email)->send(new JobSeekerVerifyEmail($jobSeeker));
            Auth::guard('job_seeker')->attempt(['email' => $data['email'], 'password' => $data['password']]);
            return redirect()->back()->with('success', 'JobSeeker Account successfully created');

        }elseif ($request->account_type == 1)
        {
            $company = new Company();
            $company->profile_image = "default_building.png";
            $company->name = $request->name;
            $company->email = $request->email;
            $company->contact_number = $request->contact_number;
            $company->password = Hash::make($request->password);
            $company->verification_token= Str::random(40);
            $company->save();

            Mail::to($request->email)->send(new CompanyVerifyEmail($company));
            Auth::guard('company')->attempt(['email' => $data['email'], 'password' => $data['password']]);
            return redirect()->back()->with('success', 'Employer Account successfully created');
        }
    }

    public function sendVerificationEmailDone($email, $verificationToken)
    {
        $company = Company::where('email', $email)->where('verification_token', $verificationToken)->first();
        if ($company)
        {
            $company->verification_status = 1;
            $company->verification_token = null;
            $company->save();

            //log the user in
            Auth::guard('company')->loginUsingId($company->id);

            return redirect()->route('index')->with('success', 'Email successfully verified');

        }else{
            return redirect()->back()->with('error', 'User does not exist');

        }
    }

    public function dashboard()
    {
        $company_id = Auth::guard('company')->user()->id;
        $company = Company::where('id', $company_id)->firstOrFail();

        $expiredJobPostCount = JobPost::where('company_id',$company_id )->where('job_active', 0)->orWhere('job_status', 0)->count();
        $activeJobPostCount = JobPost::where('company_id',$company_id )->where('job_active', 1)->where('job_status', 1)->count();
        $candidatesCount = JobPostActivity::where('company_id', $company_id)->count();

        $recentJobPosts = JobPost::with('category')->where('company_id', Auth::guard('company')->user()->id)->latest()->get();

        $jobPosts = JobPost::where('company_id',$company_id )->where('job_active', 1)->where('job_status', 1)->latest()->pluck('job_title')->toArray();
        $jobPosts = json_encode($jobPosts, JSON_NUMERIC_CHECK);

        $jobPostData = JobPost::where('company_id',$company_id )->where('job_active', 1)->where('job_status', 1)->latest()->pluck('candidates')->toArray();


        if (empty($jobPostData)){
            $maxData = 1;
        }else{
            $maxData = max($jobPostData) + 1;
        }

        $jobPostData = json_encode($jobPostData, JSON_NUMERIC_CHECK);

        //dd($jobPosts);

        return view('frontend.dashboard.company_dashboard')->with(compact('maxData','jobPosts','jobPostData','company','expiredJobPostCount', 'candidatesCount', 'activeJobPostCount', 'recentJobPosts'));
    }

    public function setting(Request $request)
    {
        $company = Company::where('id', Auth::guard('company')->user()->id)->first();
        $jobCategories = JobCategory::where('status', 1)->get();
        if ($request->isMethod('post'))
        {

            $this->validate($request,[
                'name' => 'required',
                'contact_number' => 'required',
                'company_site' => 'nullable|url',
                'linkedin' => 'nullable|url',
                'profile_image' => 'nullable|image',
                'cover_image' => 'nullable|image',
                'email' =>'required|email|unique:job_seekers',
            ]);



            ini_set('memory_limit','256M');
            if($request->hasFile('profile_image')){
                $image_tmp = Input::file('profile_image');
                if($image_tmp->isValid()){
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(7777,999999).'.'.$extension;

                    $image_path = 'public/company/images/profile/'.$filename;


                    Image::make($image_tmp)->save($image_path);
                    //remove old image
                    if ($company->profile_image != "default_building.png"){
                        if (!empty($company->profile_image))
                        {
                            if (file_exists('public/company/images/profile/'. $company->profile_image)) {
                                unlink('public/company/images/profile/'. $company->profile_image);
                            }
                        }
                    }



                }
            }else{
                if (empty($company->profile_image))
                {
                    $filename = "default_avatar.jpg";
                }else{
                    $filename = $company->profile_image;
                }
            }


            if($request->hasFile('cover_image')){
                $cover_image_tmp = Input::file('cover_image');
                if($cover_image_tmp->isValid()){
                    $extension = $cover_image_tmp->getClientOriginalExtension();
                    $cover_filename = rand(7777,999999).'.'.$extension;

                    $cover_image_path = 'public/company/images/cover/'.$cover_filename;


                    Image::make($cover_image_tmp)->save($cover_image_path);
                    //remove old image
                    if (file_exists('public/company/images/cover/'. $company->cover_image)) {
                        unlink('public/company/images/cover/'. $company->cover_image);
                    }
                }
            }else{
                if (empty($company->cover_image))
                {
                    $cover_filename = "default_avatar.jpg";
                }else{
                    $cover_filename = $company->cover_image;
                }
            }

            $company->profile_image = $filename;
            $company->cover_image = $cover_filename;
            $company->name = $request->name;
            $company->email = $request->email;
            $company->contact_number = $request->contact_number;
            $company->pin_number = $request->pin_number;
            $company->address = $request->address;
            $company->established_date = $request->established_date;
            $company->category = $request->job_category;
            $company->number_of_employees = $request->number_of_employees;
            $company->company_site = $request->company_site;
            $company->linkedin = $request->linkedin;
            $company->lat = $request->lat;
            $company->lng = $request->lng;
            $company->description = $request->description;
            if (!empty($request->current_password) && !empty($request->new_password) && !empty($request->confirm_password))
            {
                if ($request->new_password = $request->confirm_password)
                {
                    if (Hash::check($request->current_password, $company->password))
                    {
                        $company->password = bcrypt($request->new_password);
                    }
                }
            }
            $company->save();

            return redirect()->route('company.setting')->with('success', 'Profile successfully updated');

        }
        return view('frontend.dashboard.setting.company_setting')->with(compact('company', 'jobCategories'));

    }

    public function chkUserPassword(Request $request)
    {
        $data = $request->all();

        $current_password = $data['current_password'];
        $user_id = Auth::guard('company')->user()->id;
        $check_password = Company::where('id', $user_id)->first();
        if (Hash::check($current_password, $check_password->password)){
            echo "true"; die;
        }else{
            echo "false"; die;
        }
    }

    public function postJob(Request $request)
    {
        if (Auth::guard('company')->user()->verification_status == 0)
        {
            return redirect()->back()->with('error', 'Verify email to post a job');
        }

        if (Auth::guard('company')->user()->status == 0)
        {
            return redirect()->back()->with('error', 'Account has not been activated by admin');
        }

        if ($request->isMethod('post'))
        {
            //dd($request->all());

            if (empty($request->job_description))
            {
                $request->job_description = '';
            }

            if (empty($request->show_salary)){
                $request->show_salary = 0;
            }else{
                $request->show_salary = 1;
            }

            $sameTitle = JobPost::where('job_title',$request->job_title )->count();
            if ($sameTitle > 0 )
            {
                $jobDetail = JobPost::where('job_title', $request->job_title)->first();
                $jobDetail->company_id = $request->company_id;
                $jobDetail->job_title = $request->job_title;
                $jobDetail->job_type = $request->job_type;
                $jobDetail->job_category = $request->job_category;
                $jobDetail->job_location = $request->job_location;
                $jobDetail->job_publish_date = date('Y-m-d', strtotime($request->job_publish_date));
                $jobDetail->job_expiry_date = date('Y-m-d', strtotime($request->job_expiry_date));
                $jobDetail->job_vacancy_number = $request->job_vacancy_number;
                $jobDetail->min_job_salary = $request->min_job_salary;
                $jobDetail->max_job_salary = $request->max_job_salary;
                $jobDetail->show_salary = $request->show_salary;
                $jobDetail->job_description = $request->job_description;
                $jobDetail->job_active = 1; //if expiry date is updated
                $jobDetail->job_status = 0;
                $jobDetail->save();


                return redirect()->route('manage.job')->with('success', 'Job Post successfully updated');

            }

            //dd($request->all());
            $jobPost = new JobPost();
            $jobPost->company_id = $request->company_id;
            $jobPost->job_title = $request->job_title;
            $jobPost->job_type = $request->job_type;
            $jobPost->job_category = $request->job_category;
            $jobPost->job_location = $request->job_location;
            $jobPost->job_publish_date = date('Y-m-d', strtotime($request->job_publish_date));
            $jobPost->job_expiry_date = date('Y-m-d', strtotime($request->job_expiry_date));
            $jobPost->job_vacancy_number = $request->job_vacancy_number;
            $jobPost->min_job_salary = $request->min_job_salary;
            $jobPost->max_job_salary = $request->max_job_salary;
            $jobPost->show_salary = $request->show_salary;
            $jobPost->job_description = $request->job_description;
            $jobPost->save();




            return redirect()->route('manage.job')->with('success', 'Job successfully posted');
        }
        $jobTypes = DB::table('job_types')->where('status', 1)->latest()->get();
        $jobCategories = JobCategory::where('status', 1)->get();
        return view('frontend.dashboard.job_post.post_job')->with(compact('jobCategories', 'jobTypes'));
    }

    public function editJobPost(Request $request, $id)
    {
        if (Auth::guard('company')->user()->verification_status == 0)
        {
            return redirect()->back()->with('error', 'Verify email to post a job');
        }

        if (Auth::guard('company')->user()->status == 0)
        {
            return redirect()->back()->with('error', 'Account has not been activated by admin');
        }

        $job = JobPost::where('id', $id)->where('company_id', Auth::guard('company')->user()->id)->firstOrFail();
        if ($request->isMethod('post'))
        {
            if (empty($request->job_description))
            {
                $request->job_description = '';
            }

            if (empty($request->show_salary)){
                $request->show_salary = 0;
            }else{
                $request->show_salary = 1;
            }

            $jobDetail = JobPost::where('id', $id)->first();
            $jobDetail->company_id = $request->company_id;
            $jobDetail->job_title = $request->job_title;
            $jobDetail->job_type = $request->job_type;
            $jobDetail->job_category = $request->job_category;
            $jobDetail->job_location = $request->job_location;
            $jobDetail->job_publish_date = date('Y-m-d', strtotime($request->job_publish_date));
            $jobDetail->job_expiry_date = date('Y-m-d', strtotime($request->job_expiry_date));
            $jobDetail->job_vacancy_number = $request->job_vacancy_number;
            $jobDetail->min_job_salary = $request->min_job_salary;
            $jobDetail->max_job_salary = $request->max_job_salary;
            $jobDetail->show_salary = $request->max_job_salary;
            $jobDetail->job_description = $request->job_description;
            $jobDetail->job_active = 1; //if expiry date is updated
            $jobDetail->save();

            return redirect()->route('manage.job')->with('success', 'Job successfully updated');

        }

        $jobTypes = DB::table('job_types')->where('status', 1)->get();
        $jobCategories = JobCategory::where('status', 1)->get();
        return view('frontend.dashboard.job_post.edit_post_job')->with(compact('job', 'jobTypes', 'jobCategories'));
    }

    public function manageJobs()
    {
        if (Auth::guard('company')->user()->verification_status == 0)
        {
            return redirect()->back()->with('error', 'Verify email to post a job');
        }

        if (Auth::guard('company')->user()->status == 0)
        {
            return redirect()->back()->with('error', 'Account has not been activated by admin');
        }

        $jobs = JobPost::where('company_id', Auth::guard('company')->user()->id)->latest()->get();
        return view('frontend.dashboard.job_post.manage_jobs')->with(compact('jobs'));
    }

    public function deleteJobPost($id)
    {
        if (Auth::guard('company')->user()->verification_status == 0)
        {
            return redirect()->back()->with('error', 'Verify email to continue');
        }

        if (Auth::guard('company')->user()->status == 0)
        {
            return redirect()->back()->with('error', 'Account has not been activated by admin');
        }

        $job = JobPost::findOrFail($id);
        $job->job_status = 2;
        /*$jobActivities = JobPostActivity::where('job_post_id', $id)->get();
        foreach ($jobActivities as $activity)
        {
            $activity->delete();
        }*/
        $job->save();

        return redirect()->back()->with('error', 'Job successfully deactivated');
    }

    public function manageCandidates($id)
    {
        $job = JobPost::where('id', $id)->first();
        $candidates = JobPostActivity::with('company', 'jobPost', 'jobSeeker')
            ->where('job_post_id', $id)
            ->where('company_id', Auth::guard('company')->user()->id)
            ->where('application_status', '!=', 2)
            ->latest()
            ->get();

        return view('frontend.dashboard.candidates.manage_candidates')->with(compact('candidates', 'job'));
    }

    public function manageAllCandidates()
    {
        $candidates = JobPostActivity::with('company', 'jobPost', 'jobSeeker')
            ->where('company_id', Auth::guard('company')->user()->id)
            ->where('application_status', '!=', 2)
            ->latest()
            ->get();

        return view('frontend.dashboard.candidates.manage_all_candidates')->with(compact('candidates', 'job'));
    }

    public function sendMessage(Request $request ,$id)
    {
        dd($request->all());
    }

    public function sendCandidateEmail(Request $request, $id)
    {
        $candidate = JobSeeker::where('id', $id)->FirstOrFail();
        if ($request->isMethod('post'))
        {
            if (empty($request->message))
            {
                return redirect()->back()->with('error', 'Message field is empty');
            }

            $company = Company::where('id', Auth::guard('company')->user()->id)->FirstOrFail();

            Mail::to($candidate)->send(new SendMessageToCandidate($company, $request));

            return redirect()->route('manage.all.candidates')->with('success', 'Mail successfully sent');
        }

        return view('frontend.dashboard.candidates.send_email_to_candidate')->with(compact('candidate'));
    }

    public function deleteCandidate($id)
    {
        $jobActivity = JobPostActivity::with('jobSeeker', 'jobPost')
            ->where('id', $id)
            ->FirstOrFail();


        $jobActivity->application_status = 2;
        $jobActivity->save();

        $job = JobPost::where('id', $jobActivity->job_post_id)->first();
        $job->candidates = $job->candidates -1;
        $job->save();

        $user = JobSeeker::where('id', $jobActivity->job_seeker_id)->first();
        Notification::send($user, new ApplicationStatusChangeNotification(Company::where('id', $jobActivity->company_id )->first(), JobPost::where('id', $jobActivity->job_post_id)->first(), $jobActivity));
        Mail::to($user)->send(new ApplicationStatusChange($jobActivity));



        return redirect()->back()->with('success', 'Candidate  successfully removed');
    }

    public function acceptCandidate($id)
    {
        $jobActivity = JobPostActivity::
        where('id', $id)
            ->FirstOrFail();

        if ( $jobActivity->application_status == 0)
        {
            $jobActivity->application_status = 1;
            $jobActivity->save();


            $user = JobSeeker::where('id', $jobActivity->job_seeker_id)->first();
            Notification::send($user, new ApplicationStatusChangeNotification(Company::where('id', $jobActivity->company_id )->first(), JobPost::where('id', $jobActivity->job_post_id)->first(), $jobActivity));
            Mail::to($user)->send(new ApplicationStatusChange($jobActivity));

            return redirect()->back()->with('success', 'Candidate application successfully accepted');


        }elseif ($jobActivity->application_status == 1)
        {
            $jobActivity->application_status = 0;
            $jobActivity->save();

            $user = JobSeeker::where('id', $jobActivity->job_seeker_id)->first();
            Notification::send($user, new ApplicationStatusChangeNotification(Company::where('id', $jobActivity->company_id )->first(), JobPost::where('id', $jobActivity->job_post_id)->first(), $jobActivity));
            Mail::to($user)->send(new ApplicationStatusChange($jobActivity));

            return redirect()->back()->with('success', 'Candidate application successfully unselected');

        }



    }

    public function adminCompanyView()
    {
        $companies = Company::with('jobPosts')->latest()->get();

        return view('admin.company.view_companies')->with(compact('companies'));
    }

    public function adminChangeCompanyStatus(Request $request)
    {
        $company = Company::where('id', $request->company_id)->FirstOrFail();
        $company->status = $request->status;
        $company->save();

        return redirect()->route('view.company')->with('success', 'Account Status successfully updated');

    }
}
