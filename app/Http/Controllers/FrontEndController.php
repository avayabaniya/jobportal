<?php

namespace App\Http\Controllers;

use App\Company;
use App\JobBookmark;
use App\JobCategory;
use App\JobPost;
use App\JobPostActivity;
use App\JobSeeker;
use App\JobType;
use App\Mail\CompanyApplyJob;
use App\Mail\JobSeekerApplyJob;
use App\Notifications\JobApplyNotification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use PhpParser\ErrorHandler\Collecting;

class FrontEndController extends Controller
{
    public function index()
    {

        $jobs = JobPost::with('company', 'category', 'jobType')->where('job_status', 1)->where('job_active', 1)->take(5)->latest()->get();
        $allJobs = JobPost::with('company', 'category', 'jobType')->where('job_status', 1)->where('job_active', 1)->latest()->get();
        $categories = JobCategory::where('status', 1)->get();

        $totalJobPosted = JobPost::where('job_status', 1)->where('job_active', 1)->count();
        $totalCompanies = Company::where('verification_status', 1)->where('status', 1)->count();
        $totalJobSeekers = JobSeeker::count();
        $page = 1;
        //dd($jobs);

        return view('frontend.index')->with(compact('jobs', 'categories', 'allJobs', 'totalCompanies', 'totalJobPosted', 'totalJobSeekers'));
    }

    public function register(Request $request)
    {
        if ($request->isMethod('post'))
        {
            dd($request->all());
        }

        return view('frontend.register.register');
    }

    public function JobPostDetail($id)
    {
        $postJobActivity = 0;
        $jobDetail = JobPost::with('company', 'category', 'jobType')->where('id', $id)->firstOrFail();
        $similarJobs = JobPost::with('company', 'category', 'jobType')->where('id','!=',$jobDetail->id)->where('job_category', $jobDetail->job_category)->where('job_status', 1)->where('job_active', 1)->take(2)->get();
        if (Auth::guard('job_seeker')->check()){
            $jobBookmarks = JobBookmark::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
            $postJobActivity = JobPostActivity::where('job_post_id', $id)->where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->count();
        }
        //dd($similarJobs);

        return view('frontend.job_posts.job_details')->with(compact('jobDetail', 'jobBookmarks', 'similarJobs', 'postJobActivity'));
    }

    public function applyJob(Request $request, $id)
    {
        if (Auth::guard('job_seeker')->check())
        {
            $job = JobPost::with('company')->where('id', $id)->first();

            $appliedJobCount = JobPostActivity::where('job_post_id', $id)
                ->where('job_seeker_id', Auth::guard('job_seeker')->user()->id)
                ->where('company_id',$job->company_id )
                ->count();

            if ($appliedJobCount > 0)
            {
                return redirect()->back()->with('error', 'You have already applied for this job');
            }

            $jobPostActivity = new JobPostActivity();
            $jobPostActivity->job_post_id = $id;
            $jobPostActivity->job_seeker_id = Auth::guard('job_seeker')->user()->id;
            $jobPostActivity->company_id = $job->company_id;
            $jobPostActivity->applied_date = Carbon::now();
            $jobPostActivity->save();
            $job->candidates = $job->candidates + 1;
            $job->save();

            $user = Company::where('id', $job->company_id)->first();
            $jobSeeker = JobSeeker::where('id', Auth::guard('job_seeker')->user()->id)->first();

            //send notification
            $jobseekerId = Auth::guard('job_seeker')->user()->id;
            Notification::send($user,
                new JobApplyNotification(JobSeeker::where('id', $jobseekerId)->first(), JobPost::where('id', $id)->first()));

            Mail::to($user)->send(new CompanyApplyJob($jobPostActivity));
            Mail::to($jobSeeker)->send(new JobSeekerApplyJob($jobPostActivity));


            return redirect()->back()->with('success', 'Successfully applied for job');

        }else{
            return redirect()->back()->with('error', 'Please log in to apply for job');
        }
    }

    public function viewJobListing()
    {
        $jobs = JobPost::with('company', 'category', 'jobType')->where('job_status', 1)->where('job_active', 1)->orderBy('updated_at', 'DESC')->paginate(4);
        $categories = JobCategory::where('status', 1)->get();
        $jobTypes = JobType::where('status',1)->get();
        if (Auth::guard('job_seeker')->check()){
            $c_id = Auth::guard('job_seeker')->user()->preferred_job_category_id;
            $jobBookmarks = JobBookmark::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
            /*$jobs1 = JobPost::with('company', 'category', 'jobType')
                ->where('job_status', 1)
                ->where('job_active', 1)
                ->where('job_category', $c_id)
                ->get();

            $jobs2 = JobPost::with('company', 'category', 'jobType')
                ->where('job_status', 1)
                ->where('job_active', 1)
                ->where('job_category','!=', $c_id)
                ->get();

            $jobs = $jobs1->merge($jobs2);*/
        }

        //dd($jobBookmarks);
        return view('frontend.job_posts.job_list')->with(compact('jobs', 'categories', 'jobTypes', 'jobBookmarks'));
    }

    public function viewCategoryJobListing($id)
    {
        $jobs = JobPost::with('company', 'category', 'jobType')->where('job_status', 1)
            ->where('job_active', 1)
            ->where('job_category', $id)
            ->orderBy('updated_at', 'DESC')
            ->paginate(4);
        $categories = JobCategory::where('status', 1)->get();
        $jobTypes = JobType::where('status',1)->get();
        if (Auth::guard('job_seeker')->check()){
            $jobBookmarks = JobBookmark::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
        }
        $selectedCategory = JobCategory::where('id', $id)->FirstOrFail();

        //dd($jobBookmarks);
        return view('frontend.job_posts.category_job_list')->with(compact('jobs', 'categories', 'jobTypes', 'jobBookmarks', 'selectedCategory'));
    }

    public function JobSeekerDetails($id)
    {
        $jobSeeker = JobSeeker::with('timeline', 'jobPostActivity', 'jobBookmark', 'skill')->where('id',$id)->firstOrFail();
        if (Auth::guard('job_seeker')->check()){
            $jobBookmarks = JobBookmark::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
        }
        return view('frontend.job_seeker.job_seeker_detail')->with(compact('jobSeeker', 'jobBookmarks'));

    }

    public function CompanyDetails($id)
    {
       $company = Company::with('jobPosts', 'jobPostActivity', 'category')->where('id', $id)->firstOrFail();
       $jobs = JobPost::with('company', 'category', 'jobType')->where('company_id', $id)->where('job_status', 1)->where('job_active', 1)->take(5)->latest()->get();

        if (Auth::guard('job_seeker')->check()){
            $jobBookmarks = JobBookmark::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
        }
       return view('frontend.company.company_detail')->with(compact('company','jobBookmarks', 'jobs'));

    }

    public function viewCandidateListing()
    {
        $candidates = JobSeeker::with('jobPostActivity', 'jobBookmark', 'timeline', 'skill', 'achievement')
            ->where('verification_status', 1)
            ->orderBy('updated_at', 'DESC')
            ->paginate(4);

        $categories = JobCategory::where('status', 1)->get();
        $jobTypes = JobType::where('status',1)->get();

        if (Auth::guard('job_seeker')->check()){
            $jobBookmarks = JobBookmark::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
        }

        //dd($jobBookmarks);
        return view('frontend.job_seeker.candidate_list')->with(compact('candidates', 'categories', 'jobTypes', 'jobBookmarks'));
    }
}
