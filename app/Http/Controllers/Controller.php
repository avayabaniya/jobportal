<?php

namespace App\Http\Controllers;

use App\Company;
use App\JobBookmark;
use App\JobPost;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function companyJobCount()
    {
        if (Auth::guard('company')->check())
        {
            $company_id = Auth::guard('company')->user()->id;
            $jobCount = JobPost::where('company_id',$company_id )->count();
            return $jobCount;
        }
        return null;

    }

    public static function pendingJobPostsCount()
    {
        $pendingJobPostsCount = JobPost::where('job_status', 0)->count();
        return $pendingJobPostsCount;
    }

    public static function pendingCompanyCount()
    {
        $pendingCompanyCount = Company::where('status', 0)->count();
        return $pendingCompanyCount;
    }

    public static function jobBookmarkCount()
    {
        /*$user_id = Auth::guard('job_seeker')->user()->id;

        $jobBookmarkCount = JobBookmark::where('job_seeker_id', $user_id)->count();

        if ($jobBookmarkCount == 0)
        {
            $jobBookmarkCount = null;
        }
        return $jobBookmarkCount;*/
        return null;
    }
}
