<?php

namespace App\Http\Controllers;

use App\JobBookmark;
use App\JobCategory;
use App\JobPost;
use App\JobSeeker;
use App\JobType;
use App\Skill;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class SearchController extends Controller
{
    public function searchJob(Request $request)
    {

        if (empty($request->q) && empty($request->job_location) && empty($request->category))
        {
            return redirect()->route('job.listing');
        }

        $q = Input::get( 'q' );

        if (empty($request->sort))
        {
            $request->sort = "latest";
        }

        if (empty($request->location) && empty($request->category))
        {
            if($q != ""){

                if ($request->sort == "oldest")
                {
                    $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                        ->where('job_status', 1)
                        ->where('job_active', 1)
                        ->where(function ($query) use ($q) {
                            $query->where ( 'job_title', 'LIKE', '%' . $q . '%' )
                                ->orWhere( 'job_description', 'LIKE', '%' . $q . '%' );
                        })
                        ->orderBy('updated_at', 'ASC')
                        ->paginate (6)
                        ->setpath('');

                    $jobs->appends(array(
                        'q' => $request->q,
                        'sort' => $request->sort,

                    ));

                }else{
                    $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                        ->where(function ($query) use ($q) {
                            $query->where ( 'job_title', 'LIKE', '%' . $q . '%' )
                                ->orWhere( 'job_description', 'LIKE', '%' . $q . '%' );
                        })
                        ->where('job_status', 1)
                        ->where('job_active', 1)
                        ->orderBy('updated_at', 'DESC')
                        ->paginate (6)
                        ->setpath('');

                    $jobs->appends(array(
                        'q' => $request->q,
                        'sort' => $request->sort,

                    ));

                }


                $searchKeyword = $q;
            }

        }
        elseif (empty($request->location) && empty($request->q))
        {

            if ($request->sort == "oldest")
            {
                $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                    ->where('job_active', 1)
                    ->where('job_status', 1)
                    ->whereIn('job_category',$request->category)
                    ->orderBy('updated_at', 'ASC')
                    ->paginate (4)
                    ->setpath('');

                $jobs->appends(array(
                    'category' => $request->category,
                    'sort' => $request->sort,

                ));
            }else{
                $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                    ->where('job_active', 1)
                    ->where('job_status', 1)
                    ->whereIn('job_category',$request->category)
                    ->orderBy('updated_at', 'DESC')
                    ->paginate (4)
                    ->setpath('');

                $jobs->appends(array(
                    'category' => $request->category,
                    'sort' => $request->sort,

                ));
            }

            $cat = "";
            foreach (JobCategory::where('status', 1)->get() as $c)
            {
                if ( in_array($c->id, $request->category))
                {
                    $cat = $c->category_name.", ".$cat;
                }
            }

                $searchKeyword = $cat;

        }
        elseif (empty($request->category) && empty($request->q))
        {
            if ($request->sort == "oldest")
            {
                $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                    ->where('job_active', 1)
                    ->where('job_status', 1)
                    ->where('job_location', $request->location)
                    ->orderBy('updated_at', 'ASC')
                    ->paginate (4)
                    ->setpath('');

                $jobs->appends(array(
                    'location' => $request->location,
                    'q' => $request->q,
                    'sort' => $request->sort,
                ));
            }else{
                $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                    ->where('job_active', 1)
                    ->where('job_status', 1)
                    ->where('job_location', $request->location)
                    ->orderBy('updated_at', 'DESC')
                    ->paginate (4)
                    ->setpath('');

                $jobs->appends(array(
                    'location' => $request->location,
                    'q' => $request->q,
                    'sort' => $request->sort,
                ));
            }

            $searchKeyword = $request->location;
        }
        elseif (empty($request->location))
        {
            if($q != ""){
                if ($request->sort == "oldest")
                {
                    $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                        ->where('job_active', 1)
                        ->where('job_status', 1)
                        ->whereIn('job_category',$request->category)
                        ->where(function ($query) use ($q) {
                            $query->where ( 'job_title', 'LIKE', '%' . $q . '%' )
                                ->orWhere( 'job_description', 'LIKE', '%' . $q . '%' );
                        })
                        ->orderBy('updated_at', 'ASC')
                        ->paginate (4)
                        ->setpath('');

                    $jobs->appends(array(
                        'category' => $request->category,
                        'q' => $request->q,
                        'sort' => $request->sort,
                    ));
                }else{
                    $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                        ->where('job_active', 1)
                        ->where('job_status', 1)
                        ->whereIn('job_category',$request->category)
                        ->where(function ($query) use ($q) {
                            $query->where ( 'job_title', 'LIKE', '%' . $q . '%' )
                                ->orWhere( 'job_description', 'LIKE', '%' . $q . '%' );
                        })
                        ->orderBy('updated_at', 'DESC')
                        ->paginate (4)
                        ->setpath('');

                    $jobs->appends(array(
                        'category' => $request->category,
                        'q' => $request->q,
                        'sort' => $request->sort,
                    ));
                }
                $cat = "";
                foreach (JobCategory::where('status', 1)->get() as $c)
                {
                    if ( in_array($c->id, $request->category))
                    {
                        $cat = $c->category_name.", ".$cat;
                    }
                }

                $searchKeyword = $cat. " and ". $q;

            }
        }
        elseif (empty($request->category))
        {
            if($q != ""){
                if ($request->sort == "oldest")
                {
                    $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                        ->where('job_active', 1)
                        ->where('job_status', 1)
                        ->where('job_location', $request->location)
                        ->where(function ($query) use ($q) {
                            $query->where ( 'job_title', 'LIKE', '%' . $q . '%' )
                                ->orWhere( 'job_description', 'LIKE', '%' . $q . '%' );
                        })
                        ->orderBy('updated_at', 'ASC')
                        ->paginate (4)
                        ->setpath('');

                    $jobs->appends(array(
                        'location' => $request->location,
                        'q' => $request->q,
                        'sort' => $request->sort,
                    ));
                }else{
                    $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                        ->where('job_active', 1)
                        ->where('job_status', 1)
                        ->where('job_location', $request->location)
                        ->where(function ($query) use ($q) {
                            $query->where ( 'job_title', 'LIKE', '%' . $q . '%' )
                                ->orWhere( 'job_description', 'LIKE', '%' . $q . '%' );
                        })
                        ->orderBy('updated_at', 'DESC')
                        ->paginate (4)
                        ->setpath('');

                    $jobs->appends(array(
                        'location' => $request->location,
                        'q' => $request->q,
                        'sort' => $request->sort,
                    ));
                }


                $searchKeyword = $request->location. " and ". $q;

            }
        }
        else
        {
            if($q != ""){

                if ($request->sort == "oldest")
                {
                    $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                        ->where('job_active', 1)
                        ->where('job_status', 1)
                        ->where('job_location', $request->location)
                        ->whereIn('job_category',$request->category)
                        ->where(function ($query) use ($q) {
                            $query->where ( 'job_title', 'LIKE', '%' . $q . '%' )
                                ->orWhere( 'job_description', 'LIKE', '%' . $q . '%' );
                        })
                        ->orderBy('updated_at', 'ASC')
                        ->paginate (4)
                        ->setpath('');

                    $jobs->appends(array(
                        'location' => $request->location,
                        'category' => $request->category,
                        'q' => $request->q,
                        'sort' => $request->sort,
                    ));
                }else{
                    $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                        ->where('job_active', 1)
                        ->where('job_status', 1)
                        ->where('job_location', $request->location)
                        ->whereIn('job_category',$request->category)
                        ->where(function ($query) use ($q) {
                            $query->where ( 'job_title', 'LIKE', '%' . $q . '%' )
                                ->orWhere( 'job_description', 'LIKE', '%' . $q . '%' );
                        })
                        ->orderBy('updated_at', 'DESC')
                        ->paginate (4)
                        ->setpath('');

                    $jobs->appends(array(
                        'location' => $request->location,
                        'category' => $request->category,
                        'q' => $request->q,
                        'sort' => $request->sort,
                    ));
                }

                $cat = "";
                foreach (JobCategory::where('status', 1)->get() as $c)
                {
                    if ( in_array($c->id, $request->category))
                    {
                        $cat = $c->category_name.", ".$cat;
                    }
                }
                $searchKeyword = $request->location.", ".$cat. " and ". $q;

            }else{
                if ($request->sort == "oldest")
                {
                    $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                        ->where('job_active', 1)
                        ->where('job_status', 1)
                        ->where('job_location', $request->location)
                        ->whereIn('job_category',$request->category)
                        ->orderBy('updated_at', 'ASC')
                        ->paginate (4)
                        ->setpath('');

                    $jobs->appends(array(
                        'location' => $request->location,
                        'category' => $request->category,
                        'q' => $request->q,
                        'sort' => $request->sort,
                    ));
                }else{
                    $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                        ->where('job_active', 1)
                        ->where('job_status', 1)
                        ->where('job_location', $request->location)
                        ->whereIn('job_category',$request->category)
                        ->orderBy('updated_at', 'DESC')
                        ->paginate (4)
                        ->setpath('');

                    $jobs->appends(array(
                        'location' => $request->location,
                        'category' => $request->category,
                        'q' => $request->q,
                        'sort' => $request->sort,
                    ));
                }

                $cat = "";
                foreach (JobCategory::where('status', 1)->get() as $c)
                {
                    if ( in_array($c->id, $request->category))
                    {
                        $cat = $c->category_name.", ".$cat;
                    }
                }
                $searchKeyword = $request->location.", ".$cat. " and ". $q;

            }
        }



        $searchedValues = $request->all();
        $categories = JobCategory::where('status', 1)->get();
        $jobTypes = JobType::where('status', 1)->get();
        if (Auth::guard('job_seeker')->check()){
            $jobBookmarks = JobBookmark::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
        }
        if (!empty( $jobs ))
        {
            return view ( 'frontend.search.job_search' )->with(compact('searchKeyword','searchedValues','jobBookmarks', 'jobs', 'categories', 'jobTypes'));
        }else{
            return redirect()->back()->with('error', 'Jobs not found');

        }
    }


    public function filterJob(Request $request)
    {
        $salary = explode(",", $request->salary);
        //dd($salary);
        if (empty($request->sort))
        {
            $request->sort = "latest";
        }

        if (empty($request->job_category))
        {
            //dd($request->job_category);
            $jobCategories = JobCategory::select('id')->get()->toArray();

        }else{
            $jobCategories = $request->job_category;
        }

        if (empty($request->job_type))
        {
            return redirect()->back()->with('error', 'No job posting found');
            $searchKeyword = '';
            $searchedValues = $request->all();
            $categories = JobCategory::where('status', 1)->get();
            $jobTypes = JobType::where('status', 1)->get();
            if (Auth::guard('job_seeker')->check()){
                $jobBookmarks = JobBookmark::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
            }


            return view ( 'frontend.search.job_filter' )->with(compact('searchKeyword','searchedValues','jobBookmarks', 'jobs', 'categories', 'jobTypes'));

        }


        if (empty($request->job_location))
        {
            $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                ->where('job_active', 1)
                ->where('job_status', 1)
                ->whereIn('job_type', $request->job_type)
                ->where('min_job_salary', '>=', $salary[0])
                ->where('max_job_salary', '<=', $salary[1])
                ->whereIn('job_category', $jobCategories)
                ->when($request->sort == "oldest", function ($query){
                    return $query->orderBy('updated_at', 'ASC');
                })
                ->when($request->sort != "oldest", function ($query){
                    return $query->orderBy('updated_at', 'DESC');
                })
                ->paginate (4)
                ->setpath('');

            $jobs->appends(array(
                'job_location' => '',
                'job_type' =>  $request->job_type,
                'salary' => $request->salary,
                'job_category' => $jobCategories,
                'sort' => $request->sort,
            ));



        }else{
            $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                ->where('job_active', 1)
                ->where('job_status', 1)
                ->where('job_location', $request->job_location)
                ->whereIn('job_category', $jobCategories)
                ->whereIn('job_type', $request->job_type)
                ->where('min_job_salary', '>=', $salary[0])
                ->where('max_job_salary', '<=', $salary[1])
                ->when($request->sort == "oldest", function ($query){
                    return $query->orderBy('updated_at', 'ASC');
                })
                ->when($request->sort != "oldest", function ($query){
                    return $query->orderBy('updated_at', 'DESC');
                })
                ->paginate (4)
                ->setpath('');

            $jobs->appends(array(
                'job_location' =>  $request->job_location,
                'job_type' =>  $request->job_type,
                'salary' => $request->salary,
                'job_category' => $jobCategories,
                'sort' => $request->sort,
            ));
        }

        $searchKeyword = '';
        $searchedValues = $request->all();
        $categories = JobCategory::where('status', 1)->get();
        $jobTypes = JobType::where('status', 1)->get();
        if (Auth::guard('job_seeker')->check()){
            $jobBookmarks = JobBookmark::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
        }


        return view ( 'frontend.search.job_filter' )->with(compact('searchKeyword','searchedValues','jobBookmarks', 'jobs', 'categories', 'jobTypes'));

    }


    public function filterCandidate(Request $request)
    {
        //dd($request->all());
        if (empty($request->sort))
        {
            $request->sort = "latest";
        }

        if (empty($request->job_category))
        {
            $jobCategories = JobCategory::select('id')->get()->toArray();
            //dd($jobCategories);


        }else{
            $jobCategories = $request->job_category;
        }


        if (empty($request->skill))
        {
            //dd($request->job_category);
            $skills = Skill::select('skill')->distinct('skill')
                ->get()->toArray();



        }else{
            $skills = $request->skill;
        }


        if (empty($request->q))
        {
            $candidates = JobSeeker::with('jobPostActivity', 'jobBookmark', 'timeline', 'skill', 'achievement')
                ->where('verification_status', 1)
                ->whereIn('preferred_job_category_id', $jobCategories)
                ->when(!empty($request->skill), function ($query) use ($skills){
                    return $query->whereHas('skill', function ($q) use ($skills){
                        $q->whereIn('skill', $skills);
                    });
                })
                ->when($request->sort == "oldest", function ($query){
                    return $query->orderBy('updated_at', 'ASC');
                })
                ->when($request->sort != "oldest", function ($query){
                    return $query->orderBy('updated_at', 'DESC');
                })
                ->paginate (4)
                ->setpath('');

            $candidates->appends(array(
                'q' => '',
                'job_category' => $jobCategories,
                'skill' => $skills,
                'sort' => $request->sort,
            ));

        }else{
            $candidates = JobSeeker::with('jobPostActivity', 'jobBookmark', 'timeline', 'skill', 'achievement')
                ->where( 'name', 'LIKE', '%' . $request->q . '%' )
                ->where('verification_status', 1)
                ->whereIn('preferred_job_category_id', $jobCategories)
                ->whereHas('skill', function ($q) use ($skills){
                    $q->whereIn('skill', $skills);
                })
                ->when($request->sort == "oldest", function ($query){
                    return $query->orderBy('updated_at', 'ASC');
                })
                ->when($request->sort != "oldest", function ($query){
                    return $query->orderBy('updated_at', 'DESC');
                })
                ->paginate (4)
                ->setpath('');

            $candidates->appends(array(
                'q' => '',
                'job_category' => $jobCategories,
                'skill' => $skills,
                'sort' => $request->sort,
            ));
        }

        $searchKeyword = '';
        $searchedValues = $request->all();
        $categories = JobCategory::where('status', 1)->get();
        $jobTypes = JobType::where('status', 1)->get();
        if (Auth::guard('job_seeker')->check()){
            $jobBookmarks = JobBookmark::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
        }

        //dd($searchedValues['skill']);


        return view ( 'frontend.search.candidate_filter' )->with(compact('candidates','searchKeyword','searchedValues','jobBookmarks', 'jobs', 'categories', 'jobTypes'));

    }
}
