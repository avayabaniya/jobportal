<?php

namespace App\Http\Controllers;

use App\Company;
use App\JobPost;
use App\Notifications\AdminUpdateJobPostStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class JobPostController extends Controller
{
    public function jobPostView()
    {
        $jobPosts = JobPost::with('company', 'category')->latest()->get();
        //dd($jobPosts);
        return view('admin.job_post_request.job_post_request')->with(compact('jobPosts'));
    }


    public function changeJobPostStatus(Request $request)
    {
        $jobPost = JobPost::where('id', $request->job_post_id)->first();

        $jobPost->job_status = $request->status;
        $jobPost->save();

        $user = Company::where('id', $jobPost->company_id)->FirstOrFail();

        Notification::send($user, new AdminUpdateJobPostStatus($jobPost));

        return redirect()->back()->with('success', 'Job Post status successfully updated');
    }
}


