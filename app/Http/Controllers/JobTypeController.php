<?php

namespace App\Http\Controllers;

use App\JobType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobTypeController extends Controller
{
    public function addJobType(Request $request)
    {
        if ($request->isMethod('post'))
        {
            //dd($request->all());
            if (empty($request->status))
            {
                $request->status = 0;
            }else
            {
                $request->status = 1;
            }
            DB::table('job_types')->insert(['name' => $request->name, 'status' => $request->status]);
            return redirect()->route('view.job.type');
        }

        return view('admin.job_types.add_job_type');
    }

    public function viewJobType()
    {
        $jobTypes = DB::table('job_types')->latest()->get();
        return view('admin.job_types.view_job_types')->with(compact('jobTypes'));
    }

    public function editJobType(Request $request, $id)
    {
        $jobType = DB::table('job_types')->where('id', $id)->first();
        if ($request->isMethod('post'))
        {
            if (empty($request->status))
            {
                $request->status = 0;
            }else
            {
                $request->status = 1;
            }
            DB::table('job_types')
                ->where('id', $id)
                ->update(['name' => $request->name, 'status' => $request->status]);
            return redirect()->route('view.job.type');
        }
        return view('admin.job_types.edit_job_type')->with(compact('jobType'));

    }

    public function deleteJobType($id)
    {
        //DB::table('job_types')->where('id', $id)->delete();
        $jobType = JobType::where(['id' => $id])->first();
        $jobType->status = 0;
        $jobType->save();

        return redirect()->route('view.job.type')->with('success', 'Job type successfully deactivated');
    }
}
