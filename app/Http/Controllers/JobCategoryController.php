<?php

namespace App\Http\Controllers;

use App\JobCategory;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class JobCategoryController extends Controller
{
    public function addJobCategory(Request $request)
    {
        if ($request->isMethod('post'))
        {
            if (empty($request->status))
            {
                $request->status = 0;
            }else
            {
                $request->status = 1;
            }

            if($request->hasFile('image')){
                $image_tmp = Input::file('image');
                if($image_tmp->isValid()){
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(7777,999999).'.'.$extension;

                    $large_image_path = 'public/admin/uploads/job_categories/large/'.$filename;
                    $medium_image_path = 'public/admin/uploads/job_categories/medium/'.$filename;
                    $small_image_path = 'public/admin/uploads/job_categories/small/'.$filename;

                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600,600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300,300)->save($small_image_path);

                }
            }

            $jobCategory = new JobCategory();
            $jobCategory->category_name = $request->category_name;
            $jobCategory->category_description = $request->category_description;
            $jobCategory->category_image = $filename;
            $jobCategory->status = $request->status;
            $jobCategory->save();

            return redirect(route('view.job.category'));
        }

        return view('admin.job_categories.add_job_category');
    }


    public function viewJobCategory()
    {
        $jobCategories = JobCategory::latest()->get();
        return view('admin.job_categories.view_job_categories')->with(compact('jobCategories'));
    }


    public function editJobCategory(Request $request, $id)
    {
        $jobCategory = JobCategory::where('id', $id)->first();

        if ($request->isMethod('post'))
        {
            //dd($request->all());

            $data = $request->all();

            if ($request->hasFile('image')) {
                $image_tmp = Input::file('image');
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(1200, 999999) . '.' . $extension;

                    $large_image_path = 'public/admin/uploads/job_categories/large/' . $filename;
                    $medium_image_path = 'public/admin/uploads/job_categories/medium/' . $filename;
                    $small_image_path = 'public/admin/uploads/job_categories/small/' . $filename;

                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(600, 600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(300, 300)->save($small_image_path);


                    if (file_exists('public/admin/uploads/job_categories/large/' . $data['current_image'])) {
                        unlink('public/admin/uploads/job_categories/large/' . $data['current_image']);
                    }
                    if (file_exists('public/admin/uploads/job_categories/medium/' . $data['current_image'])) {
                        unlink('public/admin/uploads/job_categories/medium/' . $data['current_image']);
                    }
                    if (file_exists('public/admin/uploads/job_categories/small/' . $data['current_image'])) {
                        unlink('public/admin/uploads/job_categories/small/' . $data['current_image']);
                    }

                }
            } else {
                $filename = $data['current_image'];
            }

            if (empty($data['category_description'])) {
                $data['category_description'] = "";
            }

            if (empty($data['status'])) {
                $status = 0;
            } else {
                $status = 1;
            }

            $jobCategory->category_name = $request->category_name;
            $jobCategory->category_description = $data['category_description'];
            $jobCategory->category_image = $filename;
            $jobCategory->status = $status;
            $jobCategory->save();

            return redirect(route('view.job.category'));

        }

        return view('admin.job_categories.edit_job_category')->with(compact('jobCategory'));

    }


    public function deleteJobCategory($id)
    {
        $jobCategory = JobCategory::where(['id' => $id])->first();
        $jobCategory->status = 0;
        /*$large_image_path = 'public/admin/uploads/job_categories/large/';
        $medium_image_path = 'public/admin/uploads/job_categories/medium/';
        $small_image_path = 'public/admin/uploads/job_categories/small/';

        if (file_exists($large_image_path . $jobCategory->category_image)) {
            unlink($large_image_path . $jobCategory->category_image);
        }
        if (file_exists($small_image_path . $jobCategory->category_image)) {
            unlink($small_image_path . $jobCategory->category_image);
        }
        if (file_exists($medium_image_path . $jobCategory->category_image)) {
            unlink($medium_image_path . $jobCategory->category_image);
        }*/

        $jobCategory->save();

        return redirect(route('view.job.category'));

    }
}
