<?php

namespace App\Http\Controllers;

use App\Company;
use App\JobPost;
use App\JobPostActivity;
use App\JobSeeker;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function login(Request $request){
        if($request->isMethod('post'))
        {
            $data = $request->input();
            if(Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'admin' => '1'])){

                return redirect()->route('admin.dashboard');
            } else {
                return redirect()->route('admin.login')->with('flash_message_error', 'Invalid Username or Password');
            }
        }
        if (Auth::guard()->check()){
            return redirect(route('admin.dashboard'));
        }else{
            return view ('admin.login');
        }
    }

    public function logout(){
        Session::flush();
        return redirect()->route('admin.login')->with('flash_message_success', 'Logout Successfull');
    }

    public function dashboard()
    {
        $expiredJobPostCount = JobPost::where('job_active', 0)->orWhere('job_status', 0)->count();
        $activeJobPostCount = JobPost::where('job_active', 1)->where('job_status', 1)->count();
        $companyCount = Company::count();
        $jobSeekerCount = JobSeeker::count();

        $RecentJobPosts = JobPost::with('company', 'category')->latest()->take(5)->get();


        $counts = DB::table('job_posts')
            ->select('company_id', DB::raw('count(*) as total'))
            ->groupBy('company_id')
            ->pluck('company_id')->toArray();

        $total = DB::table('job_posts')
            ->select('company_id', DB::raw('count(*) as total'))
            ->groupBy('company_id')
            ->pluck('total')->toArray();

        $companies = Company::whereIn('id',$counts)->pluck('name')->toArray();
        $company = json_encode($companies, JSON_NUMERIC_CHECK);

        $companyPostCount = json_encode($total, JSON_NUMERIC_CHECK);

        $maxData = max($total) + 1;

        //dd($company);
        return view('admin.dashboard')->with(compact('maxData','companyPostCount','company','RecentJobPosts','expiredJobPostCount','activeJobPostCount', 'companyCount', 'jobSeekerCount'));

    }

    public function profile($id){
        try {
            $user = Auth::user()->findOrFail($id);
            return view('admin.admin_profile', compact('user'));
        } catch (\Exception $e) {
            abort(404);
        }
    }

    public function editProfile($id){
        $user = Auth::user()->findOrFail($id);
        return view ('admin.edit_profile', compact('user'));
    }

    public function updateProfile(Request $request,$id){
        $user = Auth::user()->findOrFail($id);
        $data = $request->all();
        $user->name = ucwords(strtolower($data['name']));
        $user->email = strtolower($data['email']);

        if($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = rand(999,9999).'.'.$extension;
            $file->move('public/admin/uploads/profile/', $filename);
            $user->image = 'public/admin/uploads/profile/'.$filename;
        }

        if($request->hasFile('cover_image')){
            $file = $request->file('cover_image');
            $extension = $file->getClientOriginalExtension();
            $filename = rand(888,8888).'.'.$extension;
            $file->move('public/admin/uploads/profile/', $filename);
            $user->cover_image = 'public/admin/uploads/profile/'.$filename;
        }

        $user->save();
        $image_path = $data['current_image'];
        $cover_image = $data['current_coverimage'];

        if ($data['current_image'] != "public/admin/uploads/profile/default_avatar.jpg")
        {
            if(!empty($data['image'])){
                if(File::exists($image_path)){
                    File::delete($image_path);
                }
            }
        }

        if ($data['current_coverimage'] != "public/admin/uploads/profile/default_cover_image.jpg") {
            if (!empty($data['cover_image'])) {
                if (File::exists($cover_image)) {
                    File::delete($cover_image);
                }
            }
        }

        return redirect()->route('admin.profile', Auth::user()->id)->with('success', 'Profile successfully updated');
    }

    public function editPassword(Request $request)
    {
        if ($request->isMethod('post')){
            $this->validate($request,[
                'current_password' => 'required',
                'new_password' => 'required',
                'confirm_password' => 'required'
            ]);

            $data = $request->all();

            $check_password = User::where(['email' => Auth::guard('web')->user()->email])->first();

            $current_password = $data['current_password'];
            if(Hash::check($current_password,$check_password->password))
            {
                $password = bcrypt($data['new_password']);
                User::where('email',Auth::guard('web')->user()->email)->update(['password'=>$password]);
                return redirect()->back()->with('success','Password updated Successfully!');
            }
            else {
                return redirect()->back()->with('error','Incorrect Current Password!');
            }
        }

        return view('admin.edit_password');
    }

    public function chkUserPassword(Request $request){

        $data = $request->all();

        $current_password = $data['current_password'];
        $user_id = Auth::guard('web')->user()->id;
        $check_password = User::where('id', $user_id)->first();
        if (Hash::check($current_password, $check_password->password)){
            echo "true"; die;
        }else{
            echo "false"; die;
        }
    }

    public function report(Request $request)
    {
        if ($request->isMethod('get')){
            $expiredJobPostCount = JobPostActivity::
            count();


            $activeJobPostCount = JobPost::where('job_active', 1)->where('job_status', 1)->count();
            $companyCount = Company::count();
            $jobSeekerCount = JobSeeker::count();

            $RecentJobPosts = JobPost::with('company', 'category')->latest()->take(5)->get();


            $counts = DB::table('job_posts')
                ->select('company_id', DB::raw('count(*) as total'))
                ->groupBy('company_id')
                ->pluck('company_id')->toArray();

            $total = DB::table('job_posts')
                ->select('company_id', DB::raw('count(*) as total'))
                ->groupBy('company_id')
                ->pluck('total')->toArray();

            //dd($total);

            $companies = Company::whereIn('id',$counts)->pluck('name')->toArray();
            $company = json_encode($companies, JSON_NUMERIC_CHECK);

            $companyPostCount = json_encode($total, JSON_NUMERIC_CHECK);

            if (empty($total)){
                $maxData = 1;
            }else{
                $maxData = max($total) + 1;
            }
        }


        if ($request->isMethod('post')){

            $start = strtotime($request->start_date);
            $end = strtotime($request->end_date);

            $startDate = date('Y-m-d',$start);
            $endDate = date('Y-m-d',$end);

           /* $expiredJobPostCount = JobPost::
                where(function ($query)  {
                    $query->where ( 'job_active', 0 )
                        ->orWhere( 'job_status', 0 );
                })
                ->whereBetween('updated_at', [$startDate, $endDate ])
                ->count();*/

            $expiredJobPostCount = JobPostActivity::
                whereBetween('created_at', [$startDate, $endDate ])
                ->count();

            $activeJobPostCount = JobPost::where('job_active', 1)->where('job_status', 1)->whereBetween('updated_at', [$startDate, $endDate ])->count();
            $companyCount = Company::whereBetween('updated_at', [$startDate, $endDate ])->count();
            $jobSeekerCount = JobSeeker::whereBetween('updated_at', [$startDate, $endDate ])->count();

            $RecentJobPosts = JobPost::with('company', 'category')->whereBetween('created_at', [$startDate, $endDate ])->latest()->take(5)->get();

            $dates = $request->all();


            $counts = DB::table('job_posts')
                ->select('company_id', DB::raw('count(*) as total'))
                ->groupBy('company_id')
                ->whereBetween('created_at', [$startDate, $endDate ])
                ->pluck('company_id')->toArray();

            $total = DB::table('job_posts')
                ->select('company_id', DB::raw('count(*) as total'))
                ->groupBy('company_id')
                ->whereBetween('created_at', [$startDate, $endDate ])
                ->pluck('total')->toArray();

            //dd($total);

            $companies = Company::whereIn('id',$counts)->pluck('name')->toArray();
            $company = json_encode($companies, JSON_NUMERIC_CHECK);

            $companyPostCount = json_encode($total, JSON_NUMERIC_CHECK);

            if (empty($total)){
                $maxData = 1;
            }else{
                $maxData = max($total) + 1;
            }


            return view('admin.report')->with(compact('maxData','companyPostCount','company','dates','RecentJobPosts','expiredJobPostCount','activeJobPostCount', 'companyCount', 'jobSeekerCount'));

        }

        return view('admin.report')->with(compact('maxData','companyPostCount','company','RecentJobPosts','expiredJobPostCount','activeJobPostCount', 'companyCount', 'jobSeekerCount'));

    }


}
