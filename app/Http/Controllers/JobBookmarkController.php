<?php

namespace App\Http\Controllers;

use App\JobBookmark;
use App\JobCategory;
use App\JobPost;
use App\JobType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobBookmarkController extends Controller
{
    public function viewJobBookmark(Request $request)
    {
        $user_id = Auth::guard('job_seeker')->user()->id;
        $bookmarks = JobBookmark::with('jobSeeker', 'jobPost')
            ->where('job_seeker_id', $user_id)
            /*->whereHas('jobPost', function ($q){
                $q->where('job_active', 1)
                    ->where('job_status', 1);
            })*/
            ->latest()->paginate(4);
        //dd($bookmarks);
        $categories = JobCategory::where('status',1)->get();
        $jobTypes = JobType::where('status', 1)->get();
        $searchedValues = $request->all();

        return view('frontend.bookmark.view_job_bookmark')->with(compact('searchedValues','bookmarks', 'categories','jobTypes'));
    }

    public function bookmarkJob(Request $request)
    {
        $job_id = $request->job_id;
        $current_user_id = Auth::guard('job_seeker')->user()->id;

        $bookmarkCount = JobBookmark::where('job_seeker_id', $current_user_id)
            ->where('job_post_id', $job_id)
            ->count();
        //if count == 0 add to bookmark else remove from bookmark
        if ($bookmarkCount == 0)
        {
            $jobBookmark = new JobBookmark();
            $jobBookmark->job_seeker_id = $current_user_id;
            $jobBookmark->job_post_id = $job_id;
            $jobBookmark->save();
            return 1;
        }else{
            JobBookmark::where('job_seeker_id', $current_user_id)
                ->where('job_post_id', $job_id)
                ->delete();
            return 0;
        }
    }

    public function filterJob(Request $request)
    {
        $salary = explode(",", $request->salary);
        //dd($salary);
        if (empty($request->sort))
        {
            $request->sort = "latest";
        }

        if (empty($request->job_category))
        {
            //dd($request->job_category);
            $jobCategories = JobCategory::select('id')->get()->toArray();

        }else{
            $jobCategories = $request->job_category;
        }

        if (empty($request->job_type))
        {
            return redirect()->back()->with('error', 'No job posting found');
            $searchKeyword = '';
            $searchedValues = $request->all();
            $categories = JobCategory::where('status', 1)->get();
            $jobTypes = JobType::where('status', 1)->get();
            if (Auth::guard('job_seeker')->check()){
                $jobBookmarks = JobBookmark::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
            }


            return view ( 'frontend.bookmark.bookmark_job_filter' )->with(compact('searchKeyword','searchedValues','jobBookmarks', 'jobs', 'categories', 'jobTypes'));

        }


        if (empty($request->job_location))
        {
            $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                ->whereIn('job_type', $request->job_type)
                ->where('min_job_salary', '>=', $salary[0])
                ->where('max_job_salary', '<=', $salary[1])
                ->whereIn('job_category', $jobCategories)
                ->when($request->sort == "oldest", function ($query){
                    return $query->orderBy('updated_at', 'ASC');
                })
                ->when($request->sort != "oldest", function ($query){
                    return $query->orderBy('updated_at', 'DESC');
                })
                ->whereHas('jobBookmark', function ($q){
                    $q->where('job_seeker_id', Auth::guard('job_seeker')->user()->id);

                })
                ->paginate (4)
                ->setpath('');

            $jobs->appends(array(
                'job_location' => '',
                'job_type' =>  $request->job_type,
                'salary' => $request->salary,
                'job_category' => $jobCategories,
                'sort' => $request->sort,
            ));



        }else{
            $jobs = JobPost::with('company', 'category', 'jobType', 'jobPostActivity')
                ->whereHas('jobBookmark', function ($q){
                    $q->where('job_seeker_id', Auth::guard('job_seeker')->user()->id);

                })
                ->where('job_location', $request->job_location)
                ->whereIn('job_category', $jobCategories)
                ->whereIn('job_type', $request->job_type)
                ->where('min_job_salary', '>=', $salary[0])
                ->where('max_job_salary', '<=', $salary[1])
                ->when($request->sort == "oldest", function ($query){
                    return $query->orderBy('updated_at', 'ASC');
                })
                ->when($request->sort != "oldest", function ($query){
                    return $query->orderBy('updated_at', 'DESC');
                })
                ->paginate (4)
                ->setpath('');

            $jobs->appends(array(
                'job_location' =>  $request->job_location,
                'job_type' =>  $request->job_type,
                'salary' => $request->salary,
                'job_category' => $jobCategories,
                'sort' => $request->sort,
            ));
        }

        $searchKeyword = '';
        $searchedValues = $request->all();
        $categories = JobCategory::where('status', 1)->get();
        $jobTypes = JobType::where('status', 1)->get();
        if (Auth::guard('job_seeker')->check()){
            $jobBookmarks = JobBookmark::where('job_seeker_id', Auth::guard('job_seeker')->user()->id)->get();
        }


        return view ( 'frontend.bookmark.bookmark_job_filter' )->with(compact('searchKeyword','searchedValues','jobBookmarks', 'jobs', 'categories', 'jobTypes'));

    }
}
