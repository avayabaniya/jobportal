<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferenceEmail extends Model
{
    public function timeline()
    {
        return $this->belongsTo('App\Timeline');
    }
}
