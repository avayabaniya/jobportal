<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobType extends Model
{
    protected $table = 'job_types';

    public function jobPosts ()
    {
        return $this->hasMany('App\JobPost', 'job_type');
    }
}
