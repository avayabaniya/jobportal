<?php

namespace App;

use App\Http\Controllers\Auth\JobSeekerResetPasswordController;
use App\Notifications\JobSeekerResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class JobSeeker extends Authenticatable
{
    use Notifiable;

    protected $guard = 'job_seeker';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new JobSeekerResetPasswordNotification($token));
    }

    public function jobPostActivity()
    {
        return $this->hasMany('App\JobPostActivity', 'job_seeker_id');
    }

    public function jobBookmark()
    {
        return$this->hasMany('App\JobBookmark', 'job_seeker_id');
    }

    public function timeline()
    {
        return$this->hasMany('App\Timeline', 'job_seeker_id')->orderBy('start_date', 'desc');;
    }

    public function skill()
    {
        return$this->hasMany('App\Skill', 'job_seeker_id');
    }

    public function achievement()
    {
        return$this->hasMany('App\Achievement', 'job_seeker_id');
    }

}
