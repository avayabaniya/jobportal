<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    public function jobSeeker()
    {
        return $this->belongsTo('App\JobSeeker', 'job_seeker_id');
    }
}
