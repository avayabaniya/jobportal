<?php

namespace App\Providers;

use App\JobPost;
use App\JobSeeker;
use App\Mail\JobRecommendation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $jobs = JobPost::get();

        foreach ($jobs as $job)
        {
            $currentDate = Carbon::now();
            //jobs after publish date
            if ($currentDate >= Carbon::createFromFormat('Y-m-d', $job->job_publish_date) && $currentDate <= Carbon::createFromFormat('Y-m-d', $job->job_expiry_date))
            {
                if ($job->job_active != 1)
                {
                    $job->job_active = 1;
                    $job->save();

                    $jobSeekers = JobSeeker::where('preferred_job_category_id', $job->job_category)->inRandomOrder()->take(2)->get();
                    foreach ($jobSeekers as $jobSeeker)
                    {
                        if ($jobSeeker->preferred_job_category_id == $job->job_category  ) {
                            Mail::to($jobSeeker)->send(new JobRecommendation($job));
                        }
                    }
                }
            }

            if ($currentDate > Carbon::createFromFormat('Y-m-d', $job->job_expiry_date))
            {
                $j = JobPost::find($job->id);
                if ($j->job_active != 0)
                {
                    $j->job_active = 0;
                    $j->save();
                }

            }
        }

    }
}
