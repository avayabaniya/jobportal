<?php

namespace App\Notifications;

use App\JobPost;
use App\JobSeeker;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class JobApplyNotification extends Notification
{
    use Queueable;

    public $jobSeeker;
    public $jobPost;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(JobSeeker $jobSeeker, JobPost $jobPost)
    {
        $this->jobSeeker = $jobSeeker;
        $this->jobPost = $jobPost;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'jobSeeker' => $this->jobSeeker,
            'jobPost' => $this->jobPost
        ];
    }

    public function toBroadcast($notifiable)
    {
        return [
            'jobSeeker' => $this->jobSeeker,
            'jobPost' => $this->jobPost
        ];
    }

    public function broadcastAs() {

        return 'jobPostApplication';

    }


}
