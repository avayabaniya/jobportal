<?php

namespace App\Notifications;

use App\Company;
use App\JobPost;
use App\JobPostActivity;
use App\JobSeeker;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ApplicationStatusChangeNotification extends Notification
{
    use Queueable;

    public $company;
    public $jobPost;
    public $jobPostActivity;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Company $company, JobPost $jobPost, JobPostActivity $jobPostActivity)
    {
        $this->company = $company;
        $this->jobPost = $jobPost;
        $this->jobPostActivity = $jobPostActivity;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'company' => $this->company,
            'jobPost' => $this->jobPost,
            'jobPostActivity' => $this->jobPostActivity
        ];
    }
}
