<?php

namespace App\Mail;

use App\JobPostActivity;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class JobSeekerApplyJob extends Mailable
{
    use Queueable, SerializesModels;

    public $jobPostActivity;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(JobPostActivity $jobPostActivity)
    {
        $this->jobPostActivity = $jobPostActivity;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.jobseeker.jobseeker_apply_job');
    }
}
