<?php

namespace App\Mail;

use App\JobSeeker;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class JobSeekerVerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $jobSeeker;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(JobSeeker $jobSeeker)
    {
        $this->jobSeeker = $jobSeeker;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.verification.jobseeker_verification');
    }
}
