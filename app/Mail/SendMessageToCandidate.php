<?php

namespace App\Mail;

use App\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMessageToCandidate extends Mailable
{
    use Queueable, SerializesModels;

    public $company;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Company $company, Request $message)
    {
        $this->company = $company;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.company.send_message_to_candidate');
    }
}
