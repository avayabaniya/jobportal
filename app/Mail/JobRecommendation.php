<?php

namespace App\Mail;

use App\JobPost;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class JobRecommendation extends Mailable
{
    use Queueable, SerializesModels;

    public $jobPost;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(JobPost $jobPost)
    {
        $this->jobPost = $jobPost;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.jobseeker.job_recommendation');
    }
}
