<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    public function jobSeeker()
    {
        return $this->belongsTo('App\JobSeeker', 'job_seeker_id');
    }

    public function referenceEmail()
    {
        return $this->hasMany('App\ReferenceEmail');
    }
}
