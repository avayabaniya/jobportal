<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPostActivity extends Model
{
    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function jobPost()
    {
        return $this->belongsTo('App\JobPost', 'job_post_id');
    }

    public function jobSeeker()
    {
        return $this->belongsTo('App\JobSeeker', 'job_seeker_id');
    }
}
