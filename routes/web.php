<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//notifications
Route::post('apply-job/notification', 'NotificationController@applyJobNotification');
Route::post('change-application-status/notification', 'NotificationController@applicationChangeNotification');





Route::get('/','FrontEndController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Admin Login
Route::match(['get', 'post'], '/admin/login', 'AdminController@login')->name('admin.login');

Route::group(['middleware' => ['auth']], function() {
    // Admin Routes
    Route::get('/admin/dashboard', 'AdminController@dashboard')->name('admin.dashboard');
    Route::get('/admin/profile/{id}', 'AdminController@profile')->name('admin.profile');
    Route::get('/admin/edit/profile/{id}', 'AdminController@editProfile')->name('edit.profile');
    Route::post('/admin/update/profile/{id}', 'AdminController@updateProfile')->name('update.profile');
    Route::match(['get', 'post'], '/admin/edit/password', 'AdminController@editPassword')->name('edit.password');
    Route::post('/admin/edit/check-password', 'AdminController@chkUserPassword');
    // Logout Route
    Route::get('/logout', 'AdminController@logout')->name('admin.logout');

    //job type routes
    Route::match(['get', 'post'], '/admin/job-type/add', 'JobTypeController@addJobType')->name('add.job.type');
    Route::get('/admin/job-type/view', 'JobTypeController@viewJobType')->name('view.job.type');
    Route::match(['get', 'post'], '/admin/job-type/edit/{id}' , 'JobTypeController@editJobType')->name('edit.job.type');
    Route::match(['get', 'post'], '/admin/delete-job-type/{id}', 'JobTypeController@deleteJobType');

    //job category routes
    Route::match(['get', 'post'], '/admin/job-category/add', 'JobCategoryController@addJobCategory')->name('add.job.category');
    Route::get('/admin/job-category/view', 'JobCategoryController@viewJobCategory')->name('view.job.category');
    Route::match(['get', 'post'], '/admin/job-category/edit/{id}' , 'JobCategoryController@editJobCategory')->name('edit.job.category');
    Route::match(['get', 'post'], '/admin/delete-job-category/{id}', 'JobCategoryController@deleteJobCategory');

    //job post request
    Route::get('/admin/job-post-request', 'JobPostController@jobPostView')->name('view.job.post.request');
    Route::post('/admin/job-post-request/change-status', 'JobPostController@changeJobPostStatus')->name('change.job.post.status');

    //manage Companies
    Route::get('/admin/company/view', 'CompanyController@adminCompanyView')->name('view.company');
    Route::post('/admin/company/change-status', 'CompanyController@adminChangeCompanyStatus')->name('change.company.status');

    Route::get('/admin/report', 'AdminController@report')->name('admin.report');
    Route::post('/admin/report', 'AdminController@report')->name('admin.report');


});
//search
Route::get( '/search', 'SearchController@searchJob')->name('search.job');
//search filter
Route::get( '/filter', 'SearchController@filterJob')->name('filter.job');
Route::get( '/bookmark/filter', 'JobBookmarkController@filterJob')->name('bookmark.filter.job');
Route::get( '/candidate/filter', 'SearchController@filterCandidate')->name('filter.candidate')->middleware('auth:company');

//job bookmark
Route::post( '/job-bookmark', 'JobBookmarkController@bookmarkJob')->name('bookmark.job')->middleware('auth:job_seeker');

Route::get('/job-bookmark/view', 'JobBookmarkController@viewJobBookmark')->name('view.job.bookmark')->middleware('auth:job_seeker');

//category based job listing
Route::get('/job-listing/category/{id}', 'FrontEndController@viewCategoryJobListing')->name('category.job.listing');


//full job listing
Route::get('/job-listing', 'FrontEndController@viewJobListing')->name('job.listing');

//full candidate listing
Route::get('/candidate-listing', 'FrontEndController@viewCandidateListing')->name('candidate.listing')->middleware('auth:company');

//registration
Route::post( '/register', 'CompanyController@register')->name('register');
Route::match(['get'], '/register', 'FrontEndController@register')->name('register');

Route::get('verify/job-seeker/{email}/{verifyToken}', 'JobSeekerController@sendVerificationEmailDone');
Route::get('verify/company/{email}/{verifyToken}', 'CompanyController@sendVerificationEmailDone');

//jobseeker login/logout
Route::get('/job-seeker/logout', 'JobSeekerController@logout')->name('job.seeker.logout')->middleware('auth:job_seeker');
//jobseeker dashboard
//Route::get('/job-seeker/dashboard', 'JobSeekerController@dashboard')->name('job.seeker.dashboard')->middleware('auth:job_seeker');
Route::match(['get', 'post'], '/job-seeker/setting', 'JobSeekerController@setting')->name('job.seeker.setting')->middleware('auth:job_seeker');

Route::post('/job-seeker/check-password', 'JobSeekerController@chkUserPassword');

//jobseeker timeline
Route::match(['get', 'post'], '/job-seeker/add-timeline', 'JobSeekerController@addTimeline')->name('job.seeker.add.timeline')->middleware('auth:job_seeker');
Route::get('/job-seeker/view-career-timeline', 'JobSeekerController@viewCareerTimeline')->name('job.seeker.timeline')->middleware('auth:job_seeker');
Route::get('/job-seeker/view-academic-timeline', 'JobSeekerController@viewAcademicTimeline')->name('job.seeker.academic.timeline')->middleware('auth:job_seeker');
Route::match(['get', 'post'], '/job-seeker/edit-timeline/{id}' , 'JobSeekerController@editTimeline')->name('edit.job.seeker.timeline')->middleware('auth:job_seeker');
Route::get('/company/delete-timeline/{id}', 'JobSeekerController@deleteTimeline')->name('delete.timeline')->middleware('auth:job_seeker');

//jobseeker achievements
Route::match(['get','post'], '/job-seeker/achievements', 'JobSeekerController@achievements')->name('job.seeker.achievements')->middleware('auth:job_seeker');
Route::get('/company/delete-achievement/{id}', 'JobSeekerController@deleteachievement')->name('delete.achievement')->middleware('auth:job_seeker');
Route::match(['get', 'post'], '/job-seeker/edit-achievement/{id}' , 'JobSeekerController@editAchievement')->name('edit.job.seeker.achievement')->middleware('auth:job_seeker');


Route::get('/job-seeker/view-applied-jobs', 'JobSeekerController@viewAppliedJobs')->name('job.seeker.view.applied.jobs')->middleware('auth:job_seeker');

//jobseeker profile
Route::get('/job-seeker-details/{id}', 'FrontEndController@JobSeekerDetails')->name('job.seeker.detail');






//company profile
Route::get('/company-details/{id}', 'FrontEndController@CompanyDetails')->name('company.detail');
//company Login
Route::match(['get', 'post'], '/login', 'CompanyController@login')->name('login');
Route::get('/company/logout', 'CompanyController@logout')->name('company.logout');

//company job route
Route::match(['get', 'post'], '/company/post-job', 'CompanyController@postJob')->name('post.job')->middleware('auth:company');
Route::get('/company/manage-jobs', 'CompanyController@manageJobs')->name('manage.job')->middleware('auth:company');
Route::match(['get', 'post'], '/company/edit/job-post/{id}', 'CompanyController@editJobPost')->name('edit.job.post')->middleware('auth:company');
Route::get('/company/delete-job-post/{id}', 'CompanyController@deleteJobPost')->name('delete.job.post')->middleware('auth:company');
Route::post('/company/send-message/{id}', 'CompanyController@sendMessage')->name('company.send.message')->middleware('auth:company');
//company candidates
Route::get('/company/manage-candidates/{id}', 'CompanyController@manageCandidates')->name('manage.candidates')->middleware('auth:company');
Route::get('/company/manage-all-candidates', 'CompanyController@manageAllCandidates')->name('manage.all.candidates')->middleware('auth:company');
Route::get('/company/delete-candidates/{id}', 'CompanyController@deleteCandidate')->name('delete.candidate')->middleware('auth:company');
Route::get('/company/accept-candidates/{id}', 'CompanyController@acceptCandidate')->name('accept.candidate')->middleware('auth:company');


//company dashboard
Route::get('/company/dashboard', 'CompanyController@dashboard')->name('company.dashboard')->middleware('auth:company');
Route::match(['get', 'post'], '/company/setting', 'CompanyController@setting')->name('company.setting')->middleware('auth:company');

Route::post('/company/check-password', 'CompanyController@chkUserPassword');

Route::match(['get', 'post'],'/company/message-candidate/{id}', 'CompanyController@sendCandidateEmail')->name('candidate.email')->middleware('auth:company');



//job post details
Route::get('/job-detail/{id}', 'FrontEndController@JobPostDetail')->name('job.post.detail');

//apply for job
Route::match(['get', 'post'], '/apply-job/{id}', 'FrontEndController@applyJob')->name('apply.job');

//notification
Route::post('notification/get', 'NotificationController@get');
Route::get('notification/mark-all-as-read', 'NotificationController@markAllAsRead')->name('mark.all.as.read');



//Password reset routes
Route::post('company/password/email', 'Auth\CompanyForgotPasswordController@sendResetLinkEmail')->name('company.password.email');
Route::get('company/password/reset', 'Auth\CompanyForgotPasswordController@showLinkRequestForm')->name('company.password.request');
Route::post('company/password/reset', 'Auth\CompanyResetPasswordController@reset')->name('company.password.update');
Route::get('company/password/reset/{token}','Auth\CompanyResetPasswordController@showResetForm')->name('company.password.reset');


Route::post('job-seeker/password/email', 'Auth\JobSeekerForgotPasswordController@sendResetLinkEmail')->name('jobseeker.password.email');
Route::get('job-seeker/password/reset', 'Auth\JobSeekerForgotPasswordController@showLinkRequestForm')->name('jobseeker.password.request');
Route::post('job-seeker/password/reset', 'Auth\JobSeekerResetPasswordController@reset')->name('jobseeker.password.update');
Route::get('job-seeker/password/reset/{token}','Auth\JobSeekerResetPasswordController@showResetForm')->name('jobseeker.password.reset');