@if ($paginator->hasPages())
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
        <li class="pagination-arrow"><a><i class="icon-material-outline-keyboard-arrow-left"></i></a></li>
    @else
        <li class="pagination-arrow"><a href="{{ $paginator->previousPageUrl() }}"><i class="icon-material-outline-keyboard-arrow-left"></i></a></li>
    @endif

    {{-- Pagination Elements --}}
    @foreach ($elements as $element)

        {{-- "Three Dots" Separator --}}
        {{--@if (is_string($element))
            <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
        @endif--}}

        {{-- Array Of Links --}}
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li><a class="current-page">{{ $page }}</a></li>
                @else
                    <li><a href="{{ $url }}">{{ $page }}</a></li>
                @endif
            @endforeach
        @endif
    @endforeach

    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
        <li class="pagination-arrow"><a href="{{ $paginator->nextPageUrl() }}"><i class="icon-material-outline-keyboard-arrow-right"></i></a></li>
    @else
        <li class="pagination-arrow"><a><i class="icon-material-outline-keyboard-arrow-right"></i></a></li>
    @endif
@endif
