@component('mail::message')
# Job Application for {{ $jobPostActivity->jobPost->job_title }}

Your Job Application for {{ $jobPostActivity->jobPost->job_title }} has been @if($jobPostActivity->application_status == 0) changed to pending @elseif ($jobPostActivity->application_status == 1) accepted @else declined @endif

@component('mail::button', ['url' => 'http://localhost/coursework/company-details/'.$jobPostActivity->company->id])
View Company Profile
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
