@component('mail::message')
# Job Recommendation

A job vacancy for {{ $jobPost->job_title }} is recently posted on our site which may interest you.

@component('mail::button', ['url' => 'http://localhost/coursework/job-detail/'.$jobPost->id])
View Job Detail
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

