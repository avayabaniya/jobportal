@component('mail::message')
# Job Application for {{ $jobPostActivity->jobPost->job_title }}

You have successfully applied to {{ $jobPostActivity->jobPost->job_title }} posted by {{ $jobPostActivity->company->name }}

@component('mail::button', ['url' => 'http://localhost/coursework/company-details/'.$jobPostActivity->company->id])
View Company Profile
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
