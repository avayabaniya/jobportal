@component('mail::message')
# Job Application for {{ $jobPostActivity->jobPost->job_title }}

{{ $jobPostActivity->jobSeeker->name }} has applied for your job post for {{ $jobPostActivity->jobPost->job_title }}

@component('mail::button', ['url' => 'http://localhost/coursework/job-seeker-details/'.$jobPostActivity->jobSeeker->id ])
View Candidate Profile
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
