@component('mail::message')
# {{ $message->subject }}

{!! $message->message !!}

Thanks,<br>
{{$company->name}}
@endcomponent
