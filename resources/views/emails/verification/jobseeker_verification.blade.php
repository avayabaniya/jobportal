@component('mail::message')
#Job Seeker Verification Email

Your account has been registered successfully. Please verify your account.

@component('mail::button', ['url' => 'http://localhost/coursework/verify/job-seeker/'.$jobSeeker->email.'/'.$jobSeeker->verification_token])
Click to verify your email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
