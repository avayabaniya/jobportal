@component('mail::message')
#Employer Verification Email

Your employer account has been registered successfully. Please verify your account.

@component('mail::button', ['url' => 'http://localhost/coursework/verify/company/'.$company->email.'/'.$company->verification_token])
Click to verify your email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
