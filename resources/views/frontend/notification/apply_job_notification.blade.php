<div class="simplebar-track vertical" style="visibility: visible;"><div class="simplebar-scrollbar" style="visibility: visible;"></div></div>
<div class="simplebar-track horizontal" style="visibility: visible;"><div class="simplebar-scrollbar" style="visibility: visible; left: 0px; width: 25px;"></div></div>
<div class="simplebar-scroll-content" style="padding-right: 17px; margin-bottom: -34px;"><div class="simplebar-content" style="padding-bottom: 17px; margin-right: -17px;">
<ul>
                    <!-- Notification -->
                    <span id="updated_count" style="display: none;">{{ Auth::guard('company')->user()->unreadNotifications->count() }}</span>

                    @foreach( Auth::guard('company')->user()->unreadNotifications as $notification)
                        <li class="notifications-not-read">
                            <a href="{{ route('manage.candidates',$notification->data['jobPost']['id'] ) }}">
                                @if(empty($notification->data['jobSeeker']['profile_image']))
                                    <span class="notification-icon"><i class="icon-material-outline-group"></i></span>
                                @else
                                    <span class="notification-icon"><img src="{{ asset('public/job_seeker/images/profile/'.$notification->data['jobSeeker']['profile_image']) }}" alt=""></span>
                                @endif
                                <span class="notification-text">
                                                        <strong>{{ $notification->data['jobSeeker']['name']  }}</strong> applied for a job <span class="color">{{ $notification->data['jobPost']['job_title']  }}</span>
                                                    </span>
                            </a>
                        </li>
                    @endforeach

</ul>
</div>
</div>
