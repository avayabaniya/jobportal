@extends('frontend.layouts.front_design2')
@section('content')

    <!-- Titlebar
================================================== -->
    <div class="single-page-header" data-background-image="images/single-company.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-page-header-inner">
                        <div class="left-side">
                            <div class="header-image"><img src="{{ asset('public/company/images/profile/'.$company->profile_image) }}" alt=""></div>
                            <div class="header-details">
                                <h3>{{ $company->name }} <span></span></h3>
                                <ul>
                                    {{--<li><div class="star-rating" data-rating="4.9"></div></li>--}}
                                    @if(!empty($company->address))
                                        <li><b>Address:</b> {{ $company->address }}</li>
                                    @endif
                                    @if(!empty($company->contact_number))
                                        <li><b>Contact Number:</b> {{ $company->contact_number }}</li>
                                    @endif

                                    @if(!empty($company->established_date))
                                        <li><b>Established Date:</b> {{ $company->established_date }}</li>
                                    @endif

                                    @if($company->verification_status == 1)
                                        <li><div class="verified-badge-with-title">Verified</div></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        {{--<div class="right-side">
                            <!-- Breadcrumbs -->
                            <nav id="breadcrumbs" class="white">
                                <ul>
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Browse Companies</a></li>
                                    <li>Acodia</li>
                                </ul>
                            </nav>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Page Content
    ================================================== -->
    <div class="container">
        <div class="row">

            <!-- Content -->
            <div class="col-xl-8 col-lg-8 content-right-offset">

                <div class="single-page-section">
                    <h3 class="margin-bottom-25">About Company</h3>
                    {!! $company->description !!}
                    @if(empty($company->description))
                        <p>Description not available. </p>
                    @endif
                </div>

                <!-- Boxed List -->
                <div class="boxed-list margin-bottom-60">
                    <div class="boxed-list-headline">
                        <h3><i class="icon-material-outline-business-center"></i> Latest Job Posts</h3>
                    </div>

                    <div class="listings-container compact-list-layout">

                        <!-- Job Listing -->
                        @foreach($jobs as $job)
                            <a href="{{ route('job.post.detail', $job->id) }}" class="job-listing">

                                <!-- Job Listing Details -->
                                <div class="job-listing-details">


                                    <!-- Details -->
                                    <div class="job-listing-description">

                                        <h3 class="job-listing-title">{{ $job->job_title }}@if($job->job_active != 1 || $job->job_status !=1)<span class="unverified-badge" title="Verified Employer" data-tippy-placement="top"></span>@endif</h3>
                                    </div>

                                @if(Auth::guard('job_seeker')->check())
                                    <!-- Bookmark -->
                                        <?php
                                        //check if this product is in wishlist
                                        $count = 0;
                                        foreach ($jobBookmarks as $b){
                                            if ($b->job_post_id == $job->id){
                                                $count++;
                                            }
                                        }
                                        ?>
                                        <span class="bookmark-icon bookmark-job @if($count > 0) bookmarked @endif" rel="{{ $job->id }}" ></span>
                                    @endif
                                </div>

                                <!-- Job Listing Footer -->
                                <div class="job-listing-footer">
                                    <ul>
                                        <li><i class="icon-material-outline-location-on"></i> {{ $job->job_location }}</li>
                                        <li><i class="icon-material-outline-business-center"></i> {{ $job->jobType->name }}</li>
                                        <li><i class="icon-material-outline-access-time"></i> {{ $job->updated_at->diffForHumans() }}</li>
                                        <li><i class="icon-feather-user "></i> {{ $job->job_vacancy_number }} Vacancies</li>
                                    </ul>
                                </div>
                            </a>
                            @if($loop->index == 3) @break @endif
                        @endforeach

                    </div>

                </div>
                <!-- Boxed List / End -->



            </div>


            <!-- Sidebar -->
            <div class="col-xl-4 col-lg-4">
                <div class="sidebar-container">

                    <!-- Location -->
                    <div class="sidebar-widget">
                        <h3>Location</h3>
                        <div id="single-job-map-container">
                                <div id="map-canvas" style="height: 400px; width: 500px;">

                                </div>
                        </div>
                    </div>

                    <!-- Widget -->
                   {{-- <div class="sidebar-widget">
                        <h3>Social Profiles</h3>
                        <div class="freelancer-socials margin-top-25">
                            <ul>
                                <li><a href="#" title="Dribbble" data-tippy-placement="top"><i class="icon-brand-dribbble"></i></a></li>
                                <li><a href="#" title="Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                                <li><a href="#" title="Behance" data-tippy-placement="top"><i class="icon-brand-behance"></i></a></li>
                                <li><a href="#" title="GitHub" data-tippy-placement="top"><i class="icon-brand-github"></i></a></li>

                            </ul>
                        </div>
                    </div>--}}

                    <!-- Sidebar Widget -->
                    <div class="sidebar-widget">
                        <!-- Copy URL -->
                        <div class="copy-url">
                            <input id="copy-url" type="text" value="" class="with-border">
                            <button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
                        </div>

                        <!-- Share Buttons -->
                        <div class="share-buttons margin-top-25">
                            <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                            <div class="share-buttons-content">
                                <span>Interesting? <strong>Share It!</strong></span>
                                <ul class="share-buttons-icons">
                                    <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
                                    <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                                    <li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus" data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
                                    <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')

    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyDXXKZS3Su8ZcIRlLQsBdXBLY4LMt-vmlk&libraries=places">
    </script>

    <script>
        $(document).ready(function () {
            var lat = {{ $company->lat }};
            var lng = {{ $company->lng }};

            if (!lat || !lng)
            {
                lat = 27.72;
                lng = 85.36;
            }

            var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                    lat: lat,
                    lng: lng
                },
                zoom:15
            });

            var marker = new google.maps.Marker({
               position:{
                  lat: lat,
                  lng: lng
               },
               map:map
            });

            //open in google maps
            var gotoMapButton = document.createElement("div");
            gotoMapButton.setAttribute("style", "margin: 5px; border: 1px solid; padding: 1px 12px; font: bold 11px Roboto, Arial, sans-serif; color: #000000; background-color: #FFFFFF; cursor: pointer;");
            gotoMapButton.innerHTML = "Open in Google Maps";
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(gotoMapButton);
            google.maps.event.addDomListener(gotoMapButton, "click", function () {
                var url = 'https://www.google.com/maps?q=' + encodeURIComponent(marker.getPosition().toUrlValue());
                // you can also hard code the URL
                window.open(url);
            });
        });
    </script>
@endsection
