@extends('frontend.layouts.front_design2')
@section('content')
    <!-- Titlebar
================================================== -->
    <div class="single-page-header" data-background-image="images/single-job.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-page-header-inner">
                        <div class="left-side">
                            <div class="header-image"><a href="{{ route('company.detail', $jobDetail->company->id) }}"><img src="{{ asset('public/company/images/profile/'.$jobDetail->company->profile_image ) }}" alt=""></a></div>
                            <div class="header-details">
                                <h3>{{ $jobDetail->job_title }}</h3>
                                <h5>About the Employer</h5>
                                <ul>
                                    <li><a href="{{ route('company.detail', $jobDetail->company->id) }}"><i class="icon-material-outline-business"></i> {{ $jobDetail->company->name }}</a></li>
                                    {{--<li><div class="star-rating" data-rating="4.9"></div></li>--}}
                                    {{--<li><img class="flag" src="images/flags/gb.svg" alt=""> {{ $jobDetail->location }}</li>--}}
                                    <li>
                                        @if($jobDetail->job_status == 1 && $jobDetail->job_active == 1)
                                            <span class="dashboard-status-button green">Job Post Active</span>
                                        @else
                                            <span class="dashboard-status-button red"> Job Post Inactive</span>
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @if($jobDetail->show_salary == 1)
                            <div class="right-side">
                                <div class="salary-box">
                                    <div class="salary-type">Monthly Salary</div>
                                    <div class="salary-amount">@if(empty($jobDetail->min_job_salary) || empty($jobDetail->max_job_salary)) Negotiable @else {{ $jobDetail->min_job_salary }} - {{ $jobDetail->max_job_salary }} NRP @endif</div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Page Content
    ================================================== -->
    <div class="container">
        <div class="row">

            <!-- Content -->
            <div class="col-xl-8 col-lg-8 content-right-offset">

                <div class="single-page-section">
                    <h3 class="margin-bottom-25">Job Description</h3>
                        <p>
                            {!! $jobDetail->job_description !!}
                        </p>
                    @if(empty($jobDetail->job_description))
                        <p>Description not available. </p>
                    @endif
                </div>

                {{--<div class="single-page-section">
                    <h3 class="margin-bottom-30">Location</h3>
                    <div id="single-job-map-container">
                        <div id="singleListingMap" data-latitude="51.507717" data-longitude="-0.131095" data-map-icon="im im-icon-Hamburger"></div>
                        <a href="#" id="streetView">Street View</a>
                    </div>
                </div>--}}

                <div class="single-page-section">
                    <h3 class="margin-bottom-25">Similar Jobs</h3>

                    <!-- Listings Container -->
                    <div class="listings-container grid-layout">

                        <!-- Job Listing -->
                        @if($similarJobs->count() > 0)
                        @foreach($similarJobs as $s)
                            <a href="{{ route('job.post.detail', $s->id) }}" class="job-listing">

                            <!-- Job Listing Details -->
                            <div class="job-listing-details">
                                <!-- Logo -->
                                <div class="job-listing-company-logo">
                                    <img src="{{ asset('public/company/images/profile/'.$s->company->profile_image ) }}" alt="">
                                </div>

                                <!-- Details -->
                                <div class="job-listing-description">
                                    <h4 class="job-listing-company">{{ $s->company->name }}</h4>
                                    <h3 class="job-listing-title">{{ $s->job_title }}</h3>
                                </div>
                            </div>

                            <!-- Job Listing Footer -->
                            <div class="job-listing-footer">
                                <ul>
                                    <li><i class="icon-material-outline-location-on"></i> {{ $s->job_location }}</li>
                                    <li><i class="icon-material-outline-business-center"></i>{{ $s->jobType->name }}</li>
                                    {{--<li><i class="icon-material-outline-account-balance-wallet"></i> $35000-$38000</li>--}}
                                    <li><i class="icon-feather-user "></i> {{ $s->job_vacancy_number }} Vacancies</li>
                                    <li><i class="icon-material-outline-access-time"></i> {{ $s->updated_at->diffForHumans() }}</li>
                                </ul>
                            </div>
                        </a>
                        @endforeach
                          @else
                            <p>No similar Jobs</p>
                        @endif

                    </div>
                    <!-- Listings Container / End -->

                </div>
            </div>


            <!-- Sidebar -->
            <div class="col-xl-4 col-lg-4">
                <div class="sidebar-container">
                    @if($jobDetail->job_status == 1 && $jobDetail->job_active == 1 && Auth::guard('job_seeker')->check())
                        @if(Auth::guard('job_seeker')->check())
                            @if($postJobActivity > 0)
                                <a href="" rel="0" class="apply-now-button">Already Applied<i class="icon-material-outline-arrow-right-alt"></i></a>
                            @elseif(Auth::guard('job_seeker')->user()->verification_status == 1)
                                <a id="apply-now" rel="{{ $jobDetail->id }}" rel1="{{ $jobDetail->company_id }}" href="{{ route('apply.job', $jobDetail->id) }}" class="apply-now-button">Apply Now <i class="icon-material-outline-arrow-right-alt"></i></a>
                            @else
                                <a href="" rel="0" class="apply-now-button">Verify Email to Apply<i class="icon-material-outline-arrow-right-alt"></i></a>
                            @endif
                        @elseif(!Auth::guard('company')->check())
                            <a id="apply-now" rel="{{ $jobDetail->id }}" href="{{ route('apply.job', $jobDetail->id) }}" class="apply-now-button">Apply Now <i class="icon-material-outline-arrow-right-alt"></i></a>
                        @endif
                    @else
                        <a id="apply-now" rel="{{ $jobDetail->id }}" href="#sign-in-dialog" class="popup-with-zoom-anim apply-now-button">Login to Apply <i class="icon-material-outline-arrow-right-alt"></i></a>
                @endif

                    <!-- Sidebar Widget -->
                    <div class="sidebar-widget">
                        <div class="job-overview">
                            <div class="job-overview-headline">Job Summary</div>
                            <div class="job-overview-inner">
                                <ul>
                                    <li>
                                        <i class="icon-material-outline-location-on"></i>
                                        <span>Location</span>
                                        <h5>{{ $jobDetail->job_location }}</h5>
                                    </li>
                                    <li>
                                        <i class="icon-material-outline-business-center"></i>
                                        <span>Job Type</span>
                                        <h5>{{ $jobDetail->jobType->name }}</h5>
                                    </li>
                                    {{--<li>
                                        <i class="icon-material-outline-local-atm"></i>
                                        <span>Salary</span>
                                        <h5>$35k - $38k</h5>
                                    </li>--}}
                                    <li>
                                        <i class="icon-material-outline-access-time"></i>
                                        <span>Posted Date</span>
                                        <h5> {{ date('d M, Y', strtotime($jobDetail->job_publish_date)) }}</h5>
                                    </li>

                                    <li>
                                        <i style="color: red;" class="icon-material-outline-access-time"></i>
                                        <span>Expiry Date</span>
                                        <h5>{{ date('d M, Y', strtotime($jobDetail->job_expiry_date)) }}</h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- Sidebar Widget -->
                    <div class="sidebar-widget">
                        <h3>Bookmark or Share</h3>

                    @if(Auth::guard('job_seeker')->check())
                        <!-- Bookmark Button -->
                        <?php
                        //check if this product is in wishlist
                        $count = 0;
                        foreach ($jobBookmarks as $b){
                            if ($b->job_post_id == $jobDetail->id){
                                $count++;
                            }
                        }
                        ?>


                        <button class="bookmark-button margin-bottom-25 bookmark-job @if($count > 0) bookmarked @endif" rel="{{ $jobDetail->id }}" >
                            <span class="bookmark-icon"></span>
                            <span class="bookmark-text">Bookmark</span>
                            <span class="bookmarked-text">Bookmarked</span>
                        </button>
                    @endif

                        <!-- Copy URL -->
                        <div class="copy-url">
                            <input id="copy-url" type="text" value="" class="with-border">
                            <button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
                        </div>

                        <!-- Share Buttons -->
                        <div class="share-buttons margin-top-25">
                            <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                            <div class="share-buttons-content">
                                <span>Interesting? <strong>Share It!</strong></span>
                                <ul class="share-buttons-icons">
                                    <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
                                    <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                                    <li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus" data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
                                    <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
@endsection
