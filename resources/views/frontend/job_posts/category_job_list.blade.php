<?php
use App\Http\Controllers\Controller;
$jobBookmarkCount = Controller::jobBookmarkCount();
?>
@extends('frontend.layouts.front_design2')
@section('content')

    <!-- Spacer -->
    <div class="margin-top-90"></div>
    <!-- Spacer / End-->

    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <form action="{{ route('filter.job') }}" method="get">

                    <div class="sidebar-container">

                        <!-- Location -->
                        <div class="sidebar-widget">
                            <h3>Location</h3>
                            <div class="input-with-icon">
                                <div id="autocomplete-container">
                                    <input name="job_location" id="autocomplete-input" type="text" placeholder="Location">
                                </div>
                                <i class="icon-material-outline-location-on"></i>
                            </div>
                        </div>


                        <!-- Keywords -->
                    {{--<div class="sidebar-widget">
                        <h3>Keywords</h3>
                        <div class="keywords-container">
                            <div class="keyword-input-container">
                                <input type="text" class="keyword-input" placeholder="e.g. job title"/>
                                <button class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
                            </div>
                            <div class="keywords-list"><!-- keywords go here --></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>--}}

                    <!-- Category -->
                        <div class="sidebar-widget">
                            <h3>Category</h3>
                            <select name="job_category[]" class="selectpicker default" multiple data-selected-text-format="count" data-size="7" title="All Categories" >
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" @if($category->id == $selectedCategory->id) selected @endif>{{ $category->category_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <!-- Job Types -->
                        <div class="sidebar-widget">
                            <h3>Job Type</h3>

                            <div class="switches-list">
                                @foreach($jobTypes as $type)
                                    <div class="switch-container">
                                        <label class="switch"><input name="job_type[]" checked value="{{ $type->id }}" type="checkbox"><span class="switch-button"></span> {{ $type->name }}</label>
                                    </div>
                                @endforeach
                            </div>

                        </div>

                        <!-- Salary -->
                        <div class="sidebar-widget">
                            <h3>Salary</h3>
                            <div class="margin-top-55"></div>

                            <!-- Range Slider -->
                            <input name="salary" class="range-slider" type="text" value="" data-slider-currency="NRS" data-slider-min="0" data-slider-max="100000" data-slider-step="5000" data-slider-value="[0,100000]"/>
                        </div>

                        <button class="button ripple-effect" type="submit">Filter <i class="icon-material-outline-search"></i></button>

                        <!-- Tags -->
                        {{--<div class="sidebar-widget">
                            <h3>Tags</h3>

                            <div class="tags-container">
                                <div class="tag">
                                    <input type="checkbox" id="tag1"/>
                                    <label for="tag1">front-end dev</label>
                                </div>
                                <div class="tag">
                                    <input type="checkbox" id="tag2"/>
                                    <label for="tag2">angular</label>
                                </div>
                                <div class="tag">
                                    <input type="checkbox" id="tag3"/>
                                    <label for="tag3">react</label>
                                </div>
                                <div class="tag">
                                    <input type="checkbox" id="tag4"/>
                                    <label for="tag4">vue js</label>
                                </div>
                                <div class="tag">
                                    <input type="checkbox" id="tag5"/>
                                    <label for="tag5">web apps</label>
                                </div>
                                <div class="tag">
                                    <input type="checkbox" id="tag6"/>
                                    <label for="tag6">design</label>
                                </div>
                                <div class="tag">
                                    <input type="checkbox" id="tag7"/>
                                    <label for="tag7">wordpress</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>--}}

                    </div>
                </form>

            </div>
            <div class="col-xl-9 col-lg-8 content-left-offset">

                <h3 class="page-title">Job Post Listing Under "<b>{{ $selectedCategory->category_name }}</b>" Category</h3>

                <div class="notify-box margin-top-15">
                    {{--<div class="switch-container">
                        <label class="switch"><input type="checkbox"><span class="switch-button"></span><span class="switch-text">Turn on email alerts for this search</span></label>
                    </div>--}}

                    <div class="switch-container">
                        <h4 class="job-listing-company"><span class="switch-text">Browse Job Vacancy</span></h4>
                    </div>

                    <div class="sort-by">
                        <span>Sort by:</span>
                        <form action="{{ route('filter.job') }}" method="get" id="filterJob3">
                            <input type="hidden" name="job_location" value="@if(!empty($searchedValues['job_location'])){{ $searchedValues['job_location'] }} @endif">
                            <input name="salary" type="hidden" value="@if(!empty($searchedValues['salary'])){{ $searchedValues['salary'] }}@else 0,100000 @endif" >

                            <div style="display: none">
                                <select name="job_category[]" class="selectpicker default" multiple data-selected-text-format="count" data-size="7" title="All Categories" >
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}"
                                                @if(!empty($searchedValues['job_category']))
                                                @foreach($searchedValues['job_category'] as $c)
                                                @if($c == $category->id || $c == $selectedCategory->id)
                                                selected
                                                @endif
                                                @endforeach
                                                @endif

                                        >{{ $category->category_name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div style="display: none;">
                                @foreach($jobTypes as $type)
                                    <div class="switch-container">
                                        <label class="switch"><input name="job_type[]" checked value="{{ $type->id }}" type="checkbox"><span class="switch-button"></span> {{ $type->name }}</label>
                                    </div>
                                @endforeach
                            </div>



                            <select name="sort" class="selectpicker hide-tick" onchange="this.form.submit()">
                                <option value="latest">Latest</option>
                                <option value="oldest" @if(!empty($searchedValues['sort'])) @if($searchedValues['sort'] == "oldest") selected @endif  @endif>Oldest</option>
                            </select>
                        </form>
                    </div>
                </div>

                <div class="listings-container margin-top-35">

                    <!-- Job Listing -->
                    @foreach($jobs as $job)
                    <a href="{{ route('job.post.detail', $job->id) }}" class="job-listing">

                        <!-- Job Listing Details -->
                        <div class="job-listing-details">
                            <!-- Logo -->
                            <div class="job-listing-company-logo">
                                <img src="{{ asset('public/company/images/profile/'.$job->company->profile_image ) }}" alt="">
                            </div>

                            <!-- Details -->
                            <div class="job-listing-description">
                                <h4 class="job-listing-company">{{ $job->company->name }} <span class="verified-badge" title="Verified Employer" data-tippy-placement="top"></span></h4>
                                <h3 class="job-listing-title">{{ $job->job_title }}</h3>
                                <p class="job-listing-text">{!! substr($job->job_description, 0, 500) !!}{{ strlen($job->job_description) > 500 ? "..." : "" }}</p>
                            </div>

                            @if(Auth::guard('job_seeker')->check())
                            <!-- Bookmark -->
                            <?php
                            //check if this product is in wishlist
                            $count = 0;
                            foreach ($jobBookmarks as $b){
                                if ($b->job_post_id == $job->id){
                                    $count++;
                                }
                            }
                            ?>
                            <span class="bookmark-icon bookmark-job @if($count > 0) bookmarked @endif" rel="{{ $job->id }}" ></span>
                            @endif
                        </div>

                        <!-- Job Listing Footer -->
                        <div class="job-listing-footer">
                            <ul>
                                <li><i class="icon-material-outline-location-on"></i> {{ $job->job_location }}</li>
                                <li><i class="icon-material-outline-business-center"></i> {{ $job->jobType->name }}</li>
                                <li><i class="icon-material-outline-account-balance-wallet"></i>@if(empty($job->min_job_salary) || empty($job->max_job_salary)) Negotiable @else {{ $job->min_job_salary }} NRP - {{ $job->max_job_salary }} NRP @endif</li>
                                <li><i class="icon-material-outline-access-time"></i> {{ $job->updated_at->diffForHumans() }}</li>
                                <li><i class="icon-feather-user "></i> {{ $job->job_vacancy_number }} Vacancies</li>
                            </ul>
                        </div>
                    </a>
                    @endforeach

                    <!-- Pagination -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Pagination -->
                            <div class="pagination-container margin-top-30 margin-bottom-60">
                                <nav class="pagination">
                                    <ul>
                                        {{ $jobs->links() }}
                                        {{--<li class="pagination-arrow"><a href="#"><i class="icon-material-outline-keyboard-arrow-left"></i></a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#" class="current-page">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li class="pagination-arrow"><a href="#"><i class="icon-material-outline-keyboard-arrow-right"></i></a></li>--}}
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- Pagination / End -->

                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection