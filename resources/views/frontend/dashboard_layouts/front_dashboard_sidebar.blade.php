<?php
use App\Http\Controllers\Controller;

$jobCount = Controller::companyJobCount();
?>

<!-- Dashboard Sidebar
        ================================================== -->
<div class="dashboard-sidebar">
    <div class="dashboard-sidebar-inner" data-simplebar>
        <div class="dashboard-nav-container">

            <!-- Responsive Navigation Trigger -->
            <a href="#" class="dashboard-responsive-nav-trigger">
					<span class="hamburger hamburger--collapse" >
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</span>
                <span class="trigger-title">Dashboard Navigation</span>
            </a>

            <!-- Navigation -->
            <div class="dashboard-nav">
                <div class="dashboard-nav-inner">

                    @if(Auth::guard('company')->check())
                    <ul data-submenu-title="Start">
                        <li class="active"><a href="{{ route('company.dashboard') }}"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
                        {{--<li><a href="dashboard-messages.html"><i class="icon-material-outline-question-answer"></i> Messages <span class="nav-tag">2</span></a></li>
                        <li><a href="dashboard-bookmarks.html"><i class="icon-material-outline-star-border"></i> Bookmarks</a></li>
                        <li><a href="dashboard-reviews.html"><i class="icon-material-outline-rate-review"></i> Reviews</a></li>--}}
                    </ul>
                    @endif

                    @if(Auth::guard('company')->check())
                        @if(Auth::guard('company')->user()->verification_status == 1 && Auth::guard('company')->user()->status == 1)
                            <ul data-submenu-title="Organize and Manage">
                                <li><a href="#"><i class="icon-material-outline-business-center"></i> Jobs</a>
                                    <ul>
                                        <li><a href="{{ route('manage.job') }}">Manage Jobs <span class="nav-tag">{{ $jobCount }}</span></a></li>
                                        <li><a href="{{ route('manage.all.candidates') }}">Manage Candidates</a></li>
                                        <li><a href="{{ route('post.job') }}">Post a Job</a></li>
                                    </ul>
                                </li>
                            </ul>
                        @endif
                    @endif

                    @if(Auth::guard('job_seeker')->check())
                            <ul data-submenu-title="Organize and Manage">
                                <li><a href="#"><i class="icon-material-outline-assignment"></i> Timeline</a>
                                    <ul>
                                        <li><a href="{{ route('job.seeker.timeline') }}">View Career Timeline</a></li>
                                        <li><a href="{{ route('job.seeker.academic.timeline') }}">View Academic Timeline</a></li>
                                        <li><a href="{{ route('job.seeker.add.timeline') }}">Add Timeline</a></li>
                                    </ul>
                                </li>

                                <li><a href="{{ route('job.seeker.achievements') }}"><i class="icon-line-awesome-trophy"></i> Achievements</a>
                                <li><a href="{{ route('job.seeker.view.applied.jobs') }}"><i class="icon-material-outline-business-center"></i> View Applied Jobs</a>

                            </ul>


                    @endif


                    <ul data-submenu-title="Account">
                        @if(Auth::guard('company')->check())
                            <li><a href="{{ route('company.setting') }}"><i class="icon-material-outline-settings"></i> Settings</a></li>
                            <li><a href="{{ route('company.logout') }}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
                        @endif

                        @if(Auth::guard('job_seeker')->check())
                            <li><a href="{{ route('job.seeker.setting') }}"><i class="icon-material-outline-settings"></i> Settings</a></li>
                            <li><a href="{{ route('job.seeker.logout') }}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
                        @endif
                    </ul>

                </div>
            </div>
            <!-- Navigation / End -->

        </div>
    </div>
</div>
<!-- Dashboard Sidebar / End -->