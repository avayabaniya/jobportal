<!-- Header Container
    ================================================== -->
<header id="header-container" class="fullwidth dashboard-header not-sticky">

    <!-- Header -->
    <div id="header">
        <div class="container">

            <!-- Left Side Content -->
            <div class="left-side">

                <!-- Logo -->
                <div id="logo">
                    <a href="index.html"><img src="images/logo.png" alt=""></a>
                </div>

                <!-- Main Navigation -->
                <nav id="navigation">
                    <ul id="responsive">

                        <li><a href="{{ route('index') }}" class="current">Home</a>
                        </li>

                        <li><a href="{{ route('job.listing') }}" class="current">Jobs</a>
                        </li>

                        @if(Auth::guard('job_seeker')->check())
                            <li><a href="{{ route('view.job.bookmark') }}" class="current">Job Bookmarks</a>
                            </li>

                            <li><a href="#" style="color: #28b661">Timeline</a>
                                <ul class="dropdown-nav">

                                    <li><a href="#">Career Timeline</a>
                                        <ul class="dropdown-nav">
                                            <li><a href="{{ route('job.seeker.add.timeline') }}">Add Timeline</a></li>
                                            <li><a href="{{ route('job.seeker.timeline') }}">View Timeline</a></li>
                                        </ul>
                                    </li>

                                    <li><a href="#">Academic Timeline</a>
                                        <ul class="dropdown-nav">
                                            <li><a href="{{ route('job.seeker.add.timeline') }}">Add Timeline</a></li>
                                            <li><a href="{{ route('job.seeker.academic.timeline') }}">View Timeline</a></li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>

                            <li><a href="{{ route('job.seeker.achievements') }}" class="current">Achievements</a>
                            </li>

                            <li><a href="{{ route('job.seeker.view.applied.jobs') }}" class="current">Applied Jobs</a>
                            </li>

                        @elseif(Auth::guard('company')->check())
                            <li><a href="{{ route('post.job') }}" class="current">Post Job</a>
                            </li>

                            <li><a href="{{ route('candidate.listing') }}" class="current">Candidates</a>
                            </li>
                        @endif

                        @if(Auth::guard('job_seeker')->check())
                            <li><a href="{{ route('job.seeker.detail', Auth::guard('job_seeker')->user()->id) }}" class="current">Profile</a>
                            </li>
                        @elseif(Auth::guard('company')->check())
                            <li><a href="{{ route('company.detail', Auth::guard('company')->user()->id) }}" class="current">Profile</a>
                            </li>
                        @endif



                        <li><a href="#" class="current">Contact us</a>
                        </li>

                    </ul>
                </nav>
                <div class="clearfix"></div>
                <!-- Main Navigation / End -->

            </div>
            <!-- Left Side Content / End -->


            {{--Right side login/register button if logged out profile image if logged in--}}
            @if(Auth::guard('company')->check())
                <div class="right-side">
                    <!--  User Notifications -->
                    <div class="header-widget hide-on-mobile">
                        <!-- Notifications -->
                        <div class="header-notifications">
                            <!-- Trigger -->
                            <div class="header-notifications-trigger">
                                <a href="#"><i class="icon-feather-bell"></i><span id="company_notification_count">{{ Auth::guard('company')->user()->unreadNotifications->count() }}</span></a>
                            </div>
                        {{--@if( Auth::guard('company')->user()->unreadNotifications->count())--}}
                            <!-- Dropdown -->
                                <div class="header-notifications-dropdown">
                                    <div class="header-notifications-headline">
                                        <h4>Notifications</h4>
                                        <a href="{{ route('mark.all.as.read') }}"><button class="mark-as-read ripple-effect-dark" title="Mark all as read" data-tippy-placement="left">
                                                <i class="icon-feather-check-square"></i>
                                            </button></a>
                                    </div>

                                    <div class="header-notifications-content">
                                        <div class="header-notifications-scroll fade-in-notification" data-simplebar>
                                            <ul class="fade-out-notification">
                                                <!-- Notification -->
                                                @foreach( Auth::guard('company')->user()->unreadNotifications as $notification)
                                                    <li class="notifications-not-read">
                                                        <a href="  @if($notification->type == "App\Notifications\JobApplyNotification") {{ route('manage.candidates',$notification->data['jobPost']['id'] ) }} @else {{ route('edit.job.post',$notification->data['jobPost']['id'] ) }} @endif">
                                                            @if(empty($notification->data['jobSeeker']['profile_image']))
                                                                <span class="notification-icon"><i class="icon-material-outline-group"></i></span>
                                                            @else
                                                                <span class="notification-icon"><img src="{{ asset('public/job_seeker/images/profile/'.$notification->data['jobSeeker']['profile_image']) }}" alt=""></span>
                                                            @endif
                                                            <span class="notification-text">
                                                            @if($notification->type == "App\Notifications\JobApplyNotification")
                                                                    <strong>{{ $notification->data['jobSeeker']['name']  }}</strong> applied for a job <span class="color">{{ $notification->data['jobPost']['job_title']  }}</span>
                                                                @else
                                                                    <?php
                                                                    if ($notification->data['jobPost']['job_status'] == 0){
                                                                        $jobStatus = "Pending";
                                                                    }elseif($notification->data['jobPost']['job_status'] == 1){
                                                                        $jobStatus = "Accepted";
                                                                    }else{
                                                                        $jobStatus = "Declined";
                                                                    }
                                                                    ?>

                                                                    Your job post status for <strong> {{ $notification->data['jobPost']['job_title'] }} </strong> has been changed to <span class="color"> {{ $jobStatus }} </span>

                                                                @endif
                                                        </span>
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            {{--@endif--}}


                        </div>

                    </div>
                    <!--  User Notifications / End -->

                    <!-- User Menu -->
                    <div class="header-widget">
                        <!-- Messages -->
                        <div class="header-notifications user-menu">
                            <div class="header-notifications-trigger">
                                <a href="#"><div class="user-avatar status-online"><img src="{{asset('public/company/images/profile/'. Auth::guard('company')->user()->profile_image)}}" width="42px" height="42px" alt=""></div></a>
                            </div>

                            <!-- Dropdown -->
                            <div class="header-notifications-dropdown">

                                <!-- User Status -->
                                <div class="user-status">

                                    <!-- User Name / Avatar -->
                                    <div class="user-details">
                                        <div class="user-avatar status-online"><img src="{{asset('public/company/images/profile/'. Auth::guard('company')->user()->profile_image)}}" width="42px" height="42px" alt=""></div>
                                        <div class="user-name">
                                            {{Auth::guard('company')->user()->name}} <span>Company</span>
                                        </div>
                                    </div>
                                </div>

                                <ul class="user-menu-small-nav">
                                    <li><a href="{{ route('company.dashboard') }}"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>
                                    <li><a href="{{ route('company.setting') }}"><i class="icon-material-outline-settings"></i> Settings</a></li>
                                    <li><a href="{{ route('company.logout') }}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
                                </ul>

                            </div>
                        </div>

                    </div>
                    <!-- User Menu / End -->

                    <!-- Mobile Navigation Button -->
                    <span class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</span>

                </div>
            @elseif(Auth::guard('job_seeker')->check())
                <div class="right-side">
                    <!--  User Notifications -->
                    <div class="header-widget hide-on-mobile">
                        <!-- Notifications -->
                        <div class="header-notifications">
                            <!-- Trigger -->
                            <div class="header-notifications-trigger">
                                <a href="#"><i class="icon-feather-bell"></i><span id="jobseeker_notification_count">{{ Auth::guard('job_seeker')->user()->unreadNotifications->count() }}</span></a>
                            </div>
                            <!-- Dropdown -->
                            <div class="header-notifications-dropdown">
                                <div class="header-notifications-headline">
                                    <h4>Notifications</h4>
                                    <a href="{{ route('mark.all.as.read') }}"><button class="mark-as-read ripple-effect-dark" title="Mark all as read" data-tippy-placement="left">
                                            <i class="icon-feather-check-square"></i>
                                        </button></a>
                                </div>

                                <div class="header-notifications-content">
                                    <div class="header-notifications-scroll fade-in-notification" data-simplebar>
                                        <ul class="fade-out-notification">
                                            <!-- Notification -->
                                            @foreach( Auth::guard('job_seeker')->user()->unreadNotifications as $notification)
                                                <li class="notifications-not-read">
                                                    <a href="  @if($notification->type == "App\Notifications\ApplicationStatusChangeNotification") {{ route('job.seeker.view.applied.jobs') }}  @endif">
                                                        @if(empty($notification->data['company']['profile_image']))
                                                            <span class="notification-icon"><i class="icon-material-outline-group"></i></span>
                                                        @else
                                                            <span class="notification-icon"><img src="{{ asset('public/company/images/profile/'.$notification->data['company']['profile_image']) }}" alt=""></span>
                                                        @endif
                                                        <span class="notification-text">
                                                            @if($notification->type == "App\Notifications\ApplicationStatusChangeNotification")

                                                                <?php
                                                                if ($notification->data['jobPostActivity']['application_status'] == 0){
                                                                    $jobStatus = "Pending";
                                                                }elseif($notification->data['jobPostActivity']['application_status'] == 1){
                                                                    $jobStatus = "Accepted";
                                                                }else{
                                                                    $jobStatus = "Declined";
                                                                }
                                                                ?>

                                                                Your application status for <strong> {{ $notification->data['jobPost']['job_title'] }} </strong> has been changed to <span class="color"> {{ $jobStatus }} </span>

                                                            @endif
                                                        </span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <!--  User Notifications / End -->
                    <!-- User Menu -->
                    <div class="header-widget">
                        <!-- Messages -->
                        <div class="header-notifications user-menu">
                            <div class="header-notifications-trigger">
                                <a href="#"><div class="user-avatar status-online"><img src="{{asset('public/job_seeker/images/profile/'. Auth::guard('job_seeker')->user()->profile_image)}}" alt="" width="42px" height="42px"></div></a>
                            </div>

                            <!-- Dropdown -->
                            <div class="header-notifications-dropdown">

                                <!-- User Status -->
                                <div class="user-status">

                                    <!-- User Name / Avatar -->
                                    <div class="user-details">
                                        <div class="user-avatar status-online"><img src="{{asset('public/job_seeker/images/profile/'. Auth::guard('job_seeker')->user()->profile_image)}}" alt="" width="42px" height="42px"></div>
                                        <div class="user-name">
                                            {{Auth::guard('job_seeker')->user()->name}} <span>Job Seeker</span>
                                        </div>
                                    </div>
                                </div>

                                <ul class="user-menu-small-nav">
                                    {{--<li><a href="{{ route('job.seeker.dashboard') }}"><i class="icon-material-outline-dashboard"></i> Dashboard</a></li>--}}
                                    <li><a href="{{ route('job.seeker.setting') }}"><i class="icon-material-outline-settings"></i> Settings</a></li>
                                    <li><a href="{{ route('job.seeker.logout') }}"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
                                </ul>

                            </div>
                        </div>

                    </div>
                    <!-- User Menu / End -->

                    <!-- Mobile Navigation Button -->
                    <span class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</span>

                </div>
            @else
                <div class="right-side">

                    <div class="header-widget">
                        <a href="#sign-in-dialog" class="popup-with-zoom-anim log-in-button"><i class="icon-feather-log-in"></i> <span>Log In / Register</span></a>
                    </div>

                    <!-- Mobile Navigation Button -->
                    <span class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</span>

                </div>
            @endif

        </div>
    </div>
    <!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->