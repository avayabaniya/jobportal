<!doctype html>
<html lang="en">
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>Hireo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/colors/green.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/timeline.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">


    <style>
        .error{
            color: red;
        }
    </style>
    @yield('styles')

</head>
<body class="gray">

<!-- Wrapper -->
<div id="wrapper">

    @include('frontend.dashboard_layouts.front_dashboard_header')


    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('frontend.dashboard_layouts.front_dashboard_sidebar')


        <!-- Dashboard Content
        ================================================== -->
        @yield('content')
        <!-- Dashboard Content / End -->

    </div>
    <!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->




{{--<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">Add Note</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Do Not Forget 😎</h3>
                </div>

                <!-- Form -->
                <form method="post" id="add-note">

                    <select class="selectpicker with-border default margin-bottom-20" data-size="7" title="Priority">
                        <option>Low Priority</option>
                        <option>Medium Priority</option>
                        <option>High Priority</option>
                    </select>

                    <textarea name="textarea" cols="10" placeholder="Note" class="with-border"></textarea>

                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="add-note">Add Note <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>--}}



<!-- Scripts
================================================== -->
<script src="{{ asset('public/frontend/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/jquery-migrate-3.0.0.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/mmenu.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/tippy.all.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/simplebar.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/bootstrap-slider.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/snackbar.js') }}"></script>
<script src="{{ asset('public/frontend/js/clipboard.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/counterup.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/magnific-popup.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/slick.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/custom.js') }}"></script>

<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=05fgyydvhrzu7w93s0m0hfu5plg2dthhn2v9tjqkyeb8fpkb"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>

<script>
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate:new Date(),
        format: 'dd M, yyyy'
    });
</script>

<script>
    $('.datepicker-dob').datepicker({
        autoclose: true,
        todayHighlight: true,
        endDate:new Date(),
        format: 'dd M, yyyy'
    });
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
    $(document).ready(function () {
        $(".deleteRecord").click(function(){
            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are You Sure",
                    text: "You will not be able to recover this record again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, Delete it!",
                    confirmButtonColor: "red"
                },
                function(){
                    window.location.href="/coursework/company/"+deleteFunction+"/"+id;
                }
            );
        });
    });
</script>



<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function() {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{ asset('public/frontend/js/chart.min.js') }}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196,132,215,362,210,252],
                pointRadius: 5,
                pointHoverRadius:5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: { display: false },
            title:  { display: false },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: { display: false },
                    gridLines:  { display: false },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"> </script>
<script >
    @if(Session::has('success'))

        toastr.options.positionClass = 'toast-top-right';
    toastr.success('{{ Session::get('success') }}');

    @endif
            @if(Session::has('error'))

        toastr.options.positionClass = 'toast-top-right';
    toastr.error('{{ Session::get('error') }}');

    @endif
</script>

<script>
    $(document).ready(function () {
        let company_id = {{ empty(Auth::guard('company')->user()->id) ? 0 : Auth::guard('company')->user()->id }};

        let APP_URL = {!! json_encode(url('/')) !!};
        console.log(APP_URL);
        if (company_id !== 0)
        {
            setInterval(function () {

                let notification_count = $("#company_notification_count").text();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'post',
                    url: APP_URL + '/apply-job/notification',
                    data:{
                        company_id: company_id,
                        notification_count: notification_count
                    },
                    success:function (resp) {

                        if (resp != -1)
                        {
                            console.log(resp);
                            //let a = $('#updated_count').text();
                            let a = resp[0];

                            $("#company_notification_count").text(a);

                            $('.fade-out-notification').fadeOut(100, function(){

                                $('.fade-in-notification').html(resp[1]).fadeIn();
                                //return;
                            });

                        }

                    }, error:function (resp) {
                        //alert('error')

                    }
                });

            }, 10000);
        }

    });
</script>

@yield('scripts')

</body>
</html>