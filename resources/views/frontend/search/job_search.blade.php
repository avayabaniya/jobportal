@extends('frontend.layouts.front_design2')
@section('content')

    <!-- Spacer -->
    <div class="margin-top-90"></div>
    <!-- Spacer / End-->

    <div class="container">
        <div class="row">

            <div class="col-xl-3 col-lg-4">
                <div class="sidebar-container">
                    <form action="{{ route('search.job') }}" method="get" id="searchJob2">
                    <!-- Location -->
                    <div class="sidebar-widget">
                        <h3>Keyword</h3>
                        <div class="input-with-icon">
                            <div id="autocomplete-container">
                                <input name="q" type="text" placeholder="Keyword" value="@if(!empty($searchedValues['q'])){{ $searchedValues['q'] }} @endif">
                            </div>
                        </div>
                    </div>

                    <!-- Location -->
                    <div class="sidebar-widget">
                        <h3>Location</h3>
                        <div class="input-with-icon">
                            <div id="autocomplete-container">
                                <input name="location" id="autocomplete-input" type="text" placeholder="Location" value="@if(!empty($searchedValues['location'])){{ $searchedValues['location'] }} @endif">
                            </div>
                            <i class="icon-material-outline-location-on"></i>
                        </div>
                    </div>

                <!-- Category -->
                    <div class="sidebar-widget">
                        <h3>Category</h3>
                        <select name="category[]" class="selectpicker default" multiple data-selected-text-format="count" data-size="7" title="All Categories" >
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}"
                                @if(!empty($searchedValues['category']))
                                    @foreach($searchedValues['category'] as $c)
                                        @if($c == $category->id)
                                            selected
                                        @endif
                                    @endforeach
                                @endif

                                >{{ $category->category_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="button ripple-effect">Search <i class="icon-material-outline-search"></i></button>


                    </form>
                </div>
            </div>

            <div class="col-xl-9 col-lg-8 content-left-offset">


                <h3 class="page-title">Search result for "{{ $searchKeyword }}"</h3>

                <div class="notify-box margin-top-15">


                    <div class="switch-container">
                        <h4 class="job-listing-company"><span class="switch-text">Showing {{ count($jobs) }} result(s)</span></h4>
                    </div>

                    <div class="sort-by">
                        <span>Sort by:</span>
                        <form action="{{ route('search.job') }}" method="get" id="searchJob3">
                            <input type="hidden" name="location" value="@if(!empty($searchedValues['location'])){{ $searchedValues['location'] }} @endif">
                            <input type="hidden" name="q" value="@if(!empty($searchedValues['q'])){{ $searchedValues['q'] }} @endif">

                            <div style="display: none">
                            <select name="category[]" class="selectpicker default" multiple data-selected-text-format="count" data-size="7" title="All Categories" >
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"
                                            @if(!empty($searchedValues['category']))
                                            @foreach($searchedValues['category'] as $c)
                                            @if($c == $category->id)
                                            selected
                                            @endif
                                            @endforeach
                                            @endif

                                    >{{ $category->category_name }}</option>
                                @endforeach
                            </select>
                            </div>



                            <select name="sort" class="selectpicker hide-tick" onchange="this.form.submit()">
                                <option value="latest">Latest</option>
                                <option value="oldest" @if(!empty($searchedValues['sort'])) @if($searchedValues['sort'] == "oldest") selected @endif  @endif>Oldest</option>
                            </select>
                        </form>
                    </div>
                </div>

                <div class="listings-container margin-top-35">

                    <!-- Job Listing -->
                    @foreach($jobs as $job)
                        <a href="{{ route('job.post.detail', $job->id) }}" class="job-listing">

                            <!-- Job Listing Details -->
                            <div class="job-listing-details">
                                <!-- Logo -->
                                <div class="job-listing-company-logo">
                                    <img src="{{ asset('public/company/images/profile/'.$job->company->profile_image ) }}" alt="">
                                </div>

                                <!-- Details -->
                                <div class="job-listing-description">
                                    <h4 class="job-listing-company">{{ $job->company->name }} <span class="verified-badge" title="Verified Employer" data-tippy-placement="top"></span></h4>
                                    <h3 class="job-listing-title">{{ $job->job_title }}</h3>
                                    <p class="job-listing-text">{!! substr($job->job_description, 0, 500) !!}{{ strlen($job->job_description) > 500 ? "..." : "" }}</p>
                                </div>

                            @if(Auth::guard('job_seeker')->check())
                                <!-- Bookmark -->
                                    <?php
                                    //check if this product is in wishlist
                                    $count = 0;
                                    foreach ($jobBookmarks as $b){
                                        if ($b->job_post_id == $job->id){
                                            $count++;
                                        }
                                    }
                                    ?>
                                    <span class="bookmark-icon bookmark-job @if($count > 0) bookmarked @endif" rel="{{ $job->id }}" ></span>
                                @endif
                            </div>

                            <!-- Job Listing Footer -->
                            <div class="job-listing-footer">
                                <ul>
                                    <li><i class="icon-material-outline-location-on"></i> {{ $job->job_location }}</li>
                                    <li><i class="icon-material-outline-business-center"></i> {{ $job->jobType->name }}</li>
                                    @if($job->show_salary == 1)
                                    <li><i class="icon-material-outline-account-balance-wallet"></i>@if(empty($job->min_job_salary) || empty($job->max_job_salary)) Negotiable @else {{ $job->min_job_salary }} NRP - {{ $job->max_job_salary }} NRP @endif</li>
                                    @endif
                                    <li><i class="icon-material-outline-access-time"></i> {{ $job->updated_at->diffForHumans() }}</li>
                                    <li><i class="icon-feather-user "></i> {{ $job->job_vacancy_number }} Vacancies</li>
                                </ul>
                            </div>
                        </a>
                @endforeach



                <!-- Pagination -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Pagination -->
                            <div class="pagination-container margin-top-30 margin-bottom-60">
                                <nav class="pagination">
                                    <ul>
                                        {{ $jobs->links() }}
                                        {{--<li class="pagination-arrow"><a href="#"><i class="icon-material-outline-keyboard-arrow-left"></i></a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#" class="current-page">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li class="pagination-arrow"><a href="#"><i class="icon-material-outline-keyboard-arrow-right"></i></a></li>--}}
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- Pagination / End -->

                </div>

            </div>
        </div>
    </div>

@endsection
