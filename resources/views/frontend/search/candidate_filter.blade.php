<?php
use App\Http\Controllers\Controller;
$jobBookmarkCount = Controller::jobBookmarkCount();
?>
@extends('frontend.layouts.front_design2')
@section('content')

    <!-- Spacer -->
    <div class="margin-top-90"></div>
    <!-- Spacer / End-->

    <div class="container">
        <div class="row">

            <div class="col-xl-3 col-lg-4">
                <form action="{{ route('filter.candidate') }}" method="get">
                    <div class="sidebar-container">

                        <!-- Location -->
                        <div class="sidebar-widget">
                            <h3>Name</h3>
                            <div class="input-with-icon">
                                <div id="autocomplete-container">
                                    <input name="q" type="text" placeholder="Candidate Name" value="@if(!empty($searchedValues['q'])){{ $searchedValues['q'] }} @endif">
                                </div>
                            </div>
                        </div>



                        <!-- Category -->
                        <div class="sidebar-widget">
                            <h3>Category</h3>
                            <select name="job_category[]" class="selectpicker default" multiple data-selected-text-format="count" data-size="7" title="All Categories" >
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}"
                                            @if(!empty($searchedValues['job_category']))
                                            @foreach($searchedValues['job_category'] as $c)
                                            @if($c == $category->id)
                                            selected
                                            @endif
                                            @endforeach
                                            @endif

                                    >{{ $category->category_name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <!-- Keywords -->
                        <div class="sidebar-widget">
                            <h3>Keywords</h3>
                            <div class="keywords-container">
                                <div class="keyword-input-container">
                                    <input type="text" data-role="tagsinput" class="keyword-input with-border" placeholder="Add keywords">
                                    <button class="keyword-input-button ripple-effect tags_button"><i class="icon-material-outline-add"></i></button>

                                </div>
                                <div class="keywords-list"><!-- keywords go here -->

                                    @if(!empty($searchedValues['skill']))
                                        @foreach($searchedValues['skill'] as $skill)
                                            <span class="keyword"><span class="keyword-remove"></span><span class="keyword-text">{{ $skill }}</span></span>
                                        @endforeach
                                    @endif

                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>



                        <button id="filterButton" class="button ripple-effect" type="submit">Filter <i class="icon-material-outline-search"></i></button>



                    </div>
                </form>

            </div>

            <div class="col-xl-9 col-lg-8 content-left-offset">

                <h3 class="page-title">Candidate Listing</h3>

                <div class="notify-box margin-top-15">
                    {{--<div class="switch-container">
                        <label class="switch"><input type="checkbox"><span class="switch-button"></span><span class="switch-text">Turn on email alerts for this search</span></label>
                    </div>--}}

                    <div class="switch-container">
                        <h4 class="job-listing-company"><span class="switch-text">Browse Candidates</span></h4>
                    </div>

                    <div class="sort-by">
                        <span>Sort by:</span>
                        <form action="{{ route('filter.candidate') }}" method="get" id="filterJob3">

                            <div style="display: none">
                                <select name="job_category[]" class="selectpicker default" multiple data-selected-text-format="count" data-size="7" title="All Categories" >
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}"
                                                @if(!empty($searchedValues['job_category']))
                                                @foreach($searchedValues['job_category'] as $c)
                                                @if($c == $category->id)
                                                selected
                                                @endif
                                                @endforeach
                                                @endif

                                        >{{ $category->category_name }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <select name="sort" class="selectpicker hide-tick" onchange="this.form.submit()">
                                <option value="latest">Latest</option>
                                <option value="oldest" @if(!empty($searchedValues['sort'])) @if($searchedValues['sort'] == "oldest") selected @endif  @endif>Oldest</option>
                            </select>
                        </form>
                    </div>
                </div>

                <div class="listings-container margin-top-35">

                    @foreach($candidates as $candidate)
                    <a href="{{ route('job.seeker.detail', $candidate->id) }}" class="job-listing">

                        <!-- Job Listing Details -->
                        <div class="job-listing-details">
                            <!-- Logo -->
                            <div class="job-listing-company-logo">
                                <img src="{{ asset('public/job_seeker/images/profile/'.$candidate->profile_image ) }}" alt="">
                            </div>

                            <!-- Details -->
                            <div class="job-listing-description">
                                <h4 class="job-listing-company">{{ $candidate->name }} <span class="verified-badge" title="Verified Employer" data-tippy-placement="top"></span></h4>
                                <h3 class="job-listing-title">{{ $candidate->tagline }}</h3>
                                {{--<p class="job-listing-text">{!! substr($candidate->description, 0, 500) !!}{{ strlen($candidate->description) > 500 ? "..." : "" }}</p>--}}
                            </div>

                        @if(Auth::guard('job_seeker')->check())
                            <!-- Bookmark -->
                                <?php
                                //check if this product is in wishlist
                                $count = 0;
                                foreach ($jobBookmarks as $b){
                                    if ($b->job_post_id == $job->id){
                                        $count++;
                                    }
                                }
                                ?>
                                <span class="bookmark-icon bookmark-job @if($count > 0) bookmarked @endif" rel="{{ $job->id }}" ></span>
                            @endif
                        </div>

                        <!-- Job Listing Footer -->
                        <div class="job-listing-footer">
                            <ul>
                                @if(!empty($candidate->address))
                                    <li><i class="icon-material-outline-location-on"></i> {{ $candidate->address }}</li>
                                @endif
                                <li><i class="icon-material-outline-business-center"></i> {{ $candidate->email }}</li>
                                <li><i class="icon-material-outline-access-time"></i> {{ $candidate->contact_number }}</li>
                            </ul>
                        </div>
                    </a>
                    @endforeach

                </div>



                <!-- Pagination -->
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Pagination -->
                            <div class="pagination-container margin-top-30 margin-bottom-60">
                                <nav class="pagination">
                                    <ul>

                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- Pagination / End -->

                </div>

            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.tags_button').click(function(e) {

                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();

                return false;
            });});
    </script>


    <script>
        $("#filterButton").click(function () {
            //let a = $(".keyword-text").html();
            $(".keyword-text").each(function() {
                //alert($(this).text());
                $(".keywords-list").append('<input type="hidden" name="skill[]" value="'+$(this).text()+'">');
            });
            //alert(a);
        });
    </script>
@endsection