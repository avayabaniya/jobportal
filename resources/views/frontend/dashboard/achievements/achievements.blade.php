@extends('frontend.dashboard_layouts.front_dashboard_design')
@section('content')

    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>My Achievements</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>Add Achievements</li>
                    </ul>
                </nav>
            </div>

            <form id="addAchievement" action="{{ route('job.seeker.achievements') }}" method="post" enctype="multipart/form-data">
                @csrf
                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-feather-folder-plus"></i>Add Achievement</h3>
                            </div>


                            <div class="content with-padding padding-bottom-10">
                                <div class="row">


                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Achievement Obtained Date</h5>
                                            <input id="date" autocomplete="off"  type="text" name="date" class="with-border">
                                        </div>
                                    </div>


                                    <div class="col-xl-6"></div>




                                    <div class="col-xl-12">
                                        <div class="submit-field">
                                            <h5>Achievement</h5>
                                            <input type="text" id="achievement" name="achievement" class="with-border">
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-xl-12">
                        <button id="saveChanges" type="submit" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i> Add Achievement</button>
                    </div>

                </div>
                <!-- Row / End -->
            </form>

            <!-- Row -->
            <div class="row" style="margin-top: 25px;">

                <!-- Dashboard Box -->
                <div class="col-xl-12">
                    <div class="dashboard-box margin-top-0">

                        <!-- Headline -->
                        <div class="headline">
                            <h3><i class="icon-line-awesome-trophy"></i> My Achievements</h3>
                        </div>

                        <div class="content">
                            <ul class="dashboard-box-list">
                                @foreach($achievements as $a)
                                    <li>
                                        <!-- Job Listing -->
                                        <div class="job-listing">

                                            <!-- Job Listing Details -->
                                            <div class="job-listing-details">

                                                <div class="job-listing-description">
                                                    <h3 class="job-listing-title"><a href="">{{ $a->achievement }}</a>

                                                    </h3>

                                                    <!-- Job Listing Footer -->
                                                    <div class="job-listing-footer">

                                                        <ul >
                                                            <li><i class="icon-material-outline-date-range"></i>Acquired on {{ $a->date}}</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Buttons -->
                                        <!-- Buttons -->
                                        <div class="buttons-to-right always-visible">
                                            <a href="{{ route('edit.job.seeker.achievement', $a->id) }}" class="button gray ripple-effect ico" title="Edit" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>

                                            <a href="javascript:" rel="{{$a->id}}" rel1="delete-achievement" class="button gray ripple-effect ico deleteRecord" title="Remove" data-tippy-placement="top">
                                                <i class="icon-feather-trash-2"></i>
                                            </a>

                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Row / End -->





            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
            <div class="small-footer margin-top-15">
                <div class="small-footer-copyrights">
                    © 2018 <strong>Hireo</strong>. All Rights Reserved.
                </div>
                <ul class="footer-social-links">
                    <li>
                        <a href="#" title="Facebook" data-tippy-placement="top">
                            <i class="icon-brand-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="Twitter" data-tippy-placement="top">
                            <i class="icon-brand-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="Google Plus" data-tippy-placement="top">
                            <i class="icon-brand-google-plus-g"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="LinkedIn" data-tippy-placement="top">
                            <i class="icon-brand-linkedin-in"></i>
                        </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- Footer / End -->

        </div>
    </div>

@endsection

@section('scripts')


    <script>
        $(document).ready(function () {
            $('#tags_button').click(function(e) {

                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();

                return false;
            });});
    </script>

    <script>
        $(document).ready(function () {

            $("#addAchievement").validate({
                rules: {
                    date: {
                        required:true
                    },
                    achievement: {
                        required:true
                    }
                },
                messages:{
                    date: {
                        required: "Please Enter Your Achievement Obtained Date"
                    },
                    achievement: {
                        required: "Please Enter Your Achievement"
                    }
                }

            });

        });
    </script>

    <script>
        $("#date").datepicker({
            autoclose: true,
            endDate:new Date(),
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years"
        });

    </script>

@endsection
