@extends('frontend.dashboard_layouts.front_dashboard_design')
@section('content')

    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Edit Achievement</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>Edit Achievements</li>
                    </ul>
                </nav>
            </div>

            <form id="editAchievement" action="{{ route('edit.job.seeker.achievement', $achievement->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-feather-folder-plus"></i>Edit Achievement</h3>
                            </div>


                            <div class="content with-padding padding-bottom-10">
                                <div class="row">


                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Achievement Obtained Date</h5>
                                            <input id="date" autocomplete="off"  type="text" name="date" class="with-border" value="{{ $achievement->date }}">
                                        </div>
                                    </div>


                                    <div class="col-xl-6"></div>




                                    <div class="col-xl-12">
                                        <div class="submit-field">
                                            <h5>Achievement</h5>
                                            <input type="text" id="achievement" name="achievement" class="with-border" value="{{ $achievement->achievement }}">
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-xl-12">
                        <button id="saveChanges" type="submit" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i> Edit Achievement</button>
                    </div>

                </div>
                <!-- Row / End -->
            </form>

            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
            <div class="small-footer margin-top-15">
                <div class="small-footer-copyrights">
                    © 2018 <strong>Hireo</strong>. All Rights Reserved.
                </div>
                <ul class="footer-social-links">
                    <li>
                        <a href="#" title="Facebook" data-tippy-placement="top">
                            <i class="icon-brand-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="Twitter" data-tippy-placement="top">
                            <i class="icon-brand-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="Google Plus" data-tippy-placement="top">
                            <i class="icon-brand-google-plus-g"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="LinkedIn" data-tippy-placement="top">
                            <i class="icon-brand-linkedin-in"></i>
                        </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- Footer / End -->

        </div>
    </div>

@endsection

@section('scripts')


    <script>
        $(document).ready(function () {
            $('#tags_button').click(function(e) {

                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();

                return false;
            });});
    </script>

    <script>
        $(document).ready(function () {

            $("#editAchievement").validate({
                rules: {
                    date: {
                        required:true
                    },
                    achievement: {
                        required:true
                    }
                },
                messages:{
                    date: {
                        required: "Please Enter Your Achievement Obtained Date"
                    },
                    achievement: {
                        required: "Please Enter Your Achievement"
                    }
                }

            });

        });
    </script>

    <script>
        $("#date").datepicker({
            autoclose: true,
            endDate:new Date(),
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years"
        });

    </script>

@endsection
