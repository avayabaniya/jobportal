@extends('frontend.dashboard_layouts.front_dashboard_design')
@section('content')

    <!-- Dashboard Content
        ================================================== -->
    <div class="dashboard-content-container" data-simplebar>
        <form action="{{ route('company.setting') }}" method="post" enctype="multipart/form-data" id="jobseekerSettingForm">
            @csrf
            <div class="dashboard-content-inner" >

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>Settings</h3>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Dashboard</a></li>
                            <li>Settings</li>
                        </ul>
                    </nav>
                </div>

                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-account-circle"></i> My Account</h3>
                            </div>

                            <div class="content with-padding padding-bottom-0">

                                <div class="row">

                                    <div class="col-auto">
                                        <div class="avatar-wrapper" data-tippy-placement="bottom" title="Change Avatar">
                                            <img class="profile-pic" src="{{ asset('public/company/images/profile/'.$company->profile_image) }}" alt="" />
                                            <div class="upload-button"></div>
                                            <input id="image" class="file-upload img-upload" name="profile_image" type="file" accept="image/*"  onchange="return fileValidation()"/>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="row">

                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Company Name</h5>
                                                    <input type="text" name="name" class="with-border" value="{{$company->name}}">
                                                </div>
                                            </div>

                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Email</h5>
                                                    <input type="text" name="email" class="with-border" value="{{ $company->email }}">
                                                </div>
                                            </div>

                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Contact Number</h5>
                                                    <input type="text" name="contact_number" class="with-border" value="{{ $company->contact_number }}">
                                                </div>
                                            </div>

                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Company registration number</h5>
                                                    <input type="text" name="pin_number" class="with-border" value="{{ $company->pin_number }}">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-face"></i> Company Profile</h3>
                            </div>

                            <div class="content">
                                <ul class="fields-ul">
                                    <li>
                                        <div class="row">
                                            <div class="col-xl-4">
                                                <div class="submit-field">
                                                    <h5>Address</h5>
                                                    <input type="text" name="address" class="with-border" value="{{ $company->address }}">
                                                </div>
                                            </div>

                                            <div class="col-xl-4">
                                                <div class="submit-field">
                                                    <h5>Established Date</h5>
                                                    <input type="text" name="established_date" class="with-border datepicker-dob" autocomplete="off" value="{{ $company->established_date }}">
                                                </div>
                                            </div>

                                            <div class="col-xl-4">
                                                <div class="submit-field">
                                                    <h5>Industry Category</h5>
                                                    <select id="job_category" class="selectpicker with-border" name="job_category" data-size="7">
                                                        @foreach($jobCategories as $jobCategory)
                                                            <option value="{{ $jobCategory->id  }}">{{ $jobCategory->category_name }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                            </div>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-xl-4">
                                                <div class="submit-field">
                                                    <h5>Number of employees</h5>
                                                    <input type="text" name="number_of_employees" class="with-border" value="{{ $company->number_of_employees }}">
                                                </div>
                                            </div>

                                            <div class="col-xl-4">
                                                <div class="submit-field">
                                                    <h5>Company Site</h5>
                                                    <input type="text" name="company_site" class="with-border" value="{{ $company->company_site }}">
                                                </div>
                                            </div>

                                            <div class="col-xl-4">
                                                <div class="submit-field">
                                                    <h5>LinkedIn</h5>
                                                    <input type="text" name="linkedin" class="with-border" value="{{ $company->linkedin }}">
                                                </div>
                                            </div>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5> Cover Image </h5>
                                                    <!-- Attachments -->

                                                    <div class="clearfix"></div>

                                                    <!-- Upload Button -->
                                                    <div class="uploadButton margin-top-0">
                                                        <input class="uploadButton-input" type="file" accept="image/*" id="upload" name="cover_image" onchange="return fileValidation2()" />
                                                        <label class="uploadButton-button ripple-effect" for="upload">Upload Files</label>
                                                        <span class="uploadButton-file-name">Upload cover image</span>
                                                    </div>
                                                    @if(!empty($company->cover_image))
                                                        <img style="display: block;" src="{{ asset('public/company/images/cover/'.$company->cover_image) }}" alt="" height="100px" width="150px">
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-xl-6">

                                            </div>

                                            <div class="col-xl-12">
                                                <div class="submit-field">
                                                    <h5>Profile Description</h5>
                                                    <textarea id="description" name="description" cols="30" rows="5" class="with-border">
                                                        {!! $company->description !!}
                                                    </textarea>
                                                </div>
                                            </div>



                                            <div class="col-xl-12">
                                                <div class="single-page-section">
                                                    {{--<h3 class="margin-bottom-30">Location</h3>--}}
                                                    <div class="row">
                                                        <div class="col-xl-4">
                                                            <div class="submit-field">
                                                                <h5>Map Coordinated</h5>
                                                                <input type="text" name="map_address" class="with-border" value="" id="searchmap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="map-canvas">

                                                    </div>
                                                    <div class="row" style="margin-top: 20px;">
                                                        <div class="col-xl-6">
                                                            <div class="submit-field">
                                                                <h5>Lat</h5>
                                                                <input type="text" name="lat" class="with-border" value="{{ $company->lat }}" id="lat" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-6">
                                                            <div class="submit-field">
                                                                <h5>lng</h5>
                                                                <input type="text" name="lng" class="with-border" value="{{ $company->lng }}" id="lng" readonly>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div id="test1" class="dashboard-box">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-lock"></i> Password & Security</h3>
                            </div>

                            <div class="content with-padding">
                                <div class="row">
                                    <div class="col-xl-4">
                                        <div class="submit-field">
                                            <h5>Current Password</h5>
                                            <input id="current_password" type="password" name="current_password" class="with-border">
                                            <p style="display: inline-block" id="correct_password" class="error"></p>

                                        </div>
                                    </div>

                                    <div class="col-xl-4">
                                        <div class="submit-field">
                                            <h5>New Password</h5>
                                            <input id="new_password" type="password" name="new_password" class="with-border" readonly>
                                        </div>
                                    </div>

                                    <div class="col-xl-4">
                                        <div class="submit-field">
                                            <h5>Repeat New Password</h5>
                                            <input id="confirm_password" type="password" name="confirm_password" class="with-border" readonly>
                                        </div>
                                    </div>

                                    <div class="col-xl-12">
                                        <div class="checkbox">

                                            <p><span class="checkbox-icon"></span> **Enter all 3 fields to update password</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="col-xl-12">
                        <button type="submit" class="button ripple-effect big margin-top-30">Save Changes</button>
                    </div>

                </div>
                <!-- Row / End -->

                <!-- Footer -->
                <div class="dashboard-footer-spacer"></div>
                <div class="small-footer margin-top-15">
                    <div class="small-footer-copyrights">
                        © 2018 <strong>Hireo</strong>. All Rights Reserved.
                    </div>
                    <ul class="footer-social-links">
                        <li>
                            <a href="#" title="Facebook" data-tippy-placement="top">
                                <i class="icon-brand-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Twitter" data-tippy-placement="top">
                                <i class="icon-brand-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Google Plus" data-tippy-placement="top">
                                <i class="icon-brand-google-plus-g"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="LinkedIn" data-tippy-placement="top">
                                <i class="icon-brand-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <!-- Footer / End -->

            </div>
        </form>
    </div>
    <!-- Dashboard Content / End -->

@endsection

@section('styles')
    <style>
        #map-canvas{
            width: 100%;
            height: 400px;
        }
    </style>
@endsection

@section('scripts')

    <script>
        tinymce.init({
            selector: '#description'
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#tags_button').click(function(e) {

                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();

                return false;
            });});
    </script>

    <script>
        $(document).ready(function () {

            $("#jobseekerSettingForm").validate({
                rules: {
                    email:{
                        required:true,
                        email:true
                    },
                    name:{
                        required:true
                    },
                    contact_number:{
                        required:true,
                        number:true
                    },
                    company_site:{
                        required:false,
                        url:true
                    },
                    linkedin:{
                        required:false,
                        url:true
                    },
                    new_password:{
                        required: false
                    },
                    confirm_password:{
                        required: false,
                        equalTo:"#new_password"
                    }
                },
                messages:{
                    email: {
                        required: "Please enter a valid email"
                    },
                    name: {
                        required: "Your company name is required"
                    },
                    contact_number: {
                        required: "Contact Number is required",
                        number: "Enter a valid number"
                    },
                    company_site: {
                        url: "Please enter a valid url"
                    },
                    linkedin: {
                        url: "Please enter a valid url"
                    },
                    confirm_password: {
                        equalTo: "This password should match your new password"
                    }
                }

            });

        });
    </script>

    <script>
        $("#current_password").keyup(function(){
            var current_password = $("#current_password").val();
            //alert(current_password);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'post',
                url:'check-password',
                data:{current_password:current_password},
                success:function(resp){
                    console.log(resp);

                    if(resp=="false"){

                        $("#correct_password").text("Entered value does not match the current password").css("color","red");
                        $("#new_password").attr("readonly", true);
                        $("#confirm_password").attr("readonly", true);

                    }else if(resp=="true"){

                        $("#correct_password").text("Current password matched").css("color","green");
                        $("#new_password").attr("readonly", false);
                        $("#confirm_password").attr("readonly", false);
                    }
                },error:function(resp){
                    console.log(resp);
                }
            });
        });
    </script>

    {{--only upload image--}}
    <script type="text/javascript">
        function fileValidation(){
            var fileInput = document.getElementById('image');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
            if(!allowedExtensions.exec(filePath)){
                alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
                fileInput.value = '';
                return false;
            }
            else{
                //Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
                    };
                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        }
    </script>


    {{--only upload image--}}
    <script type="text/javascript">
        function fileValidation2(){
            var fileInput = document.getElementById('upload');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
            if(!allowedExtensions.exec(filePath)){
                alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
                fileInput.value = '';
                return false;
            }
            else{
                //Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
                    };
                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        }
    </script>


    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyDXXKZS3Su8ZcIRlLQsBdXBLY4LMt-vmlk&libraries=places">
    </script>

    <script>
        $(document).ready(function () {
            var lat = {{ $company->lat }};
            var lng = {{ $company->lng }};

            if (!lat || !lng)
            {
                lat = 27.72;
                lng = 85.36;
            }

            var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                    lat: lat,
                    lng: lng
                },
                zoom:15
            });

            var marker = new google.maps.Marker({
                position: {
                    lat: lat,
                    lng: lng
                },
                map: map,
                draggable:true
            });

            // Create the search box and link it to the UI element.
            var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });

            google.maps.event.addListener(searchBox, 'places_changed', function () {

                var places = searchBox.getPlaces();
                console.log(places);
                var bounds = new google.maps.LatLngBounds();
                var i, place;

                for (i = 0; place = places[i]; i++)
                {
                    bounds.extend(place.geometry.location);
                    marker.setPosition(place.geometry.location);
                }

                map.fitBounds(bounds);
                map.setZoom(15);

            });


            google.maps.event.addListener(marker, 'position_changed', function () {

                var lat = marker.getPosition().lat();
                var lng = marker.getPosition().lng();

                $('#lat').val(lat);
                $('#lng').val(lng);

            });
        });
    </script>



@endsection