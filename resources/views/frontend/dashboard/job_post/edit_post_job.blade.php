@extends('frontend.dashboard_layouts.front_dashboard_design')
@section('content')
    <div class="dashboard-content-container" data-simplebar>
    <div class="dashboard-content-inner" >

        <!-- Dashboard Headline -->
        <div class="dashboard-headline">
            <h3>Update Job Post : {{ $job->job_title }}</h3>

            <!-- Breadcrumbs -->
            <nav id="breadcrumbs" class="dark">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Dashboard</a></li>
                    <li>Update Job Post</li>
                </ul>
            </nav>
        </div>

        <form id="editPostJobForm" action="{{ route('edit.job.post', $job->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="company_id" value="{{ Auth::guard('company')->user()->id }}">
        <!-- Row -->
        <div class="row">

            <!-- Dashboard Box -->
            <div class="col-xl-12">
                <div class="dashboard-box margin-top-0">

                    <!-- Headline -->
                    <div class="headline">
                        <h3><i class="icon-feather-folder-plus"></i> Job Submission Form</h3>
                    </div>


                    <div class="content with-padding padding-bottom-10">
                        <div class="row">

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Job Title</h5>
                                    <input type="text" value="{{ $job->job_title }}" name="job_title" class="with-border">
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Job Type</h5>
                                    <select class="selectpicker with-border" name="job_type" data-size="7" title="Select Job Type">
                                        @foreach($jobTypes as $jobTypeOption)
                                            <option value="{{ $jobTypeOption->id }}" @if($jobTypeOption->id == $job->job_type ) selected @endif>{{ $jobTypeOption->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Job Category</h5>
                                    <select class="selectpicker with-border" name="job_category" data-size="7" title="Select Category">
                                        @foreach($jobCategories as $jobCategory)
                                            <option value="{{ $jobCategory->id }}" @if($jobCategory->id == $job->job_category ) selected @endif>{{ $jobCategory->category_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Location</h5>
                                    <div class="input-with-icon">
                                        <div id="autocomplete-container">
                                            <input id="autocomplete-input" value="{{ $job->job_location }}" name="job_location" class="with-border" type="text" placeholder="Type Address">
                                        </div>
                                        <i class="icon-material-outline-location-on"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Publish Date</h5>
                                    <input  value="{{ date('d M, Y', strtotime($job->job_publish_date)) }}" id="job_publish_date" autocomplete="off"  type="text" name="job_publish_date" class="with-border">
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Expiry Date</h5>
                                    <input  value="{{ date('d M, Y', strtotime($job->job_expiry_date)) }}" readonly id="job_expiry_date" autocomplete="off"  type="text" name="job_expiry_date" class="with-border">
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Number of Vacancies</h5>
                                    <input id="job_vacancy_number" type="text" name="job_vacancy_number" class="with-border" value="{{ $job->job_vacancy_number }}">
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Salary in NRS<span>(optional)</span></h5>
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="input-with-icon">
                                                <input id="min_job_salary" class="with-border" value="{{ $job->min_job_salary }}" type="text" name="min_job_salary" placeholder="Min Salary">
                                                {{--<i class="currency">NRS</i>--}}
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="input-with-icon">
                                                <input id="max_job_salary" class="with-border" value="{{ $job->max_job_salary }}" type="text" name="max_job_salary"  placeholder="Max Salary">
                                                {{--<i class="currency">NRS</i>--}}
                                            </div>
                                        </div>
                                        <div class="col-xl-12">
                                            <p style="display: inline-block" id="max_job_salary-error2" class="error"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5> &nbsp; </h5>
                                    <div class="checkbox" style="margin-top: 10px;">
                                        <input type="checkbox" id="chekcbox1" name="show_salary" @if($job->show_salary == 1) checked="" @else @endif>
                                        <label for="chekcbox1"><span class="checkbox-icon"></span> Display Salary?</label>
                                    </div>
                                </div>
                            </div>

                           {{-- <div class="col-xl-4">
                                <div class="submit-field">
                                    <h5>Tags <span>(optional)</span>  <i class="help-icon" data-tippy-placement="right" title="Maximum of 10 tags"></i></h5>
                                    <div class="keywords-container">
                                        <div class="keyword-input-container">
                                            <input type="text" class="keyword-input with-border" placeholder="e.g. job title, responsibilites"/>
                                            <button class="keyword-input-button ripple-effect"><i class="icon-material-outline-add"></i></button>
                                        </div>
                                        <div class="keywords-list"><!-- keywords go here --></div>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            </div>--}}

                            <div class="col-xl-12">
                                <div class="submit-field">
                                    <h5>Job Description</h5>
                                    <textarea id="description" name="job_description" rows="10" class="with-border">
                                        {{ $job->job_description }}
                                    </textarea>
                                    {{--<div class="uploadButton margin-top-30">
                                        <input class="uploadButton-input" type="file" accept="image/*, application/pdf" id="upload" multiple/>
                                        <label class="uploadButton-button ripple-effect" for="upload">Upload Files</label>
                                        <span class="uploadButton-file-name">Images or documents that might be helpful in describing your job</span>
                                    </div>--}}
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <div class="col-xl-12">
                <button id="post_job_submit_button" type="submit" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i> Update Job Post</button>
            </div>

        </div>
        <!-- Row / End -->
        </form>

        <!-- Footer -->
        <div class="dashboard-footer-spacer"></div>
        <div class="small-footer margin-top-15">
            <div class="small-footer-copyrights">
                © 2018 <strong>Hireo</strong>. All Rights Reserved.
            </div>
            <ul class="footer-social-links">
                <li>
                    <a href="#" title="Facebook" data-tippy-placement="top">
                        <i class="icon-brand-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="Twitter" data-tippy-placement="top">
                        <i class="icon-brand-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="Google Plus" data-tippy-placement="top">
                        <i class="icon-brand-google-plus-g"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="LinkedIn" data-tippy-placement="top">
                        <i class="icon-brand-linkedin-in"></i>
                    </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Footer / End -->

    </div>
</div>
@endsection

@section('scripts')
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=05fgyydvhrzu7w93s0m0hfu5plg2dthhn2v9tjqkyeb8fpkb"></script>
    <script>
        tinymce.init({
            selector: '#description'
        });
    </script>

    <script>
        $(document).ready(function () {

            $("#editPostJobForm").validate({
                rules: {
                    job_title:{
                        required:true
                    },
                    job_category: {
                        required:true
                    },
                    job_location: {
                        required:true
                    },
                    job_publish_date: {
                        required:true
                    },
                    job_expiry_date: {
                        required:true
                    },
                    job_vacancy_number: {
                        required:true,
                        digits:true,
                        min:1
                    },
                    min_job_salary: {
                        number: true,
                        min: 0
                    },
                    max_job_salary: {
                        number: true
                    }
                },
                messages:{
                    job_title: {
                        required: "Please Enter Job Title"
                    },
                    job_category: {
                        required: "Please Select Job Category"
                    },
                    job_location: {
                        required: "Please Enter Location "
                    },
                    job_publish_date: {
                        required: "Please Select Expiry Date"
                    },
                    job_expiry_date: {
                        required: "Please Select Expiry Date"
                    },
                    job_vacancy_number: {
                        required: "Please Enter Number of Vacancies",
                        digits: "Entered value must be a number",
                        min: "Entered value cannot be less than 1"
                    },
                    min_job_salary: {
                        number: "Must be a number",
                        min: "Cannot be less than 0"
                    },
                    max_job_salary: {
                        number: "Must be a number"
                    }

                }

            });

        });
    </script>

    <script>
        $(document).ready(function () {
            $("#max_job_salary").keyup(function () {

                var minSalary = parseFloat($("#min_job_salary").val());
                var maxSalary = parseFloat($(this).val());

                if (maxSalary < minSalary)
                {
                    //alert('Max salary cannot be less than min salary');
                    $("#max_job_salary-error2").html("Max salary must be greater than min salary");
                    $("#post_job_submit_button").prop('disabled', true);
                    $("#post_job_submit_button").css('background-color', 'red');

                }
                else
                {
                    $("#max_job_salary-error2").html("");
                    $("#post_job_submit_button").prop('disabled', false);
                    $("#post_job_submit_button").css('background-color', '');


                }


            });


        });
    </script>

    <script>
        $(document).ready(function () {

            $("#post_job_submit_button").click(function () {
                var minSalary = parseFloat($("#min_job_salary").val());
                var maxSalary = parseFloat($("#max_job_salary").val());


                if (maxSalary < minSalary || !minSalary || !maxSalary )
                {
                    $("#max_job_salary-error2").html("Max salary must be greater than min salary");
                    $("#post_job_submit_button").prop('disabled', true);
                    $("#post_job_submit_button").css('background-color', 'red');

                }
                else
                {
                    $("#max_job_salary-error2").html("");
                    $("#post_job_submit_button").prop('disabled', false);
                    $("#post_job_submit_button").css('background-color', '');


                }
            });

            $("#min_job_salary").keyup(function () {

                var minSalary = parseFloat($("#min_job_salary").val());
                var maxSalary = parseFloat($("#max_job_salary").val());

                if (maxSalary < minSalary)
                {
                    //alert('Max salary cannot be less than min salary');
                    $("#max_job_salary-error2").html("Max salary must be greater than min salary");
                    $("#post_job_submit_button").prop('disabled', true);
                    $("#post_job_submit_button").css('background-color', 'red');

                }
                else
                {
                    $("#max_job_salary-error2").html("");
                    $("#post_job_submit_button").prop('disabled', false);
                    $("#post_job_submit_button").css('background-color', '');


                }
            });
        });
    </script>

    <script>
        $("#job_publish_date").datepicker({
            autoclose: true,
            todayHighlight: true,
            startDate:new Date(),
            format: 'dd M, yyyy'
        });

    </script>

    <script>
        $("#job_publish_date").change(function () {
            var start_year = $(this).val();

            $("#job_expiry_date").removeAttr('readonly');
            $("#job_expiry_date").datepicker('destroy');
            $("#job_expiry_date").datepicker({
                autoclose: true,
                todayHighlight: true,
                startDate:new Date(start_year),
                format: 'dd M, yyyy'
            });
        });

        $("#job_expiry_date").keyup(function () {
            $(this).val('');
        });
    </script>


@endsection