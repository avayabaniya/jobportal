@extends('frontend.dashboard_layouts.front_dashboard_design')
@section('content')

    <!-- Dashboard Content
        ================================================== -->
    <div class="dashboard-content-container" data-simplebar>
        <form action="{{ route('candidate.email', $candidate->id) }}" method="post" enctype="multipart/form-data" id="sendMailToCandidateForm">
            @csrf
            <div class="dashboard-content-inner" >

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>Candidate Info</h3>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Dashboard</a></li>
                            <li>Mail Candidate</li>
                        </ul>
                    </nav>
                </div>

                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-account-circle"></i> Candidate Info</h3>
                            </div>

                            <div class="content with-padding padding-bottom-0">

                                <div class="row">

                                    <div class="col-auto">
                                        <div class="avatar-wrapper" data-tippy-placement="bottom">
                                            <img class="profile-pic" src="{{ asset('public/job_seeker/images/profile/'.$candidate->profile_image) }}" alt="" />
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Full Name</h5>
                                                    <input type="hidden" name="candidate_id" value="{{ $candidate->id }}">
                                                    <input readonly type="text" style="border: none" name="name" class="with-border" value="{{$candidate->name}}">
                                                </div>
                                            </div>

                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Email</h5>
                                                    <input type="text" readonly style="border: none" name="email" class="with-border" value="{{ $candidate->email }}">
                                                </div>
                                            </div>

                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Contact Number</h5>
                                                    <input readonly type="text" style="border: none" name="contact_number" class="with-border" value="{{ $candidate->contact_number }}">
                                                </div>
                                            </div>

                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Gender</h5>
                                                    <input readonly type="text" style="border: none" name="contact_number" class="with-border" value="@if($candidate->gender == 0)Male @endif
                                                    @if($candidate->gender == 1)Female @endif
                                                    @if($candidate->fender == 2)Other @endif
                                                    ">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-feather-mail"></i>Send Mail</h3>
                            </div>

                            <div class="content">
                                <ul class="fields-ul">
                                    <li>
                                        <div class="row">
                                            <div class="col-xl-4">
                                                <div class="submit-field">
                                                    <h5>Subject</h5>
                                                    <input type="text" name="subject" class="with-border">
                                                </div>
                                            </div>

                                        </div>
                                    </li>

                                    <li>
                                        <div class="row">

                                            <div class="col-xl-12">
                                                <div class="submit-field">
                                                    <h5>Message</h5>
                                                    <textarea id="message" name="message" cols="80" rows="5" class="with-border">

                                                    </textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12">
                        <button type="submit" id="sendMail" class="button ripple-effect big margin-top-30">Send Mail</button>
                    </div>

                </div>
                <!-- Row / End -->

                <!-- Footer -->
                <div class="dashboard-footer-spacer"></div>
                <div class="small-footer margin-top-15">
                    <div class="small-footer-copyrights">
                        © 2018 <strong>Hireo</strong>. All Rights Reserved.
                    </div>
                    <ul class="footer-social-links">
                        <li>
                            <a href="#" title="Facebook" data-tippy-placement="top">
                                <i class="icon-brand-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Twitter" data-tippy-placement="top">
                                <i class="icon-brand-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Google Plus" data-tippy-placement="top">
                                <i class="icon-brand-google-plus-g"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="LinkedIn" data-tippy-placement="top">
                                <i class="icon-brand-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <!-- Footer / End -->

            </div>
        </form>
    </div>
    <!-- Dashboard Content / End -->

@endsection

@section('scripts')

    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=05fgyydvhrzu7w93s0m0hfu5plg2dthhn2v9tjqkyeb8fpkb"></script>
    <script>
        tinymce.init({
            selector: '#message'
        });
    </script>


    <script>
        $(document).ready(function () {

            $("#sendMailToCandidateForm").validate({
                rules: {
                    subject:{
                        required:true
                    },
                    message:{
                        required:true
                    }
                },
                messages:{
                    subject: {
                        required: "Please enter a subject"
                    },
                    message: {
                        required: "Please enter your message"
                    }
                }

            });

        });
    </script>

@endsection