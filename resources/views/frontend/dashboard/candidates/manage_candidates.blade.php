@extends('frontend.dashboard_layouts.front_dashboard_design')
@section('content')

    <!-- Dashboard Container -->
        <!-- Dashboard Content
        ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner" >

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>Manage Candidates</h3>
                    <span class="margin-top-7">Job Applications for <a href="{{ route('job.post.detail', $job->id) }}">{{ $job->job_title }}</a></span>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Dashboard</a></li>
                            <li>Manage Candidates</li>
                        </ul>
                    </nav>
                </div>

                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-supervisor-account"></i> {{ $job->candidates }} Candidates</h3>
                            </div>

                            <div class="content">
                                <ul class="dashboard-box-list">

                                    @foreach($candidates as $c)
                                    <li>
                                        <!-- Overview -->
                                        <div class="freelancer-overview manage-candidates">
                                            <div class="freelancer-overview-inner">

                                                <!-- Avatar -->
                                                <div class="freelancer-avatar">
                                                    <div class="verified-badge"></div>
                                                    <a href="#"><img src="{{asset('public/job_seeker/images/profile/'.$c->jobSeeker->profile_image)}}" alt=""></a>
                                                </div>

                                                <!-- Name -->
                                                <div class="freelancer-name">
                                                    <h4><a href="{{ route('job.seeker.detail', $c->jobSeeker->id) }}">{{ $c->jobSeeker->name }} {{--<img class="flag" src="images/flags/au.svg" alt="" title="Australia" data-tippy-placement="top">--}}</a></h4>

                                                    <!-- Details -->
                                                    <span class="freelancer-detail-item"><a href="#"><i class="icon-feather-mail"></i> {{ $c->jobSeeker->email }}</a></span>
                                                    <span class="freelancer-detail-item"><i class="icon-feather-phone"></i> {{ $c->jobSeeker->contact_number }}</span>

                                                    <!-- Rating -->
                                                    {{--<div class="freelancer-rating">
                                                        <div class="star-rating" data-rating="5.0"></div>
                                                    </div>--}}

                                                    <!-- Buttons -->
                                                    <div class="buttons-to-right always-visible margin-top-25 margin-bottom-5">
                                                        @if(!empty($c->jobSeeker->cv))
                                                            <a target="_blank" href="{{ asset('public/job_seeker/files/'.$c->jobSeeker->cv) }}" class="button ripple-effect"><i class="icon-feather-file-text"></i> Download CV</a>
                                                        @else
                                                            <span class="button ripple-effect"><i class="icon-feather-file-text"></i> CV not available</span>
                                                        @endif
                                                            <a href="{{ route('candidate.email',$c->jobSeeker->id)  }}" class="button dark ripple-effect"><i class="icon-feather-mail"></i> Send Mail</a>
                                                            <a href="{{ route('accept.candidate', $c->id) }}" class="button @if($c->application_status == 1) green @else gray @endif ripple-effect ico" title="@if($c->application_status == 1) Deselect Candidate @else Accept Candidate @endif" data-tippy-placement="top"><i class="icon-material-outline-check"></i></a>
                                                            <a href="javascript:" rel="{{$c->id}}" rel1="delete-candidates" class="button gray ripple-effect ico deleteRecord" title="Remove" data-tippy-placement="top">
                                                                <i class="icon-feather-trash-2"></i>
                                                            </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Row / End -->

                <!-- Footer -->
                <div class="dashboard-footer-spacer"></div>
                <div class="small-footer margin-top-15">
                    <div class="small-footer-copyrights">
                        © 2018 <strong>Hireo</strong>. All Rights Reserved.
                    </div>
                    <ul class="footer-social-links">
                        <li>
                            <a href="#" title="Facebook" data-tippy-placement="top">
                                <i class="icon-brand-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Twitter" data-tippy-placement="top">
                                <i class="icon-brand-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="Google Plus" data-tippy-placement="top">
                                <i class="icon-brand-google-plus-g"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" title="LinkedIn" data-tippy-placement="top">
                                <i class="icon-brand-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <!-- Footer / End -->

            </div>
        </div>
        <!-- Dashboard Content / End -->


    <!-- Dashboard Container / End -->

@endsection

@section('scripts')

    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=05fgyydvhrzu7w93s0m0hfu5plg2dthhn2v9tjqkyeb8fpkb"></script>
    <script>
        tinymce.init({
            selector: '#email-message'
        });
    </script>

@endsection