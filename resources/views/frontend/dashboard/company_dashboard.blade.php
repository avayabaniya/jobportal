@extends('frontend.dashboard_layouts.front_dashboard_design')
@section('content')

<div class="dashboard-content-container" data-simplebar>
    <div class="dashboard-content-inner" >

        @if($company->verification_status == 1 || $company->status == 1)
        <!-- Dashboard Headline -->
        <div class="dashboard-headline">
            <h3>Hello, {{ $company->name }}</h3>
            <span>We are glad to see you again!</span>

            <!-- Breadcrumbs -->
            <nav id="breadcrumbs" class="dark">
                <ul>
                    <li><a href="{{ route('index') }}">Home</a></li>
                    <li>Dashboard</li>
                </ul>
            </nav>
        </div>

        <!-- Fun Facts Container -->
        <div class="fun-facts-container">
            <div class="fun-fact" data-fun-fact-color="#36bd78">
                <div class="fun-fact-text">
                    <span>Active Job Posts</span>
                    <h4>{{ $activeJobPostCount }}</h4>
                </div>
                <div class="fun-fact-icon"><i class="icon-material-outline-business-center"></i></div>
            </div>
            <div class="fun-fact" data-fun-fact-color="#b81b7f">
                <div class="fun-fact-text">
                    <span>Inactive Job Posts</span>
                    <h4>{{ $expiredJobPostCount }}</h4>
                </div>
                <div class="fun-fact-icon"><i class="icon-material-outline-business-center"></i></div>
            </div>
            <div class="fun-fact" data-fun-fact-color="#efa80f">
                <div class="fun-fact-text">
                    <span>Candidates</span>
                    <h4>{{ $candidatesCount }}</h4>
                </div>
                <div class="fun-fact-icon"><i class="icon-material-outline-rate-review"></i></div>
            </div>

            <!-- Last one has to be hidden below 1600px, sorry :( -->
            <div class="fun-fact" data-fun-fact-color="#2a41e6">
                <div class="fun-fact-text">
                    <span>This Month Views</span>
                    <h4>987</h4>
                </div>
                <div class="fun-fact-icon"><i class="icon-material-outline-group"></i></div>
            </div>
        </div>

        @if ($activeJobPostCount != 0)
        <div class="row">
            <div class="col-xl-12">
                <div class="dashboard-box">
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>
        @endif
        <!-- Row -->
        <div class="row">

            <!-- Dashboard Box -->
            <div class="col-xl-6">
                <div class="dashboard-box">
                    <div class="headline">
                        <h3><i class="icon-material-baseline-notifications-none"></i>Recent Notifications</h3>
                        {{--<button class="mark-as-read ripple-effect-dark" data-tippy-placement="left" title="Mark all as read">
                            <i class="icon-feather-check-square"></i>
                        </button>--}}
                    </div>
                    <div class="content">
                        <ul class="dashboard-box-list">
                            @if( Auth::guard('company')->user()->unreadNotifications->count())
                                @foreach( Auth::guard('company')->user()->unreadNotifications as $notification)

                                    <li class="notifications-not-read">

                                        @if(empty($notification->data['jobSeeker']['profile_image']))
                                            <span class="notification-icon"><i class="icon-material-outline-group"></i></span>
                                        @else
                                            <span class="notification-icon"><img src="{{ asset('public/job_seeker/images/profile/'.$notification->data['jobSeeker']['profile_image']) }}" alt=""></span>
                                        @endif
                                        <span class="notification-text">

                                            @if($notification->type == "App\Notifications\JobApplyNotification")
                                                <strong>{{ $notification->data['jobSeeker']['name']  }}</strong> applied for a job <a href="{{ route('manage.candidates',$notification->data['jobPost']['id'] ) }}">{{ $notification->data['jobPost']['job_title']  }}</a>
                                            @else
                                                <?php
                                                if ($notification->data['jobPost']['job_status'] == 0){
                                                    $jobStatus = "Pending";
                                                }elseif($notification->data['jobPost']['job_status'] == 1){
                                                    $jobStatus = "Accepted";
                                                }else{
                                                    $jobStatus = "Declined";
                                                }
                                                ?>

                                                Your job post status for <strong><a href="{{ route('edit.job.post',$notification->data['jobPost']['id'] ) }}"> {{ $notification->data['jobPost']['job_title'] }}</a> </strong> has been changed to <span class="color"> {{ $jobStatus }} </span>
                                            @endif


                                        </span>

                                        @if($notification->type == "App\Notifications\JobApplyNotification")
                                        <!-- Buttons -->
                                        <div class="buttons-to-right">
                                            <a href="{{ route('manage.candidates',$notification->data['jobPost']['id'] ) }}" class="button ripple-effect ico" title="Manage Applicant" data-tippy-placement="left"><i class="icon-feather-check-square"></i></a>
                                        </div>
                                        @endif

                                    </li>
                                    @if($loop->index == 4) @break @endif
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Dashboard Box -->
            <div class="col-xl-6">
                <div class="dashboard-box">
                    <div class="headline">
                        <h3><i class="icon-material-outline-business-center"></i> Recent Job Posts</h3>
                    </div>
                    <div class="content">
                        <ul class="dashboard-box-list">
                            @foreach($recentJobPosts as $jobPost)
                                <li>
                                    <div class="invoice-list-item">
                                        <strong>{{ $jobPost->job_title }}</strong>
                                        <ul>
                                            @if(\Carbon\Carbon::parse($jobPost->job_publish_date) > \Carbon\Carbon::now())
                                                <li><span class="unpaid">Inactive</span></li>
                                            @elseif(\Carbon\Carbon::parse($jobPost->job_expiry_date) <= \Carbon\Carbon::now())
                                                <li><span class="unpaid">Inactive</span></li>
                                            @elseif($jobPost->job_status == 0)
                                                <li><span class="unpaid">Inactive</span></li>
                                            @elseif($jobPost->job_status == 1 && $jobPost->job_active == 1)
                                                <li><span class="paid">Active</span></li>
                                            @elseif($jobPost->job_status == 2)
                                                <li><span class="unpaid">Inactive</span></li>
                                            @endif
                                            <li>{{ $jobPost->category->category_name }}</li>
                                            <li>Publish On: {{ date('d M, Y', strtotime($jobPost->job_publish_date)) }}</li>
                                        </ul>
                                    </div>
                                    <!-- Buttons -->
                                    <div class="buttons-to-right">
                                        <a href="{{ route('edit.job.post', $jobPost->id) }}" class="button gray">Edit Post</a>
                                    </div>
                                </li>
                                @if($loop->index == 3) @break @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- Row / End -->

            <!-- Row -->
            {{--<div class="row">

                <div class="col-xl-12">
                    <!-- Dashboard Box -->
                    <div class="dashboard-box main-box-in-row">
                        <div class="headline">
                            <h3><i class="icon-feather-bar-chart-2"></i> Your Profile Views</h3>
                            <div class="sort-by">
                                <select class="selectpicker hide-tick">
                                    <option>Last 6 Months</option>
                                    <option>This Year</option>
                                    <option>This Month</option>
                                </select>
                            </div>
                        </div>
                        <div class="content">
                            <!-- Chart -->
                            <div class="chart">
                                <canvas id="chart" width="100" height="45"></canvas>
                            </div>
                        </div>
                    </div>
                    <!-- Dashboard Box / End -->
                </div>
            </div>--}}
            <!-- Row / End -->

        <!-- Footer -->
        <div class="dashboard-footer-spacer"></div>
        <div class="small-footer margin-top-15">
            <div class="small-footer-copyrights">
                © 2018 <strong>Hireo</strong>. All Rights Reserved.
            </div>
            <ul class="footer-social-links">
                <li>
                    <a href="#" title="Facebook" data-tippy-placement="top">
                        <i class="icon-brand-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="Twitter" data-tippy-placement="top">
                        <i class="icon-brand-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="Google Plus" data-tippy-placement="top">
                        <i class="icon-brand-google-plus-g"></i>
                    </a>
                </li>
                <li>
                    <a href="#" title="LinkedIn" data-tippy-placement="top">
                        <i class="icon-brand-linkedin-in"></i>
                    </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <!-- Footer / End -->
        @else
                <div class="dashboard-headline">
                    <h3>Hello, {{ $company->name }}</h3>
                    @if($company->verification_status == 0)
                    <span>Please verify your Email to access your dashboard</span>
                    @endif

                    @if($company->status == 0)
                        <span>Please wait for your account to be activated by our admin</span>
                    @endif


                <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{ route('index') }}">Home</a></li>
                            <li>Dashboard</li>
                        </ul>
                    </nav>
                </div>
        @endif

    </div>
</div>

@endsection

@section('scripts')
    <script>
        let myChart = document.getElementById('myChart').getContext('2d');

        // Global Options
        Chart.defaults.global.defaultFontSize = 16;
        Chart.defaults.global.defaultFontColor = '#777';
        Chart.scaleService.updateScaleDefaults('linear',{
           ticks:{
               min:0,
               max: <?php echo $maxData ?>,
               stepSize: 1
           }
        });

        let massPopChart = new Chart(myChart, {


            type:'bar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
            data:{
                labels: <?php echo $jobPosts ?>,
                datasets:[{
                    label:'Applied Candidates',
                    data: <?php echo $jobPostData ?>,
                    //backgroundColor:'green',
                    backgroundColor:[
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                        'rgba(75, 192, 192, 0.6)',
                        'rgba(153, 102, 255, 0.6)',
                        'rgba(255, 159, 64, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgb(40,182,97,0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                        'rgba(75, 192, 192, 0.6)',
                        'rgba(153, 102, 255, 0.6)',
                        'rgba(255, 159, 64, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgb(40,182,97,0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                        'rgba(75, 192, 192, 0.6)',
                        'rgba(153, 102, 255, 0.6)',
                        'rgba(255, 159, 64, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                    ],
                    borderWidth:1,
                    borderColor:'#777',
                    hoverBorderWidth:3,
                    hoverBorderColor:'#000'
                }]
            },
            options:{
                title:{
                    display:true,
                    text:'Number of Candidates for Job Posts',
                    fontSize:25
                },
                legend:{
                    display:false,
                },
                layout:{
                    padding:{
                        left:50,
                        right:50,
                        bottom:20,
                        top:20
                    }
                },
                tooltips:{
                    enabled:true
                },
                scales: {
                    xAxes: [{
                        barPercentage: 0.8,
                        maxBarThickness: 80,
                        ticks: {
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                return  value;
                            },
                            autoSkip: false
                        },
                        scaleLabel: {
                            display: true,
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Number of candidates'

                        }
                    }]
                }
            }
        });
    </script>

@endsection