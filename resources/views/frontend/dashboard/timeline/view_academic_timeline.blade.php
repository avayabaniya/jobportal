@extends('frontend.dashboard_layouts.front_dashboard_design')
@section('content')

    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Academic Timeline</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>View Timeline</li>
                    </ul>
                </nav>
            </div>

            <!-- Dashboard Box -->
            <div class="col-xl-12">
                <div class="dashboard-box margin-top-0">

                    <div class="content with-padding padding-bottom-10" >



                        <h2 style="margin-left: 50px; color: #28b661;">
                            Academic Timeline
                        </h2>
                        <ul class="timeline" style="margin-left: 50px; width: 100% !important; margin-top: 15px;">
                            @foreach($timelineEvents as $event)
                                <li class="timeline-event">
                                <label class="timeline-event-icon"></label>
                                <div class="timeline-event-copy">
                                    <p class="timeline-event-thumbnail">
                                        {{ $event->start_date }} - {{ $event->end_date }}
                                    </p>

                                    <h3>{{ $event->title }} </h3>
                                    <h4>{{ $event->sub_title }}</h4>
                                    <div style="display: inline-block;" id="description{{$event->id}}">
                                        {!! substr($event->description, 0, 200) !!}
                                        @if(strlen($event->description) > 200)
                                            <button id="btn{{ $event->id }}" style="color: #28b661;" href="" rel="{{ $event->id }}" rel2="{{$event->description}}" class="descriptionLink">...Read More</button>@endif
                                    </div>
                                    <div class="sidebar-widget" style="margin-top: 0px;">
                                        <h4 style="margin-bottom: 10px;">Reference Emails</h4>
                                        @if(count($event->referenceEmail) != 0)
                                        <div class="task-tags">
                                            @foreach($event->referenceEmail as $e)
                                                <span>{{ $e->email }}</span>
                                            @endforeach
                                        </div>
                                        @else
                                            <p>
                                                Reference emails not entered
                                            </p>
                                        @endif
                                    </div>

                                    <a href="{{ route('edit.job.seeker.timeline', $event->id) }}" class="button gray ripple-effect ico" title="Edit" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>

                                    <a href="javascript:" rel="{{$event->id}}" rel1="delete-timeline" class="button gray ripple-effect ico deleteRecord" title="Remove" data-tippy-placement="top">
                                        <i class="icon-feather-trash-2"></i>
                                    </a>

                                </div>
                            </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>

            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
            <div class="small-footer margin-top-15">
                <div class="small-footer-copyrights">
                    © 2018 <strong>Hireo</strong>. All Rights Reserved.
                </div>
                <ul class="footer-social-links">
                    <li>
                        <a href="#" title="Facebook" data-tippy-placement="top">
                            <i class="icon-brand-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="Twitter" data-tippy-placement="top">
                            <i class="icon-brand-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="Google Plus" data-tippy-placement="top">
                            <i class="icon-brand-google-plus-g"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="LinkedIn" data-tippy-placement="top">
                            <i class="icon-brand-linkedin-in"></i>
                        </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- Footer / End -->

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(".descriptionLink").click(function () {
            let timeline_id = $(this).attr('rel');
            let c = $(this).attr('rel2');


            //alert("#description"+timeline_id);



            //$("#btn"+timeline_id).css('display', 'none');
            //$("#more"+timeline_id).attr('style', 'display: inline-block !important;');
            $("#description"+timeline_id).empty();
            $("#description"+timeline_id).html(c);
        });
    </script>
@endsection
