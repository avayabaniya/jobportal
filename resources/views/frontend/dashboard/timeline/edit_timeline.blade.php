@extends('frontend.dashboard_layouts.front_dashboard_design')
@section('content')

    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Carrer Timeline</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>Edit Timeline</li>
                    </ul>
                </nav>
            </div>

            <form id="addTimeLineForm" action="{{ route('edit.job.seeker.timeline', $timeline->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-feather-folder-plus"></i>Edit Timeline</h3>
                            </div>


                            <div class="content with-padding padding-bottom-10">
                                <div class="row">

                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Company/Institution Name</h5>
                                            <input type="text" name="title" class="with-border" id="title" value="{{ $timeline->title }}">
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Your Position</h5>
                                            <input type="text" name="sub_title" class="with-border" id="sub_title" value="{{ $timeline->sub_title }}">
                                        </div>
                                    </div>


                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Start Date</h5>
                                            <input id="start_date" autocomplete="off"  type="text" name="start_date" class="with-border" value="{{ $timeline->start_date }}">
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>End Date</h5>
                                            <input id="end_date" autocomplete="off"  type="text" name="end_date" class="with-border" value="{{ $timeline->end_date }}">
                                        </div>
                                    </div>

                                    <div class="col-xl-6">
                                        <div class="submit-field">
                                            <h5>Reference Emails <span>(optional)</span>  <i class="help-icon" data-tippy-placement="right"></i></h5>
                                            <div class="keywords-container">
                                                <div class="keyword-input-container">
                                                    <input name="email[]" type="text" data-role="tagsinput" class="keyword-input with-border" placeholder="Add Emails">
                                                    <button class="keyword-input-button ripple-effect tags_button"><i class="icon-material-outline-add"></i></button>

                                                </div>
                                                <div class="keywords-list"><!-- keywords go here -->

                                                    @foreach($timeline->referenceEmail as $email)
                                                        <span class="keyword"><span class="keyword-remove"></span><span class="keyword-text">{{ $email->email }}</span></span>
                                                    @endforeach

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-xl-6"></div>


                                    <div class="col-xl-6 col-md-6 margin-bottom-25">
                                        <div class="section-headline margin-bottom-12">
                                            <h5>Timeline Type</h5>
                                        </div>

                                        <div class="radio" style="margin-right: 20px">
                                            <input name="timeline_type" id="radio-1" type="radio" value="0" @if($timeline->timeline_type == 0) checked @endif>
                                            <label for="radio-1"><span class="radio-label"></span> Carrer</label>
                                        </div>

                                        <div class="radio">
                                            <input name="timeline_type" id="radio-2" value="1" type="radio" @if($timeline->timeline_type == 1) checked @endif>
                                            <label for="radio-2"><span class="radio-label"></span> Academics</label>
                                        </div>
                                    </div>


                                    <div class="col-xl-12">
                                        <div class="submit-field">
                                            <h5>Job Description</h5>
                                            <textarea id="description" name="description" rows="10" class="with-border">{!! trim($timeline->description) !!}  </textarea>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-xl-12">
                        <button id="saveChanges" type="submit" class="button ripple-effect big margin-top-30"><i class="icon-feather-plus"></i> Update Timeline Event</button>
                    </div>

                </div>
                <!-- Row / End -->
            </form>



            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>
            <div class="small-footer margin-top-15">
                <div class="small-footer-copyrights">
                    © 2018 <strong>Hireo</strong>. All Rights Reserved.
                </div>
                <ul class="footer-social-links">
                    <li>
                        <a href="#" title="Facebook" data-tippy-placement="top">
                            <i class="icon-brand-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="Twitter" data-tippy-placement="top">
                            <i class="icon-brand-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="Google Plus" data-tippy-placement="top">
                            <i class="icon-brand-google-plus-g"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="LinkedIn" data-tippy-placement="top">
                            <i class="icon-brand-linkedin-in"></i>
                        </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- Footer / End -->

        </div>
    </div>

@endsection

@section('scripts')
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=05fgyydvhrzu7w93s0m0hfu5plg2dthhn2v9tjqkyeb8fpkb"></script>
    <script>
        tinymce.init({
            selector: '#description'
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#tags_button').click(function(e) {

                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();

                return false;
            });});
    </script>

    <script>
        $(document).ready(function () {

            $("#addTimeLineForm").validate({
                rules: {
                    title:{
                        required:true
                    },
                    sub_title: {
                        required:true
                    },
                    start_date: {
                        required:true
                    },
                    end_date: {
                        required:true
                    }
                },
                messages:{
                    title: {
                        required: "Please Enter The Company/Institution Name"
                    },
                    sub_title:{
                        required: "Please Enter Your Position"
                    },
                    start_date: {
                        required: "Please Enter Your Start Date"
                    },
                    end_date: {
                        required: "Please Enter Your End Date"
                    }
                }

            });

        });
    </script>

    <script>
        $("#start_date").datepicker({
            autoclose: true,
            endDate:new Date(),
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years"
        });

    </script>

    <script>
        $("#start_date").change(function () {
            var start_year = $(this).val();
            var end_date = $("#end_date").val();

            if (parseFloat(start_year) > parseFloat(end_date))
            {
                $("#end_date").val('');
            }

            $("#end_date").removeAttr('readonly');
            $("#end_date").datepicker('destroy');
            $("#end_date").datepicker({
                autoclose: true,
                startDate:new Date(start_year),
                endDate:new Date(),
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
        });

        $("#end_date").keyup(function () {
            $(this).val('');
        });
    </script>

    <script>
        $(document).ready(function () {
            $('.tags_button').click(function(e) {

                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();

                return false;
            });});
    </script>

    <script>
        $("#saveChanges").click(function () {

            //let a = $(".keyword-text").html();
            $(".keyword-text").each(function() {
                //alert($(this).text());
                $(".keywords-list").append('<input type="hidden" name="email[]" value="'+$(this).text()+'">');
            });
            //alert(a);
        });

    </script>
@endsection
