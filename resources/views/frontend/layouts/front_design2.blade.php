<!doctype html>
<html lang="en">
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>Hireo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('public/frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('public/frontend/css/colors/green.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    @yield('styles')

</head>
<body>

<!-- Wrapper -->
<div id="wrapper">

    @include('frontend.layouts.front_header2')

    @yield('content')

    @include('frontend.layouts.front_footer2')

</div>
<!-- Wrapper / End -->

<!-- Scripts
================================================== -->
<script src="{{ asset('public/frontend/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/jquery-migrate-3.0.0.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/mmenu.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/tippy.all.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/simplebar.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/bootstrap-slider.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/snackbar.js') }}"></script>
<script src="{{ asset('public/frontend/js/clipboard.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/counterup.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/magnific-popup.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/slick.min.js') }}"></script>
<script src="{{ asset('public/frontend/js/custom.js') }}"></script>

<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function() {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"> </script>
<script >
    @if(Session::has('success'))

        toastr.options.positionClass = 'toast-top-right';
    toastr.success('{{ Session::get('success') }}');

    @endif
            @if(Session::has('error'))

        toastr.options.positionClass = 'toast-top-right';
    toastr.error('{{ Session::get('error') }}');

    @endif
</script>

{{--bookmark--}}
<script>
    $(document).ready(function () {
        $('.bookmark-job').click(function () {
            var job_id = $(this).attr('rel');
            var APP_URL = {!! json_encode(url('/')) !!}
            console.log(APP_URL);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'post',
                url: APP_URL + '/job-bookmark',
                data:{
                    job_id: job_id
                },
                success:function (resp) {
                    console.log(resp);
                }, error:function (resp) {
                    console.log(resp);
                }
            });
        }) ;
    });
</script>

<script>
    $(document).ready(function () {
        let company_id = {{ empty(Auth::guard('company')->user()->id) ? 0 : Auth::guard('company')->user()->id }};

        let APP_URL = {!! json_encode(url('/')) !!};
        console.log(APP_URL);
        if (company_id !== 0)
        {
            setInterval(function () {

                let notification_count = $("#company_notification_count").text();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type:'post',
                    url: APP_URL + '/apply-job/notification',
                    data:{
                        company_id: company_id,
                        notification_count: notification_count
                    },
                    success:function (resp) {

                        if (resp != -1)
                        {
                            console.log(resp);
                            //let a = $('#updated_count').text();
                            let a = resp[0];

                            $("#company_notification_count").text(a);

                            $('.fade-out-notification').fadeOut(100, function(){

                                $('.fade-in-notification').html(resp[1]).fadeIn();
                                //return;
                            });

                        }

                    }, error:function (resp) {
                        //alert('error')

                    }
                });

            }, 10000);
        }

    });
</script>





@yield('scripts')


</body>
</html>