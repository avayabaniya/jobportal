@extends('frontend.layouts.front_design2')
@section('content')

    <!-- Titlebar
================================================== -->
    <div class="single-page-header freelancer-header" data-background-image="images/single-freelancer.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-page-header-inner">
                        <div class="left-side">
                            <div class="header-image freelancer-avatar"><img src="{{asset('public/job_seeker/images/profile/'.$jobSeeker->profile_image)}}" alt=""></div>
                            <div class="header-details">
                                <h3>{{ $jobSeeker->name }} <span>{{ $jobSeeker->tagline }}</span></h3>
                                <ul>
                                    {{--<li><div class="star-rating" data-rating="5.0"></div></li>--}}
                                    @if(!empty($jobSeeker->address))
                                    <li><b>Address:</b> {{ $jobSeeker->address }}</li>
                                    @endif
                                    @if(!empty($jobSeeker->contact_number))
                                        <li><b>Contact Number:</b> {{ $jobSeeker->contact_number }}</li>
                                    @endif

                                    @if(!empty($jobSeeker->date_of_birth))
                                        <li><b>Date of Birth:</b> {{ $jobSeeker->date_of_birth }}</li>
                                    @endif

                                    @if($jobSeeker->verification_status == 1)
                                    <li><div class="verified-badge-with-title">Verified</div></li>
                                    @endif


                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Page Content
    ================================================== -->
    <div class="container">
        <div class="row">

            <!-- Content -->
            <div class="col-xl-8 col-lg-8 content-right-offset">

                <!-- Page Content -->
                <div class="single-page-section">
                    <h3 class="margin-bottom-25">Description</h3>
                    {!! $jobSeeker->description !!}
                    @if(empty($jobSeeker->description))
                        <p>Description not available. </p>
                    @endif
                </div>

                <!-- Boxed List -->
                <div class="boxed-list margin-bottom-60">
                    <div class="boxed-list-headline">
                        <h3><i class="icon-material-outline-business"></i> Career Timeline</h3>
                    </div>
                    <ul class="boxed-list-ul">
                        @foreach($jobSeeker->timeline as $event)
                            @if($event->timeline_type == 0)
                                <li>
                            <div class="boxed-list-item">
                                <!-- Content -->
                                <div class="item-content">
                                    <h4>{{ $event->sub_title }}</h4>
                                    <div class="item-details margin-top-7">
                                        <div class="detail-item"><a href="#"><i class="icon-material-outline-business"></i>{{ $event->title }}</a></div>
                                        <div class="detail-item"><i class="icon-material-outline-date-range"></i> {{ $event->start_date }} - @if(!empty($event->start_date)){{ $event->end_date }} @else Present @endif </div>
                                        @if(!empty($event->referenceEmail))
                                            <div class="detail-item">

                                                @foreach($event->referenceEmail as $email)
                                                    <i class="icon-material-baseline-mail-outline"></i>
                                                    {{ $email->email }}
                                                @endforeach

                                            </div>
                                        @endif

                                    </div>
                                    <div class="item-description">
                                        <div style="display: inline-block;" id="description{{$event->id}}">
                                            {!! substr($event->description, 0, 200) !!}
                                            @if(strlen($event->description) > 200)
                                                <button id="btn{{ $event->id }}" style="color: #28b661;" href="" rel="{{ $event->id }}" rel2="{{$event->description}}" class="descriptionLink">...Read More</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <!-- Boxed List / End -->

                <!-- Boxed List -->
                <div class="boxed-list margin-bottom-60">
                    <div class="boxed-list-headline">
                        <h3><i class="icon-material-outline-business"></i> Academic Timeline</h3>
                    </div>
                    <ul class="boxed-list-ul">
                        @foreach($jobSeeker->timeline as $event)
                            @if($event->timeline_type == 1)
                                <li>
                                    <div class="boxed-list-item">
                                        <!-- Content -->
                                        <div class="item-content">
                                            <h4>{{ $event->sub_title }}</h4>
                                            <div class="item-details margin-top-7">
                                                <div class="detail-item"><a href="#"><i class="icon-material-outline-business"></i>{{ $event->title }}</a></div>
                                                <div class="detail-item">
                                                    <i class="icon-material-outline-date-range"></i>
                                                    {{ $event->start_date }} - @if(!empty($event->start_date)){{ $event->end_date }} @else Present @endif
                                                </div>

                                                @if(!empty($event->referenceEmail))
                                                <div class="detail-item">

                                                    @foreach($event->referenceEmail as $email)
                                                    <i class="icon-material-baseline-mail-outline"></i>
                                                        {{ $email->email }}
                                                    @endforeach

                                                </div>
                                                @endif
                                            </div>
                                            <div class="item-description">
                                                <div style="display: inline-block;" id="description{{$event->id}}">
                                                    {!! substr($event->description, 0, 200) !!}
                                                    @if(strlen($event->description) > 200)
                                                        <button id="btn{{ $event->id }}" style="color: #28b661;" href="" rel="{{ $event->id }}" rel2="{{$event->description}}" class="descriptionLink">...Read More</button>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <!-- Boxed List / End -->



                <!-- Boxed List Achievement -->
                <div class="boxed-list margin-bottom-60">
                    <div class="boxed-list-headline">
                        <h3><i class="icon-material-outline-business"></i> Achievements</h3>
                    </div>
                    <ul class="boxed-list-ul">
                        @foreach($jobSeeker->achievement as $event)

                                <li>
                                    <div class="boxed-list-item">
                                        <!-- Content -->
                                        <div class="item-content">
                                            <h4>{{ $event->achievement }}</h4>
                                            <div class="item-details margin-top-7">
                                                <div class="detail-item"><i class="icon-material-outline-date-range"></i>{{ $event->date }}</div>
                                        </div>
                                    </div>
                                </li>

                        @endforeach
                    </ul>
                </div>
                <!-- Boxed List / End -->

            </div>


            <!-- Sidebar -->
            <div class="col-xl-4 col-lg-4">
                <div class="sidebar-container">

                   {{-- <!-- Profile Overview -->
                    <div class="profile-overview">
                        <div class="overview-item"><strong>$35</strong><span>Hourly Rate</span></div>
                        <div class="overview-item"><strong>53</strong><span>Jobs Done</span></div>
                        <div class="overview-item"><strong>22</strong><span>Rehired</span></div>
                    </div>

                    <!-- Button -->
                    <a href="#small-dialog" class="apply-now-button popup-with-zoom-anim margin-bottom-50">Make an Offer <i class="icon-material-outline-arrow-right-alt"></i></a>
--}}


                   <!-- Freelancer Indicators -->
                    {{--<div class="sidebar-widget">
                        <div class="freelancer-indicators">

                            <!-- Indicator -->
                            <div class="indicator">
                                <strong>88%</strong>
                                <div class="indicator-bar" data-indicator-percentage="88"><span></span></div>
                                <span>Job Success</span>
                            </div>

                            <!-- Indicator -->
                            <div class="indicator">
                                <strong>100%</strong>
                                <div class="indicator-bar" data-indicator-percentage="100"><span></span></div>
                                <span>Recommendation</span>
                            </div>

                            <!-- Indicator -->
                            <div class="indicator">
                                <strong>90%</strong>
                                <div class="indicator-bar" data-indicator-percentage="90"><span></span></div>
                                <span>On Time</span>
                            </div>

                            <!-- Indicator -->
                            <div class="indicator">
                                <strong>80%</strong>
                                <div class="indicator-bar" data-indicator-percentage="80"><span></span></div>
                                <span>On Budget</span>
                            </div>
                        </div>
                    </div>--}}

                    <!-- Widget -->
                    <div class="sidebar-widget">
                        <h3>Social Profiles</h3>
                        <div class="freelancer-socials margin-top-25">
                            <ul>
                                @if(!empty($jobSeeker->twitter))
                                    <li><a href="{{ $jobSeeker->twitter }}" target="_blank" title="Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                                @endif

                                @if(!empty($jobSeeker->github))
                                    <li><a href="{{ $jobSeeker->github }}" target="_blank" title="GitHub" data-tippy-placement="top"><i class="icon-brand-github"></i></a></li>
                                @endif

                                @if(!empty($jobSeeker->linkedin))
                                    <li><a href="{{ $jobSeeker->linkedin }}" target="_blank" title="linkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin"></i></a></li>
                                @endif

                                @if(!empty($jobSeeker->facebook))
                                    <li><a href="{{ $jobSeeker->facebook }}" target="_blank" title="facebook" data-tippy-placement="top"><i class="icon-brand-facebook"></i></a></li>
                                @endif
                                @if(!empty($jobSeeker->personal_site))
                                    <li><a href="{{ $jobSeeker->facebook }}" target="_blank" title="Personal Site" data-tippy-placement="top"><i class="icon-material-outline-account-circle"></i></a></li>
                                @endif



                            </ul>
                        </div>
                    </div>

                    <!-- Widget -->
                    <div class="sidebar-widget">
                        <h3>Skills</h3>
                        <div class="task-tags">
                            @foreach($jobSeeker->skill as $skill)
                                <span>{{ $skill->skill }}</span>
                            @endforeach
                        </div>
                    </div>

                    <!-- Widget -->
                    <div class="sidebar-widget">
                        <h3>Attachments</h3>
                        @if(!empty($jobSeeker->cv))
                            <div class="attachments-container">
                                <a href="{{ asset('public/job_seeker/files/'.$jobSeeker->cv) }}" target="_blank" class="attachment-box ripple-effect"><span>{{ $jobSeeker->name }}'s CV</span><i>PDF</i></a>
                            </div>
                        @else
                            <p>Attachments not available </p>
                        @endif
                    </div>

                    <!-- Sidebar Widget -->
                    <div class="sidebar-widget">
                        <h3>Share</h3>

                        <!-- Copy URL -->
                        <div class="copy-url">
                            <input id="copy-url" type="text" value="" class="with-border">
                            <button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
                        </div>

                        <!-- Share Buttons -->
                        <div class="share-buttons margin-top-25">
                            <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                            <div class="share-buttons-content">
                                <span>Interesting? <strong>Share It!</strong></span>
                                <ul class="share-buttons-icons">
                                    <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
                                    <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                                    <li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus" data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
                                    <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


@endsection

@section('scripts')
    <script>
        $(".descriptionLink").click(function () {
            let timeline_id = $(this).attr('rel');
            let c = $(this).attr('rel2');


            //alert("#description"+timeline_id);



            //$("#btn"+timeline_id).css('display', 'none');
            //$("#more"+timeline_id).attr('style', 'display: inline-block !important;');
            $("#description"+timeline_id).empty();
            $("#description"+timeline_id).html(c);
        });
    </script>
@endsection
