<!DOCTYPE html>
<html lang="en">
<head>
    <title>JobPortal - Admin Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{asset('public/admin/login/images/icons/favicon.ico')}}"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/login/vendor/bootstrap/css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/login/vendor/animate/animate.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/login/vendor/css-hamburgers/hamburgers.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/login/vendor/select2/select2.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/login/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/login/css/main.css')}}">
    <!--===============================================================================================-->

    <style>
        #email{
            border: 1px solid rgba(0,0,0,.15) !important;
        }
    </style>
</head>


<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100" style="padding-top: 50px;">
            <div class="login100-pic js-tilt" data-tilt>
                <img src="{{asset('public/admin/login/images/img-01.png')}}" alt="IMG">
            </div>

            <form method="POST" action="{{ route('password.email') }}" class="login100-form validate-form">
                @csrf
                <span class="login100-form-title">
						Reset Password
					</span>
                <div class="form-group row">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <label for="email" class="col-md-12 col-form-label text-center">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-12">
                        <input   id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-12 offset-md-4">
                        <button type="submit" class="login100-form-btn">
                            {{ __('Send Password Reset Link') }}
                        </button>
                    </div>
                </div>
            </form>


            <div class="text-center p-t-136">

            </div>
        </div>
    </div>
</div>




<!--===============================================================================================-->
<script src="{{asset('public/admin/login/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('public/admin/login/vendor/bootstrap/js/popper.js')}}"></script>
<script src="{{asset('public/admin/login/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('public/admin/login/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
<script src="{{asset('public/admin/login/vendor/tilt/tilt.jquery.min.js')}}"></script>
<script >
    $('.js-tilt').tilt({
        scale: 1.1
    })
</script>
<!--===============================================================================================-->
<script src="{{asset('public/admin/login/js/main.js')}}"></script>

</body>
</html>