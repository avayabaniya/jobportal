@extends('admin.layouts.admin_design')

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">

                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h3 class="info">{{ $activeJobPostCount }}</h3>
                                            <h6>Total Active Job Posts</h6>
                                        </div>
                                        <div>
                                            <i class="icon-briefcase info font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                    <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                        <div class="progress-bar bg-gradient-x-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h3 class="warning">{{ $expiredJobPostCount }}</h3>
                                            <h6>Total Inactive Job Posts</h6>
                                        </div>
                                        <div>
                                            <i class="icon-briefcase warning font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                    <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                        <div class="progress-bar bg-gradient-x-warning" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h3 class="success">{{ $jobSeekerCount }}</h3>
                                            <h6>Total Job Seekers</h6>
                                        </div>
                                        <div>
                                            <i class="icon-user-following success font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                    <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                        <div class="progress-bar bg-gradient-x-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-12">
                        <div class="card pull-up">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="media d-flex">
                                        <div class="media-body text-left">
                                            <h3 class="danger">{{ $companyCount }}</h3>
                                            <h6>Total Company</h6>
                                        </div>
                                        <div>
                                            <i class="icon-home danger font-large-2 float-right"></i>
                                        </div>
                                    </div>
                                    <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                                        <div class="progress-bar bg-gradient-x-danger" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12" id="recent-transactions">
                        <div class="card">
                            <div class="card-header">
                                <h1 class="card-title">Recent Job Post Request</h1>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a class="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="{{ route('view.job.post.request') }}">View all Job Post Request</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="table-responsive">
                                    <table class="table table-hover table-xl mb-0" id="recent-orders">
                                        <thead>
                                        <tr>
                                            <th class="border-top-0">Status</th>
                                            <th class="border-top-0">Job Title</th>
                                            <th class="border-top-0">Company</th>
                                            <th class="border-top-0">Publish Date</th>
                                            <th class="border-top-0">Expiry Date</th>
                                            <th class="border-top-0">Vacancy</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($RecentJobPosts as $job)
                                            <tr>
                                            <td class="text-truncate">
                                                @if(\Carbon\Carbon::parse($job->job_publish_date) > \Carbon\Carbon::now())
                                                    <i class="la la-dot-circle-o warning font-medium-1 mr-1"></i>
                                                    Pending
                                                @elseif(\Carbon\Carbon::parse($job->job_expiry_date) <= \Carbon\Carbon::now())
                                                    <i class="la la-dot-circle-o danger font-medium-1 mr-1"></i>
                                                    Expired
                                                @elseif($job->job_status == 0)
                                                    <i class="la la-dot-circle-o warning font-medium-1 mr-1"></i>
                                                    Pending
                                                @elseif($job->job_status == 1 && $job->job_active == 1)
                                                    <i class="la la-dot-circle-o success font-medium-1 mr-1"></i>
                                                    Active
                                                @elseif($job->job_status == 2)
                                                    <i class="la la-dot-circle-o danger font-medium-1 mr-1"></i>
                                                    Declined
                                                @endif
                                            </td>
                                            <td class="text-truncate">
                                                <span>{{ $job->job_title }}</span>
                                            </td>
                                            <td class="text-truncate p-1">
                                                <ul class="list-unstyled users-list m-0">
                                                    <li class="avatar avatar-sm pull-up" data-toggle="tooltip" data-original-title="{{ $job->company->name }}" data-popup="tooltip-custom">
                                                        <img class="media-object rounded-circle no-border-top-radius no-border-bottom-radius" alt="Avatar" src="{{asset('public/company/images/profile/'.$job->company->profile_image)}}">
                                                        <span>{{ $job->company->name }}</span>
                                                    </li>
                                                </ul>
                                            </td>
                                            <td style="margin-left: 100px;">
                                                <button class="btn btn-sm btn-outline-success round" type="button">{{ date('d M, Y', strtotime($job->job_publish_date)) }}</button>
                                            </td>
                                            <td>
                                                <button class="btn btn-sm btn-outline-danger round" type="button">{{ date('d M, Y', strtotime($job->job_expiry_date)) }}</button>
                                            </td>
                                            <td class="text-truncate">{{ $job->job_vacancy_number }}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-12">
                        <div class="dashboard-box">
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts')


        <script>
            let myChart = document.getElementById('myChart').getContext('2d');

            // Global Options
            Chart.defaults.global.defaultFontSize = 16;
            Chart.defaults.global.defaultFontColor = '#777';
            Chart.scaleService.updateScaleDefaults('linear',{
                ticks:{
                    min:0,
                    max: <?php echo $maxData ?>,
                    stepSize: 10
                }
            });

            let massPopChart = new Chart(myChart, {


                type:'bar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
                data:{
                    labels: <?php echo $company ?>,
                    datasets:[{
                        label:'Job Posts',
                        data: <?php echo $companyPostCount ?>,
                        //backgroundColor:'green',
                        backgroundColor:[
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(255, 206, 86, 0.6)',
                            'rgba(75, 192, 192, 0.6)',
                            'rgba(153, 102, 255, 0.6)',
                            'rgba(255, 159, 64, 0.6)',
                            'rgba(255, 99, 132, 0.6)',
                            'rgb(40,182,97,0.6)',
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(255, 206, 86, 0.6)',
                            'rgba(75, 192, 192, 0.6)',
                            'rgba(153, 102, 255, 0.6)',
                            'rgba(255, 159, 64, 0.6)',
                            'rgba(255, 99, 132, 0.6)',
                            'rgb(40,182,97,0.6)',
                            'rgba(255, 99, 132, 0.6)',
                            'rgba(54, 162, 235, 0.6)',
                            'rgba(255, 206, 86, 0.6)',
                            'rgba(75, 192, 192, 0.6)',
                            'rgba(153, 102, 255, 0.6)',
                            'rgba(255, 159, 64, 0.6)',
                            'rgba(255, 99, 132, 0.6)',
                        ],
                        borderWidth:1,
                        borderColor:'#777',
                        hoverBorderWidth:3,
                        hoverBorderColor:'#000'
                    }]
                },
                options:{
                    title:{
                        display:true,
                        text:'Number of Job Posts by Companies',
                        fontSize:25
                    },
                    legend:{
                        display:false,
                    },
                    layout:{
                        padding:{
                            left:50,
                            right:50,
                            bottom:20,
                            top:20
                        }
                    },
                    tooltips:{
                        enabled:true
                    },
                    scales: {
                        xAxes: [{
                            barPercentage: 0.8,
                            maxBarThickness: 80,
                            ticks: {
                                // Include a dollar sign in the ticks
                                callback: function(value, index, values) {
                                    return  value;
                                },
                                autoSkip: false
                            },
                            scaleLabel: {
                                display: true,
                            }
                        }],
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'Number of job posts'

                            }
                        }]
                    }
                }
            });
    </script>

@endsection