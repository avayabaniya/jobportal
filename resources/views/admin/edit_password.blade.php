@extends('admin.layouts.admin_design')

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Update Password</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Profile</a>
                                </li>
                                <li class="breadcrumb-item">Update Password
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="horizontal-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="horz-layout-basic">Update Password</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collpase show">
                                    <div class="card-body">
                                        <form id="update_password" class="form form-horizontal" method="post" action="{{ route('edit.password') }}" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-user"></i> Change Password</h4>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput1">Current Password</label>
                                                    <div class="col-md-9">
                                                        <input type="password" id="current_password" class="form-control" placeholder="Current Password" name="current_password">
                                                       <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('current_password') }}</p>
                                                       <b><p id="correct_password" style="color: green;"></p></b>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput1">New Password</label>
                                                    <div class="col-md-9">
                                                        <input type="password" id="new_password" class="form-control" placeholder="New Password" name="new_password">
                                                        <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('new_password') }}</p>
                                                    </div>
                                                </div>


                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput1">Confirm Password</label>
                                                    <div class="col-md-9">
                                                        <input type="password" id="confirm_password" class="form-control" placeholder="Confirm Password"
                                                               name="confirm_password">
                                                        <p style="color: red; margin-bottom: 0px;">{{ $errors -> first('confirm_password') }}</p>

                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Update Password
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#update_password').validate({
                rules: {
                    current_password:{
                        required: true,
                        minlength:6,
                        maxlength:20
                    },
                    new_password:{
                        required: true,
                        minlength:6,
                        maxlength:20
                    },
                    confirm_password:{
                        required: true,
                        minlength:6,
                        maxlength:20,
                        equalTo:"#new_password"
                    }
                },
                messages: {
                    current_password: {
                        required: "Your Current Password is required"
                    },
                    new_password: {
                        required: "Please enter your new password"
                    },
                    confirm_password: {
                        required: "Please confirm your new password",
                        equalTo: "This password should match your new password"
                    }
                }
            });
        });
    </script>

    {{--Check current password--}}
    <script>
        $("#current_password").keyup(function(){
            var current_password = $("#current_password").val();
            // alert(current_password);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'post',
                url:'check-password',
                data:{current_password:current_password},
                success:function(resp){

                    if(resp=="false"){

                        $("#correct_password").text("Entered value does not match the current password").css("color","red");

                    }else if(resp=="true"){

                        $("#correct_password").text("Current password matched").css("color","green");
                    }
                },error:function(resp){

                }
            });
        });
    </script>

@endsection

@section('styles')
    <style>
        .error{
            color: red;
        }
    </style>
@endsection

