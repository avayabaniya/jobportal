@extends('admin.layouts.admin_design')

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Add Job Category</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item">Job Category
                                </li>
                                <li class="breadcrumb-item">Add Job Category
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="horizontal-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="horz-layout-basic">Add Job Category</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collpase show">
                                    <div class="card-body">
                                        <form class="form form-horizontal" method="post" action="{{route('add.job.category')}}" enctype="multipart/form-data" id="addJobCategory">
                                            @csrf
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-grid"></i>Add Job Category</h4>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="category_name">Category Name</label>
                                                    <div class="col-md-9">
                                                        <input type="text" id="category_name" class="form-control" placeholder="Job Category Name"
                                                               name="category_name">
                                                        <a href="" style="color: red;">{{ $errors -> first('category_name') }}</a>

                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="description">Description</label>
                                                    <div class="col-md-9">
                                                        <textarea id="description" rows="10" class="form-control" name="category_description"></textarea>
                                                        <a href="" style="color: red;">{{ $errors -> first('category_description') }}</a>
                                                    </div>

                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control">Category Image</label>
                                                    <div class="col-md-9">
                                                        <label id="image" class="file center-block">
                                                            <input type="file" onchange="return fileValidation()" id="file" name="image">
                                                            <span class="file-custom"></span>
                                                        </label><br>
                                                        <a href="" style="color: red;">{{ $errors -> first('image') }}</a>
                                                    </div>

                                                </div>


                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="category_name">Active</label>
                                                    <div class="col-md-9">
                                                        <input type="checkbox" class="switch" id="status" name="status" checked/>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-actions right">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Add Job Category
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('public/admin/assets/app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/admin/assets/app-assets/js/scripts/forms/select/form-select2.js')}}" type="text/javascript"></script>



    {{--Switch JS--}}
    <script src="{{asset('public/admin/assets/app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/admin/assets/app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/admin/assets/app-assets/vendors/js/forms/toggle/switchery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/admin/assets/app-assets/js/scripts/forms/switch.js')}}" type="text/javascript"></script>


    <script>
        $(document).ready(function () {
            $('#addProduct').validate({
                rules: {
                    product_name:{
                        required: true,
                    },
                    parent_id: {
                        required: true
                    },
                    product_code: {
                        required: true
                    },
                    price: {
                        required: true
                    },
                    image: {
                        required: true,
                    }



                },
                messages: {
                    product_name: {
                        required: "<span class='text-danger'> Please Enter Product Name </span>"
                    },
                    parent_id:{
                        required: "<span class='text-danger'> Please Select Category </span>"
                    },
                    product_code:{
                        required: "<span class='text-danger'> Please Enter Product Code </span>"
                    },
                    price:{
                        required: "<span class='text-danger'> Please Enter Product Code </span>"
                    },
                    image:{
                        required: "<span class='text-danger'> Please Select Image </span>"
                    }

                }
            });
        });
    </script>

    <script type="text/javascript">
        function fileValidation(){
            var fileInput = document.getElementById('file');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
            if(!allowedExtensions.exec(filePath)){
                alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
                fileInput.value = '';
                return false;
            }
            else{
                //Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
                    };
                    reader.readAsDataURL(fileInput.files[0]);
                }
            }
        }

    </script>

    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#description').summernote();
            $('#meta_description').summernote();

        });
    </script>
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/assets/app-assets/css/plugins/forms/switch.css')}}">

@endsection