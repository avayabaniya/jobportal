@extends('admin.layouts.admin_design')

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Admin Profile</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Profile</a>
                                </li>
                                <li class="breadcrumb-item active">Edit Profile
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <div id="user-profile">
                    <div class="row">
                        <div class="col-12">
                            <div class="card profile-with-cover">
                                <div class="card-img-top img-fluid bg-cover height-300" style="background: url({{asset($user->cover_image)}}) 50%;"></div>
                                <div class="media profil-cover-details w-100">
                                    <div class="media-left pl-2 pt-2">
                                        <a href="#" class="profile-image">
                                            <img src="{{asset($user->image)}}" class="rounded-circle img-border height-100"
                                                 alt="Card image" style="margin-bottom: 10px;">
                                        </a>
                                    </div>
                                    <div class="media-body pt-3 px-2">
                                        <div class="row">
                                            <div class="col">
                                                <h3 class="card-title">{{$user->name}}</h3>
                                                <h4>{{$user->email}}</h4>
                                            </div>
                                            <nav class="navbar navbar-light navbar-profile align-self-end">
                                                <button class="navbar-toggler d-sm-none" type="button" data-toggle="collapse" aria-expanded="false"
                                                        aria-label="Toggle navigation"></button>
                                                <nav class="navbar navbar-expand-lg">
                                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                                        <ul class="navbar-nav mr-auto">
                                                            <li class="nav-item active">
                                                                <a class="nav-link" href="{{route('edit.password')}}"><i class="la la-line-chart"></i> Update Password <span class="sr-only">(current)</span></a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link" href="{{route('edit.profile', $user->id)}}"><i class="la la-user"></i> Edit Profile</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                        </nav>
                                            </nav>
                                        </div>
                                        </div>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection