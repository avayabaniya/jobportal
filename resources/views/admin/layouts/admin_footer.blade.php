<!-- ////////////////////////////////////////////////////////////////////////////-->
<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; <?php echo date('Y'); ?> <a class="text-bold-800 grey darken-2" href="https://onlinezeal.com/"
                                                                                     target="_blank">Online Job Portal</a>, All rights reserved. </span>
        <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span>
    </p>
</footer>
<!-- BEGIN VENDOR JS-->
<script src="{{asset('public/admin/assets/app-assets/vendors/js/vendors.min.js')}}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{asset('public/admin/assets/app-assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/admin/assets/app-assets/vendors/js/charts/raphael-min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/admin/assets/app-assets/vendors/js/charts/morris.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/admin/assets/app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js')}}"
        type="text/javascript"></script>
<script src="{{asset('public/admin/assets/app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js')}}"
        type="text/javascript"></script>
<script src="{{asset('public/admin/assets/app-assets/data/jvector/visitor-data.js')}}" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN MODERN JS-->
<script src="{{asset('public/admin/assets/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
<script src="{{asset('public/admin/assets/app-assets/js/core/app.js')}}" type="text/javascript"></script>
<script src="{{asset('public/admin/assets/app-assets/js/scripts/customizer.js')}}" type="text/javascript"></script>
<!-- END MODERN JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="{{asset('public/admin/assets/app-assets/js/scripts/pages/dashboard-sales.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>


<script src="{{asset('public/admin/assets/toastr.js')}}"></script>

<script>
    @if(Session::has('success'))
    toastr.success('{{Session::get('success')}}')
    @endif

    @if(Session::has('info'))
    toastr.info('{{Session::get('info')}}')
    @endif

    @if(Session::has('error'))
    toastr.error('{{Session::get('danger')}}')
    @endif
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
    $(document).ready(function () {
        $(".deleteRecord").click(function(){
            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are You Sure",
                    text: "You will not be able to recover this record again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Yes, Delete it!",
                    confirmButtonColor: "red"
                },
                function(){
                    window.location.href="/coursework/admin/"+deleteFunction+"/"+id;
                }
            );
        });
    });
</script>

<script>
    $("#start_date").datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd M, yyyy'
    });

</script>

<script>
    $("#start_date").change(function () {
        var start_year = $(this).val();

        $("#end_date").removeAttr('readonly');
        $("#end_date").datepicker('destroy');
        $("#end_date").datepicker({
            autoclose: true,
            todayHighlight: true,
            startDate:new Date(start_year),
            format: 'dd M, yyyy'
        });
    });

    $("#end_date").keyup(function () {
        $(this).val('');
    });
</script>

@yield('scripts')
<!-- END PAGE LEVEL JS-->