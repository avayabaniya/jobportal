<?php
use App\Http\Controllers\Controller;
$pendingJobsCount = Controller::pendingJobPostsCount();
$pendingCompanyCount = Controller::pendingCompanyCount();
?>
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class=" nav-item">
                <a href="{{route('admin.dashboard')}}"><i class="la la-home"></i>
                    <span class="menu-title" data-i18n="nav.support_documentation.main">Dashboard</span>
                </a>
            </li>

            <li class=" nav-item"><a href=""><i class="la la-object-group"></i><span class="menu-title" data-i18n="nav.dash.main">Job Types</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{ route('add.job.type') }}" data-i18n="nav.dash.ecommerce">Add Job Types</a>
                    </li>
                    <li><a class="menu-item" href="{{ route('view.job.type') }}" data-i18n="nav.dash.crypto">View Job Types</a>
                    </li>

                </ul>
            </li>

            <li class=" nav-item"><a href=""><i class="la la-object-group"></i><span class="menu-title" data-i18n="nav.dash.main">Job Categories</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{ route('add.job.category') }}" data-i18n="nav.dash.ecommerce">Add Job Categories</a>
                    </li>
                    <li><a class="menu-item" href="{{ route('view.job.category') }}" data-i18n="nav.dash.crypto">View Job Categories</a>
                    </li>

                </ul>
            </li>

            <li class=" nav-item"><a href="{{ route('view.job.post.request') }}"><i class="la la-object-group"></i><span class="menu-title" data-i18n="nav.dash.main">Job Post Requests <span class="badge badge-danger">{{$pendingJobsCount}}</span></span></a>

            <li class=" nav-item"><a href="{{ route('view.company') }}"><i class="la la-object-group"></i><span class="menu-title" data-i18n="nav.dash.main">View Companies <span class="badge badge-danger">{{$pendingCompanyCount}}</span></span></a>

            <li class=" nav-item"><a href="{{ route('admin.report') }}"><i class="la la-object-group"></i><span class="menu-title" data-i18n="nav.dash.main">View Report</span></a>


                </ul>
            </li>
        </ul>
    </div>
</div>