@extends('admin.layouts.admin_design')

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Job Categories </h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item active">View Job Post Request
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">View All Job Request</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">

                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Company Name</th>
                                                <th>Job Title</th>
                                                <th>Job Category</th>
                                                <th>Publish Date</th>
                                                <th>Expiry Date</th>
                                                <th>Job Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($jobPosts as $jobPost)
                                                <tr>
                                                    <td>{{$loop->index +1}}</td>
                                                    <td>
                                                        {{ $jobPost->company->name }}
                                                    </td>
                                                    <td>
                                                        {{ $jobPost->job_title }}
                                                    </td>
                                                    <td>{{$jobPost->category->category_name}}</td>
                                                    <td>{{ date('d M, Y', strtotime($jobPost->job_publish_date)) }}</td>
                                                    <td>
                                                        {{ date('d M, Y', strtotime($jobPost->job_expiry_date)) }}
                                                    </td>
                                                    <td>
                                                        @if($jobPost->job_status == 0)
                                                            <span class="badge badge-warning">Pending</span>
                                                        @elseif($jobPost->job_status == 1)
                                                            <span class="badge badge-success">Accepted</span>
                                                        @elseif($jobPost->job_status == 2)
                                                            <span class="badge badge-danger">Denied</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <form id="store" name="store" method="post" action="{{ route('change.job.post.status') }}">
                                                            @csrf
                                                            <input type="hidden" value="{{ $jobPost->id }}" name="job_post_id">
                                                            <input type="hidden" value="{{ $jobPost->job_status }}" name="old_status">
                                                            <select name="status" id="status" onchange="this.form.submit()">
                                                                <option value=0 @if($jobPost->job_status == 0) selected @endif>Pending</option>
                                                                <option value="1" @if($jobPost->job_status == 1) selected @endif>Accepted</option>
                                                                <option value="2" @if($jobPost->job_status == 2) selected @endif>Denied</option>
                                                            </select>
                                                        </form>
                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Zero configuration table -->

            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/assets/app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
@endsection

@section('scripts')
    <script src="{{asset('public/admin/assets/app-assets/vendors/js/tables/datatable/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/admin/assets/app-assets/js/scripts/tables/datatables/datatable-basic.js')}}"
            type="text/javascript"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js">
    </script>

@endsection